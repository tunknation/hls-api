﻿using System.Data;
using Dapper;
using HLS.Sales.Domain.SharedKernel;

namespace HLS.Sales.Application.SharedKernel.TypeHandlers {
    public class NorwegianKroneHandler : SqlMapper.TypeHandler<NorwegianKrone> {
        public override void SetValue(IDbDataParameter parameter, NorwegianKrone kr) {
            parameter.Value = kr.Value;
            parameter.DbType = DbType.Decimal;
        }

        public override NorwegianKrone Parse(object value) {
            return NorwegianKrone.Of((decimal)value);
        }
    }
}