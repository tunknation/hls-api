﻿using System;
using System.Data;
using Dapper;
using HLS.Sales.Domain.Containers;

namespace HLS.Sales.Application.Containers.TypeHandlers {
    public class ContainerIdHandler : SqlMapper.TypeHandler<ContainerId> {
        public override void SetValue(IDbDataParameter parameter, ContainerId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override ContainerId Parse(object value) {
            return new ContainerId(Guid.Parse(value.ToString()!));
        }
    }
}