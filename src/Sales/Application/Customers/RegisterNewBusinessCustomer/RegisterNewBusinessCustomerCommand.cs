﻿using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.RegisterNewBusinessCustomer {
    public class RegisterNewBusinessCustomerCommand : CommandBase<BusinessCustomerId> { }
}