﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.Customers;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Application.Customers.RegisterNewBusinessCustomer {
    internal class
        RegisterNewBusinessCustomerCommandHandler : ICommandHandler<RegisterNewBusinessCustomerCommand,
            BusinessCustomerId> {
        private readonly IAggregateStore _aggregateStore;

        public RegisterNewBusinessCustomerCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public Task<BusinessCustomerId> Handle(RegisterNewBusinessCustomerCommand cmd,
            CancellationToken cancellationToken) {
            var customer = BusinessCustomer.RegisterNewBusinessCustomer();

            _aggregateStore.AppendChanges(customer);

            return Task.FromResult(customer.Id);
        }
    }
}