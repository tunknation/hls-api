﻿using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.Dtos {
    public class PrivateCustomerDto : CustomerDto {
        public new PrivateCustomerId Id { get; set; }
    }
}