﻿using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.Dtos {
    public abstract class CustomerDto {
        public CustomerId Id { get; set; }

        public CustomerType Type { get; set; }

        public CustomerStatus Status { get; set; }
    }
}