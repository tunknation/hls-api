﻿using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.Dtos {
    public class BusinessCustomerDto : CustomerDto {
        public new BusinessCustomerId Id { get; set; }
    }
}