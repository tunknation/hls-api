﻿using System.Data;
using Dapper;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.TypeHandlers {
    public class CustomerStatusHandler : SqlMapper.TypeHandler<CustomerStatus> {
        public override void SetValue(IDbDataParameter parameter, CustomerStatus status) {
            parameter.Value = status.Code;
            parameter.DbType = DbType.String;
        }

        public override CustomerStatus Parse(object value) {
            return CustomerStatus.Of(value.ToString());
        }
    }
}