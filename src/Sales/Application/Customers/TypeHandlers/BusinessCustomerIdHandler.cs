﻿using System;
using System.Data;
using Dapper;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.TypeHandlers {
    public class BusinessCustomerIdHandler : SqlMapper.TypeHandler<BusinessCustomerId> {
        public override void SetValue(IDbDataParameter parameter, BusinessCustomerId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override BusinessCustomerId Parse(object value) {
            return new BusinessCustomerId(Guid.Parse(value.ToString()!));
        }
    }
}