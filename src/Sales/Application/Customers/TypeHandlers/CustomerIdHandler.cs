﻿using System;
using System.Data;
using Dapper;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.TypeHandlers {
    public class CustomerIdHandler : SqlMapper.TypeHandler<CustomerId> {
        public override void SetValue(IDbDataParameter parameter, CustomerId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override CustomerId Parse(object value) {
            return new CustomerId(Guid.Parse(value.ToString()!));
        }
    }
}