﻿using System;
using System.Data;
using Dapper;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.TypeHandlers {
    public class PrivateCustomerIdHandler : SqlMapper.TypeHandler<PrivateCustomerId> {
        public override void SetValue(IDbDataParameter parameter, PrivateCustomerId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override PrivateCustomerId Parse(object value) {
            return new PrivateCustomerId(Guid.Parse(value.ToString()!));
        }
    }
}