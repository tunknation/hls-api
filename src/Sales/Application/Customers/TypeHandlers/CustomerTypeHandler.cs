﻿using System.Data;
using Dapper;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.TypeHandlers {
    public class CustomerTypeHandler : SqlMapper.TypeHandler<CustomerType> {
        public override void SetValue(IDbDataParameter parameter, CustomerType type) {
            parameter.Value = type.Code;
            parameter.DbType = DbType.String;
        }

        public override CustomerType Parse(object value) {
            return CustomerType.Of(value.ToString());
        }
    }
}