﻿using System;
using HLS.Sales.Application.Configuration.Queries;
using HLS.Sales.Application.Customers.Dtos;

namespace HLS.Sales.Application.Customers.GetCustomer {
    public class GetCustomerQuery : QueryBase<CustomerDto> {
        public GetCustomerQuery(Guid customerId) {
            CustomerId = customerId;
        }

        public Guid CustomerId { get; }
    }
}