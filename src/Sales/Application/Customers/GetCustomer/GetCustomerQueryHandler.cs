﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Sales.Application.Configuration.Queries;
using HLS.Sales.Application.Customers.Dtos;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.GetCustomer {
    internal class GetCustomerQueryHandler : IQueryHandler<GetCustomerQuery, CustomerDto> {
        private readonly IDbConnection _connection;

        public GetCustomerQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<CustomerDto> Handle(GetCustomerQuery query, CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [C].[Id],
                       [C].[Type],
                       [C].[Status]
                FROM [sales].[v_Customer] AS [C]
                WHERE [C].[Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", query.CustomerId);

            using var reader = await _connection.ExecuteReaderAsync(sql, parameters);

            var businessCustomerParser = reader.GetRowParser<BusinessCustomerDto>();
            var privateCustomerParser = reader.GetRowParser<PrivateCustomerDto>();

            var customers = new List<CustomerDto>();

            while (reader.Read()) {
                CustomerDto customer = reader.GetString(reader.GetOrdinal("Type")) switch {
                    nameof(CustomerType.Business) => businessCustomerParser(reader),
                    nameof(CustomerType.Private) => privateCustomerParser(reader),
                    _ => throw new Exception("Customer type is not supported")
                };

                customers.Add(customer);
            }

            return customers.FirstOrDefault();
        }
    }
}