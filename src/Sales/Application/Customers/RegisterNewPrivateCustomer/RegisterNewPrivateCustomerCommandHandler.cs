﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.Customers;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Application.Customers.RegisterNewPrivateCustomer {
    internal class
        RegisterNewPrivateCustomerCommandHandler : ICommandHandler<RegisterNewPrivateCustomerCommand,
            PrivateCustomerId> {
        private readonly IAggregateStore _aggregateStore;

        public RegisterNewPrivateCustomerCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public Task<PrivateCustomerId> Handle(RegisterNewPrivateCustomerCommand cmd,
            CancellationToken cancellationToken) {
            var customer = PrivateCustomer.RegisterNewPrivateCustomer();

            _aggregateStore.AppendChanges(customer);

            return Task.FromResult(customer.Id);
        }
    }
}