﻿using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.RegisterNewPrivateCustomer {
    public class RegisterNewPrivateCustomerCommand : CommandBase<PrivateCustomerId> { }
}