﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Core.Domain;
using HLS.Sales.Application.Configuration.Projections;
using HLS.Sales.Domain.Customers.Events;

namespace HLS.Sales.Application.Customers.Projections {
    internal class CustomerProjector : IProjector {
        private readonly IDbConnection _connection;

        public CustomerProjector(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Project(IDomainEvent @event) {
            await _when((dynamic)@event);
        }

        private static Task _when(IDomainEvent @event) {
            return Task.CompletedTask;
        }

        private async Task _when(NewBusinessCustomerRegisteredDomainEvent @event) {
            const string sql = @"
                INSERT INTO [sales].[Customer]
                    ([Id], [Type], [Status]) 
                VALUES (@Id, @Type, @Status)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.CustomerId);
            parameters.Add("@Type", @event.Type);
            parameters.Add("@Status", @event.Status);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(NewPrivateCustomerRegisteredDomainEvent @event) {
            const string sql = @"
                INSERT INTO [sales].[Customer]
                    ([Id], [Type], [Status]) 
                VALUES (@Id, @Type, @Status)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.CustomerId);
            parameters.Add("@Type", @event.Type);
            parameters.Add("@Status", @event.Status);

            await _connection.ExecuteAsync(sql, parameters);
        }
    }
}