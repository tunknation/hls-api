﻿using System.Collections.Generic;
using HLS.Sales.Application.Configuration.Queries;
using HLS.Sales.Application.Customers.Dtos;
using HLS.Sales.Application.Pagination;

namespace HLS.Sales.Application.Customers.GetCustomers {
    public class GetCustomersQuery : QueryBase<IEnumerable<CustomerDto>>, IPagedQuery {
        public GetCustomersQuery(int? page, int? perPage) {
            Page = page;
            PerPage = perPage;
        }

        public int? Page { get; }

        public int? PerPage { get; }
    }
}