﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Sales.Application.Configuration.Queries;
using HLS.Sales.Application.Customers.Dtos;
using HLS.Sales.Application.Pagination;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Application.Customers.GetCustomers {
    internal class GetCustomersQueryHandler : IQueryHandler<GetCustomersQuery, IEnumerable<CustomerDto>> {
        private readonly IDbConnection _connection;

        public GetCustomersQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<IEnumerable<CustomerDto>>
            Handle(GetCustomersQuery query, CancellationToken cancellationToken) {
            var parameters = new DynamicParameters();
            var pageData = PagedQueryHelper.GetPageData(query);

            parameters.Add(nameof(PagedQueryHelper.Offset), pageData.Offset);
            parameters.Add(nameof(PagedQueryHelper.Next), pageData.Next);

            var sql = @"
                SELECT [C].[Id],
                       [C].[Type],
                       [C].[Status]
                FROM [sales].[v_Customer] AS [C]
                ORDER BY [C].[Id]";

            sql = PagedQueryHelper.AppendPageStatement(sql);

            using var reader = await _connection.ExecuteReaderAsync(sql, parameters);

            var businessCustomerParser = reader.GetRowParser<BusinessCustomerDto>();
            var privateCustomerParser = reader.GetRowParser<PrivateCustomerDto>();

            var customers = new List<CustomerDto>();

            while (reader.Read()) {
                CustomerDto customer = reader.GetString(reader.GetOrdinal("Type")) switch {
                    nameof(CustomerType.Business) => businessCustomerParser(reader),
                    nameof(CustomerType.Private) => privateCustomerParser(reader),
                    _ => throw new Exception("Customer type is not supported")
                };

                customers.Add(customer);
            }

            return customers;
        }
    }
}