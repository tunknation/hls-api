﻿using System;
using System.Data;
using Dapper;
using HLS.Sales.Domain.Zones;

namespace HLS.Sales.Application.Zones.TypeHandlers {
    public class ZoneIdHandler : SqlMapper.TypeHandler<ZoneId> {
        public override void SetValue(IDbDataParameter parameter, ZoneId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override ZoneId Parse(object value) {
            return new ZoneId(Guid.Parse(value.ToString()!));
        }
    }
}