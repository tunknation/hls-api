﻿using MediatR;

namespace HLS.Sales.Application.Configuration.Queries {
    public interface IQuery<out TResult> : IRequest<TResult> { }
}