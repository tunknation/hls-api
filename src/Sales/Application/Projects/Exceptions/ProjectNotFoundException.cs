﻿using System;

namespace HLS.Sales.Application.Projects.Exceptions {
    public class ProjectNotFoundException : Exception {
        private const string ErrorMessage = "Project not found";

        public ProjectNotFoundException() : base(ErrorMessage) { }
    }
}