﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Core.Domain;
using HLS.Sales.Application.Configuration.Projections;
using HLS.Sales.Domain.Projects.Events;

namespace HLS.Sales.Application.Projects.Projections {
    internal class ProjectProjector : IProjector {
        private readonly IDbConnection _connection;

        public ProjectProjector(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Project(IDomainEvent @event) {
            await _when((dynamic)@event);
        }

        private static Task _when(IDomainEvent @event) {
            return Task.CompletedTask;
        }

        private async Task _when(ProjectCreatedDomainEvent @event) {
            const string sql = @"                
                INSERT INTO [sales].[Project] 
                    ([Id], [CustomerId], [Address])
                VALUES (@Id, @CustomerId, @Address)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ProjectId);
            parameters.Add("@CustomerId", @event.CustomerId);
            parameters.Add("@Address", @event.Address);

            await _connection.ExecuteAsync(sql, parameters);
        }
    }
}