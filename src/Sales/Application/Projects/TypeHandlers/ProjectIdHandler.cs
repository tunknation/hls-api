﻿using System;
using System.Data;
using Dapper;
using HLS.Sales.Domain.Projects;

namespace HLS.Sales.Application.Projects.TypeHandlers {
    public class ProjectIdHandler : SqlMapper.TypeHandler<ProjectId> {
        public override void SetValue(IDbDataParameter parameter, ProjectId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override ProjectId Parse(object value) {
            return new ProjectId(Guid.Parse(value.ToString()!));
        }
    }
}