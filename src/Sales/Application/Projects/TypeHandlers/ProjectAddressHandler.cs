﻿using System.Data;
using Dapper;
using HLS.Sales.Domain.Projects;
using Newtonsoft.Json;

namespace HLS.Sales.Application.Projects.TypeHandlers {
    public class ProjectAddressHandler : SqlMapper.TypeHandler<ProjectAddress> {
        public override void SetValue(IDbDataParameter parameter, ProjectAddress address) {
            parameter.Value = JsonConvert.SerializeObject(address);
            parameter.DbType = DbType.String;
            parameter.Size = int.MaxValue;
        }

        public override ProjectAddress Parse(object value) {
            return JsonConvert.DeserializeObject<ProjectAddress>(value.ToString()!);
        }
    }
}