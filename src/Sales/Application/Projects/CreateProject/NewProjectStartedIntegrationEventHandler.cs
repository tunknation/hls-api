﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.IntegrationEvents;
using HLS.Sales.Application.Configuration.Commands;
using MediatR;

namespace HLS.Sales.Application.Projects.CreateProject {
    internal class NewProjectStartedIntegrationEventHandler : INotificationHandler<NewProjectStartedIntegrationEvent> {
        private readonly ICommandsScheduler _commandsScheduler;

        public NewProjectStartedIntegrationEventHandler(ICommandsScheduler commandsScheduler) {
            _commandsScheduler = commandsScheduler;
        }

        public async Task Handle(NewProjectStartedIntegrationEvent @event, CancellationToken cancellationToken) {
            var cmd = new CreateProjectCommand(
                @event.ProjectId,
                @event.CustomerId,
                @event.City,
                @event.PostalCode,
                @event.StreetAddress);

            await _commandsScheduler.EnqueueAsync(cmd);
        }
    }
}