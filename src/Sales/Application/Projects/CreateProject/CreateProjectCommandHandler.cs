﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.Customers;
using HLS.Sales.Domain.Projects;
using HLS.Sales.Domain.SeedWork;
using MediatR;

namespace HLS.Sales.Application.Projects.CreateProject {
    internal class CreateProjectCommandHandler : ICommandHandler<CreateProjectCommand> {
        private readonly IAggregateStore _aggregateStore;

        public CreateProjectCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public Task<Unit> Handle(CreateProjectCommand cmd, CancellationToken cancellationToken) {
            var projectId = new ProjectId(cmd.ProjectId);
            var customerId = new CustomerId(cmd.CustomerId);
            var address = ProjectAddress.Of(cmd.City, cmd.PostalCode, cmd.StreetAddress);

            var project = Project.Create(projectId, customerId, address);

            _aggregateStore.AppendChanges(project);

            return Task.FromResult(Unit.Value);
        }
    }
}