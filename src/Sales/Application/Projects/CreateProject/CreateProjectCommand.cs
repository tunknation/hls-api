﻿using System;
using HLS.Sales.Application.Configuration.Commands;

namespace HLS.Sales.Application.Projects.CreateProject {
    public class CreateProjectCommand : InternalCommandBase {
        public CreateProjectCommand(
            Guid projectId,
            Guid customerId,
            string city,
            string postalCode,
            string streetAddress) {
            ProjectId = projectId;
            CustomerId = customerId;
            City = city;
            PostalCode = postalCode;
            StreetAddress = streetAddress;
        }

        public Guid ProjectId { get; }

        public Guid CustomerId { get; }

        public string City { get; }

        public string PostalCode { get; }

        public string StreetAddress { get; }
    }
}