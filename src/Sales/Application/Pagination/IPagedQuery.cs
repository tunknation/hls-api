﻿namespace HLS.Sales.Application.Pagination {
    public interface IPagedQuery {
        int? Page { get; }
        int? PerPage { get; }
    }
}