﻿using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Application.Configuration.Queries;

namespace HLS.Sales.Application.Contracts {
    public interface ISalesModule {
        Task<TResult> ExecuteCommandAsync<TResult>(ICommand<TResult> command);

        Task ExecuteCommandAsync(ICommand command);

        Task<TResult> ExecuteQueryAsync<TResult>(IQuery<TResult> query);
    }
}