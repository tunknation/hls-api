﻿using HLS.Sales.Domain.Orders;

namespace HLS.Sales.Application.Orders.Dtos {
    public abstract class OrderDto {
        public OrderId Id { get; set; }

        public OrderType Type { get; set; }

        public OrderStatus Status { get; set; }
    }
}