﻿using System;
using HLS.Sales.Domain.Containers;
using HLS.Sales.Domain.Customers;
using HLS.Sales.Domain.Orders;
using HLS.Sales.Domain.Projects;

namespace HLS.Sales.Application.Orders.Dtos {
    public class ContainerRetrievalOrderDto : OrderDto {
        public new ContainerRetrievalOrderId Id { get; set; }

        public ProjectId ProjectId { get; set; }

        public CustomerId CustomerId { get; set; }

        public ContainerId ContainerId { get; set; }

        public OrderAddress Address { get; set; }

        public DateTime? FulfillmentDate { get; set; }
    }
}