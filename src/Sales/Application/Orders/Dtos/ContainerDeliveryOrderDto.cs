﻿using System;
using HLS.Sales.Domain.ContainerTypes;
using HLS.Sales.Domain.Customers;
using HLS.Sales.Domain.Orders;

namespace HLS.Sales.Application.Orders.Dtos {
    public class ContainerDeliveryOrderDto : OrderDto {
        public new ContainerDeliveryOrderId Id { get; set; }

        public CustomerId CustomerId { get; set; }

        public ContainerTypeId ContainerTypeId { get; set; }

        public OrderAddress Address { get; set; }

        public DateTime? FulfillmentDate { get; set; }
    }
}