﻿using System;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.Orders;

namespace HLS.Sales.Application.Orders.PlaceContainerDeliveryOrder {
    public class PlaceContainerDeliveryOrderCommand : CommandBase<ContainerDeliveryOrderId> {
        public PlaceContainerDeliveryOrderCommand(
            Guid customerId,
            Guid containerTypeId,
            string city,
            string postalCode,
            string streetAddress) {
            CustomerId = customerId;
            ContainerTypeId = containerTypeId;
            City = city;
            PostalCode = postalCode;
            StreetAddress = streetAddress;
        }

        public Guid CustomerId { get; }

        public Guid ContainerTypeId { get; }

        public string City { get; }

        public string PostalCode { get; }

        public string StreetAddress { get; }
    }
}