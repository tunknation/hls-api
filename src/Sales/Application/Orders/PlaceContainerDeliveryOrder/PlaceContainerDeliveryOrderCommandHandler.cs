﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.ContainerTypes;
using HLS.Sales.Domain.Customers;
using HLS.Sales.Domain.Orders;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Application.Orders.PlaceContainerDeliveryOrder {
    internal class
        PlaceContainerDeliveryOrderCommandHandler : ICommandHandler<PlaceContainerDeliveryOrderCommand,
            ContainerDeliveryOrderId> {
        private readonly IAggregateStore _aggregateStore;

        public PlaceContainerDeliveryOrderCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public Task<ContainerDeliveryOrderId> Handle(PlaceContainerDeliveryOrderCommand cmd,
            CancellationToken cancellationToken) {
            var customerId = new CustomerId(cmd.CustomerId);
            var containerTypeId = new ContainerTypeId(cmd.ContainerTypeId);
            var address = OrderAddress.Of(cmd.City, cmd.PostalCode, cmd.StreetAddress);

            var order = ContainerDeliveryOrder.PlaceContainerDeliveryOrder(
                customerId,
                containerTypeId,
                address);

            _aggregateStore.AppendChanges(order);

            return Task.FromResult(order.Id);
        }
    }
}