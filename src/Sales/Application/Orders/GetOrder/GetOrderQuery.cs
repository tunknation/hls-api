﻿using System;
using HLS.Sales.Application.Configuration.Queries;
using HLS.Sales.Application.Orders.Dtos;

namespace HLS.Sales.Application.Orders.GetOrder {
    public class GetOrderQuery : QueryBase<OrderDto> {
        public GetOrderQuery(Guid orderId) {
            OrderId = orderId;
        }

        public Guid OrderId { get; }
    }
}