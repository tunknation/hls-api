﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Application.Orders.Exceptions;
using HLS.Sales.Domain.Orders;
using HLS.Sales.Domain.SeedWork;
using MediatR;

namespace HLS.Sales.Application.Orders.FulfillContainerDeliveryOrder {
    internal class FulfillContainerDeliveryOrderCommandHandler : ICommandHandler<FulfillContainerDeliveryOrderCommand> {
        private readonly IAggregateStore _aggregateStore;

        public FulfillContainerDeliveryOrderCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(FulfillContainerDeliveryOrderCommand cmd, CancellationToken cancellationToken) {
            var order = await _aggregateStore.Load(new ContainerDeliveryOrderId(cmd.OrderId));

            if (order is null) {
                throw new ContainerDeliveryOrderNotFoundException();
            }

            order.FulfillContainerDeliveryOrder(cmd.FulfillmentDate);

            _aggregateStore.AppendChanges(order);

            return Unit.Value;
        }
    }
}