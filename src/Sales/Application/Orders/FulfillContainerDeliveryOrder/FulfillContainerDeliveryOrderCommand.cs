﻿using System;
using HLS.Sales.Application.Configuration.Commands;

namespace HLS.Sales.Application.Orders.FulfillContainerDeliveryOrder {
    public class FulfillContainerDeliveryOrderCommand : CommandBase {
        public FulfillContainerDeliveryOrderCommand(Guid orderId, DateTime? fulfillmentDate) {
            OrderId = orderId;
            FulfillmentDate = fulfillmentDate;
        }

        public Guid OrderId { get; }

        public DateTime? FulfillmentDate { get; }
    }
}