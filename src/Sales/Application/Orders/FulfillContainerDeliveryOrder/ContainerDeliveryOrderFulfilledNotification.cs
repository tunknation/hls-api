﻿using System;
using HLS.Core.Application.Events;
using HLS.Sales.Domain.Orders.Events;
using Newtonsoft.Json;

namespace HLS.Sales.Application.Orders.FulfillContainerDeliveryOrder {
    public class
        ContainerDeliveryOrderFulfilledNotification : DomainNotificationBase<
            ContainerDeliveryOrderFulfilledDomainEvent> {
        [JsonConstructor]
        public ContainerDeliveryOrderFulfilledNotification(ContainerDeliveryOrderFulfilledDomainEvent domainEvent,
            Guid id) : base(domainEvent, id) { }
    }
}