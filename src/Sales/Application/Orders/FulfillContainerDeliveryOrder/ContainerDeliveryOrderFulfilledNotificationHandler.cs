﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HLS.Core.EventBus;
using HLS.Sales.Application.Orders.Exceptions;
using HLS.Sales.Domain.SeedWork;
using HLS.Sales.IntegrationEvents;
using MediatR;

namespace HLS.Sales.Application.Orders.FulfillContainerDeliveryOrder {
    internal class
        ContainerDeliveryOrderFulfilledNotificationHandler : INotificationHandler<
            ContainerDeliveryOrderFulfilledNotification> {
        private readonly IAggregateStore _aggregateStore;

        private readonly IEventsBus _eventsBus;

        public ContainerDeliveryOrderFulfilledNotificationHandler(IAggregateStore aggregateStore,
            IEventsBus eventsBus) {
            _aggregateStore = aggregateStore;
            _eventsBus = eventsBus;
        }

        public async Task Handle(
            ContainerDeliveryOrderFulfilledNotification notification,
            CancellationToken cancellationToken) {
            var order = await _aggregateStore.Load(notification.DomainEvent.OrderId);

            if (order is null) {
                throw new ContainerDeliveryOrderNotFoundException();
            }

            var orderSnapshot = order.GetSnapshot();

            if (orderSnapshot.FulfillmentDate is null) {
                throw new InvalidOperationException();
            }

            var @event = new ContainerDeliveryOrderFulfilledIntegrationEvent(
                notification.Id,
                notification.DomainEvent.OccurredOn,
                orderSnapshot.OrderId.Value,
                orderSnapshot.CustomerId.Value,
                orderSnapshot.ContainerTypeId.Value,
                orderSnapshot.FulfillmentDate.Value,
                orderSnapshot.Address.City,
                orderSnapshot.Address.PostalCode,
                orderSnapshot.Address.StreetAddress);

            await _eventsBus.Publish(@event);
        }
    }
}