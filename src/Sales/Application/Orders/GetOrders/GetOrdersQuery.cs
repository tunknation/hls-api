﻿using System.Collections.Generic;
using HLS.Sales.Application.Configuration.Queries;
using HLS.Sales.Application.Orders.Dtos;
using HLS.Sales.Application.Pagination;

namespace HLS.Sales.Application.Orders.GetOrders {
    public class GetOrdersQuery : QueryBase<IEnumerable<OrderDto>>, IPagedQuery {
        public GetOrdersQuery(int? page, int? perPage) {
            Page = page;
            PerPage = perPage;
        }

        public int? Page { get; }

        public int? PerPage { get; }
    }
}