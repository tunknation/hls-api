﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Sales.Application.Configuration.Queries;
using HLS.Sales.Application.Orders.Dtos;
using HLS.Sales.Application.Pagination;
using HLS.Sales.Domain.Orders;

namespace HLS.Sales.Application.Orders.GetOrders {
    internal class GetOrdersQueryHandler : IQueryHandler<GetOrdersQuery, IEnumerable<OrderDto>> {
        private readonly IDbConnection _connection;

        public GetOrdersQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<IEnumerable<OrderDto>>
            Handle(GetOrdersQuery query, CancellationToken cancellationToken) {
            var parameters = new DynamicParameters();
            var pageData = PagedQueryHelper.GetPageData(query);

            parameters.Add(nameof(PagedQueryHelper.Offset), pageData.Offset);
            parameters.Add(nameof(PagedQueryHelper.Next), pageData.Next);

            var sql = @"
                SELECT [O].[Id],
                       [O].[Type],
                       [O].[Status],
                       [O].[CustomerId],
                       [O].[ContainerTypeId],
                       [O].[ProjectId],
                       [O].[ContainerId],
                       [O].[Address],
                       [O].[FulfillmentDate]
                FROM [sales].[v_Order] AS [O]
                ORDER BY [O].[Id]";

            sql = PagedQueryHelper.AppendPageStatement(sql);

            using var reader = await _connection.ExecuteReaderAsync(sql, parameters);

            var containerDeliveryOrderParser = reader.GetRowParser<ContainerDeliveryOrderDto>();
            var containerRetrievalOrderParser = reader.GetRowParser<ContainerRetrievalOrderDto>();

            var orders = new List<OrderDto>();

            while (reader.Read()) {
                OrderDto order = reader.GetString(reader.GetOrdinal("Type")) switch {
                    nameof(OrderType.ContainerDelivery) => containerDeliveryOrderParser(reader),
                    nameof(OrderType.ContainerRetrieval) => containerRetrievalOrderParser(reader),
                    _ => throw new Exception("Order type is not supported")
                };

                orders.Add(order);
            }

            return orders;
        }
    }
}