﻿using System;
using HLS.Core.Application.Events;
using HLS.Sales.Domain.Orders.Events;
using Newtonsoft.Json;

namespace HLS.Sales.Application.Orders.FulfillContainerRetrievalOrder {
    public class
        ContainerRetrievalOrderFulfilledNotification : DomainNotificationBase<
            ContainerRetrievalOrderFulfilledDomainEvent> {
        [JsonConstructor]
        public ContainerRetrievalOrderFulfilledNotification(ContainerRetrievalOrderFulfilledDomainEvent domainEvent,
            Guid id) : base(domainEvent, id) { }
    }
}