﻿using System;
using HLS.Sales.Application.Configuration.Commands;

namespace HLS.Sales.Application.Orders.FulfillContainerRetrievalOrder {
    public class FulfillContainerRetrievalOrderCommand : CommandBase {
        public FulfillContainerRetrievalOrderCommand(Guid orderId, DateTime? fulfillmentDate) {
            OrderId = orderId;
            FulfillmentDate = fulfillmentDate;
        }

        public Guid OrderId { get; }

        public DateTime? FulfillmentDate { get; }
    }
}