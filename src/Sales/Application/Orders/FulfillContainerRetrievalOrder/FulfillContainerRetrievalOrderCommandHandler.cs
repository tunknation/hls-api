﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Application.Orders.Exceptions;
using HLS.Sales.Domain.Orders;
using HLS.Sales.Domain.SeedWork;
using MediatR;

namespace HLS.Sales.Application.Orders.FulfillContainerRetrievalOrder {
    internal class
        FulfillContainerRetrievalOrderCommandHandler : ICommandHandler<FulfillContainerRetrievalOrderCommand> {
        private readonly IAggregateStore _aggregateStore;

        public FulfillContainerRetrievalOrderCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(FulfillContainerRetrievalOrderCommand cmd, CancellationToken cancellationToken) {
            var order = await _aggregateStore.Load(new ContainerRetrievalOrderId(cmd.OrderId));

            if (order is null) {
                throw new ContainerRetrievalOrderNotFoundException();
            }

            order.FulfillContainerRetrievalOrder(cmd.FulfillmentDate);

            _aggregateStore.AppendChanges(order);

            return Unit.Value;
        }
    }
}