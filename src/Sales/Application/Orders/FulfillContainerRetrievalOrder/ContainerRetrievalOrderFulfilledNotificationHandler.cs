﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HLS.Core.EventBus;
using HLS.Sales.Application.Orders.Exceptions;
using HLS.Sales.Domain.SeedWork;
using HLS.Sales.IntegrationEvents;
using MediatR;

namespace HLS.Sales.Application.Orders.FulfillContainerRetrievalOrder {
    public class
        ContainerRetrievalOrderFulfilledNotificationHandler : INotificationHandler<
            ContainerRetrievalOrderFulfilledNotification> {
        private readonly IAggregateStore _aggregateStore;

        private readonly IEventsBus _eventsBus;

        public ContainerRetrievalOrderFulfilledNotificationHandler(
            IAggregateStore aggregateStore,
            IEventsBus eventsBus) {
            _aggregateStore = aggregateStore;
            _eventsBus = eventsBus;
        }

        public async Task Handle(
            ContainerRetrievalOrderFulfilledNotification notification,
            CancellationToken cancellationToken) {
            var order = await _aggregateStore.Load(notification.DomainEvent.OrderId);

            if (order is null) {
                throw new ContainerRetrievalOrderNotFoundException();
            }

            var orderSnapshot = order.GetSnapshot();

            if (orderSnapshot.FulfillmentDate is null) {
                throw new InvalidOperationException();
            }

            var @event = new ContainerRetrievalOrderFulfilledIntegrationEvent(
                notification.Id,
                notification.DomainEvent.OccurredOn,
                orderSnapshot.OrderId.Value,
                orderSnapshot.ProjectId.Value,
                orderSnapshot.ContainerId.Value,
                orderSnapshot.FulfillmentDate.Value);

            await _eventsBus.Publish(@event);
        }
    }
}