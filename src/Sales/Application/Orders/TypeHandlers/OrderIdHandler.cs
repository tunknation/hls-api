﻿using System;
using System.Data;
using Dapper;
using HLS.Sales.Domain.Orders;

namespace HLS.Sales.Application.Orders.TypeHandlers {
    public class OrderIdHandler : SqlMapper.TypeHandler<OrderId> {
        public override void SetValue(IDbDataParameter parameter, OrderId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override OrderId Parse(object value) {
            return new OrderId(Guid.Parse(value.ToString()!));
        }
    }
}