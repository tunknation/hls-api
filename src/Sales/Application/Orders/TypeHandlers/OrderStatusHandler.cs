﻿using System.Data;
using Dapper;
using HLS.Sales.Domain.Orders;

namespace HLS.Sales.Application.Orders.TypeHandlers {
    public class OrderStatusHandler : SqlMapper.TypeHandler<OrderStatus> {
        public override void SetValue(IDbDataParameter parameter, OrderStatus status) {
            parameter.Value = status.Code;
            parameter.DbType = DbType.String;
        }

        public override OrderStatus Parse(object value) {
            return OrderStatus.Of(value.ToString());
        }
    }
}