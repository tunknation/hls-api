﻿using System;
using System.Data;
using Dapper;
using HLS.Sales.Domain.Orders;

namespace HLS.Sales.Application.Orders.TypeHandlers {
    public class ContainerRetrievalOrderIdHandler : SqlMapper.TypeHandler<ContainerRetrievalOrderId> {
        public override void SetValue(IDbDataParameter parameter, ContainerRetrievalOrderId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override ContainerRetrievalOrderId Parse(object value) {
            return new ContainerRetrievalOrderId(Guid.Parse(value.ToString()!));
        }
    }
}