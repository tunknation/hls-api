﻿using System;
using System.Data;
using Dapper;
using HLS.Sales.Domain.Orders;

namespace HLS.Sales.Application.Orders.TypeHandlers {
    public class ContainerDeliveryOrderIdHandler : SqlMapper.TypeHandler<ContainerDeliveryOrderId> {
        public override void SetValue(IDbDataParameter parameter, ContainerDeliveryOrderId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override ContainerDeliveryOrderId Parse(object value) {
            return new ContainerDeliveryOrderId(Guid.Parse(value.ToString()!));
        }
    }
}