﻿using System.Data;
using Dapper;
using HLS.Sales.Domain.Orders;
using Newtonsoft.Json;

namespace HLS.Sales.Application.Orders.TypeHandlers {
    public class OrderAddressHandler : SqlMapper.TypeHandler<OrderAddress> {
        public override void SetValue(IDbDataParameter parameter, OrderAddress address) {
            parameter.Value = JsonConvert.SerializeObject(address);
            parameter.DbType = DbType.String;
            parameter.Size = int.MaxValue;
        }

        public override OrderAddress Parse(object value) {
            return JsonConvert.DeserializeObject<OrderAddress>(value.ToString()!);
        }
    }
}