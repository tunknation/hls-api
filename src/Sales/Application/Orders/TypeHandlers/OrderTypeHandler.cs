﻿using System.Data;
using Dapper;
using HLS.Sales.Domain.Orders;

namespace HLS.Sales.Application.Orders.TypeHandlers {
    public class OrderTypeHandler : SqlMapper.TypeHandler<OrderType> {
        public override void SetValue(IDbDataParameter parameter, OrderType type) {
            parameter.Value = type.Code;
            parameter.DbType = DbType.String;
        }

        public override OrderType Parse(object value) {
            return OrderType.Of(value.ToString());
        }
    }
}