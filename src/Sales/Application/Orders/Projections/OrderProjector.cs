﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Core.Domain;
using HLS.Sales.Application.Configuration.Projections;
using HLS.Sales.Domain.Orders.Events;

namespace HLS.Sales.Application.Orders.Projections {
    internal class OrderProjector : IProjector {
        private readonly IDbConnection _connection;

        public OrderProjector(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Project(IDomainEvent @event) {
            await _when((dynamic)@event);
        }

        private static Task _when(IDomainEvent @event) {
            return Task.CompletedTask;
        }

        private async Task _when(ContainerDeliveryOrderPlacedDomainEvent @event) {
            const string sql = @"
                INSERT INTO [sales].[Order]
                    ([Id], [Type], [Status], [CustomerId], [ContainerTypeId], [Address])
                VALUES (@Id, @Type, @Status, @CustomerId, @ContainerTypeId, @Address)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.OrderId);
            parameters.Add("@Type", @event.Type);
            parameters.Add("@Status", @event.Status);
            parameters.Add("@CustomerId", @event.CustomerId);
            parameters.Add("@ContainerTypeId", @event.ContainerTypeId);
            parameters.Add("@Address", @event.Address);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerDeliveryOrderFulfilledDomainEvent @event) {
            const string sql = @"
                UPDATE [sales].[Order]
                SET [Status] = @Status,
                    [FulfillmentDate] = @FulfillmentDate
                WHERE [Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.OrderId);
            parameters.Add("@Status", @event.Status);
            parameters.Add("@FulfillmentDate", @event.FulfillmentDate);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerRetrievalOrderPlacedDomainEvent @event) {
            const string sql = @"
                INSERT INTO [sales].[Order]
                    ([Id], [Type], [Status], [CustomerId], [ProjectId], [ContainerId], [Address])
                VALUES (@Id, @Type, @Status, @CustomerId, @ProjectId, @ContainerId, @Address)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.OrderId);
            parameters.Add("@Type", @event.Type);
            parameters.Add("@Status", @event.Status);
            parameters.Add("@CustomerId", @event.CustomerId);
            parameters.Add("@ProjectId", @event.ProjectId);
            parameters.Add("@ContainerId", @event.ContainerId);
            parameters.Add("@Address", @event.Address);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerRetrievalOrderFulfilledDomainEvent @event) {
            const string sql = @"
                UPDATE [sales].[Order]
                SET [Status] = @Status,
                    [FulfillmentDate] = @FulfillmentDate
                WHERE [Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.OrderId);
            parameters.Add("@Status", @event.Status);
            parameters.Add("@FulfillmentDate", @event.FulfillmentDate);

            await _connection.ExecuteAsync(sql, parameters);
        }
    }
}