﻿using System;

namespace HLS.Sales.Application.Orders.Exceptions {
    public class ContainerDeliveryOrderNotFoundException : Exception {
        private const string ErrorMessage = "Container delivery order not found";

        public ContainerDeliveryOrderNotFoundException() : base(ErrorMessage) { }
    }
}