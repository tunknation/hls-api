﻿using System;

namespace HLS.Sales.Application.Orders.Exceptions {
    public class ContainerRetrievalOrderNotFoundException : Exception {
        private const string ErrorMessage = "Container retrieval order not found";

        public ContainerRetrievalOrderNotFoundException() : base(ErrorMessage) { }
    }
}