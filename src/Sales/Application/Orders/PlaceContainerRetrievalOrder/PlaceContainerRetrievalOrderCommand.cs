﻿using System;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.Orders;

namespace HLS.Sales.Application.Orders.PlaceContainerRetrievalOrder {
    public class PlaceContainerRetrievalOrderCommand : CommandBase<ContainerRetrievalOrderId> {
        public PlaceContainerRetrievalOrderCommand(Guid projectId, Guid containerId) {
            ProjectId = projectId;
            ContainerId = containerId;
        }

        public Guid ProjectId { get; }

        public Guid ContainerId { get; }
    }
}