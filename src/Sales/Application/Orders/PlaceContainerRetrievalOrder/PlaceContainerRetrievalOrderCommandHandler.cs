﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Application.Projects.Exceptions;
using HLS.Sales.Domain.Containers;
using HLS.Sales.Domain.Orders;
using HLS.Sales.Domain.Projects;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Application.Orders.PlaceContainerRetrievalOrder {
    internal class
        PlaceContainerRetrievalOrderCommandHandler : ICommandHandler<PlaceContainerRetrievalOrderCommand,
            ContainerRetrievalOrderId> {
        private readonly IAggregateStore _aggregateStore;

        public PlaceContainerRetrievalOrderCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<ContainerRetrievalOrderId> Handle(PlaceContainerRetrievalOrderCommand cmd,
            CancellationToken cancellationToken) {
            var project = await _aggregateStore.Load(new ProjectId(cmd.ProjectId));

            if (project is null) {
                throw new ProjectNotFoundException();
            }

            var projectSnapshot = project.GetSnapshot();
            var containerId = new ContainerId(cmd.ContainerId);

            var order = ContainerRetrievalOrder.PlaceContainerRetrievalOrder(projectSnapshot, containerId);

            _aggregateStore.AppendChanges(order);

            return order.Id;
        }
    }
}