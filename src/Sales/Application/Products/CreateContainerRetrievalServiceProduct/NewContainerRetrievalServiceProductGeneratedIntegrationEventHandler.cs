﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Catalog.IntegrationEvents;
using HLS.Sales.Application.Configuration.Commands;
using MediatR;

namespace HLS.Sales.Application.Products.CreateContainerRetrievalServiceProduct {
    internal class
        NewContainerRetrievalServiceProductGeneratedIntegrationEventHandler : INotificationHandler<
            NewContainerRetrievalServiceProductGeneratedIntegrationEvent> {
        private readonly ICommandsScheduler _commandsScheduler;

        public NewContainerRetrievalServiceProductGeneratedIntegrationEventHandler(
            ICommandsScheduler commandsScheduler) {
            _commandsScheduler = commandsScheduler;
        }

        public async Task Handle(
            NewContainerRetrievalServiceProductGeneratedIntegrationEvent @event,
            CancellationToken cancellationToken) {
            var cmd = new CreateContainerRetrievalServiceProductCommand(
                @event.ProductId,
                @event.Sku,
                @event.ContainerTypeId,
                @event.ZoneId);

            await _commandsScheduler.EnqueueAsync(cmd);
        }
    }
}