﻿using System;
using HLS.Sales.Application.Configuration.Commands;

namespace HLS.Sales.Application.Products.CreateContainerRetrievalServiceProduct {
    public class CreateContainerRetrievalServiceProductCommand : InternalCommandBase {
        public CreateContainerRetrievalServiceProductCommand(
            Guid productId,
            string sku,
            Guid containerTypeId,
            Guid zoneId) {
            ProductId = productId;
            Sku = sku;
            ContainerTypeId = containerTypeId;
            ZoneId = zoneId;
        }

        public Guid ProductId { get; }

        public string Sku { get; }

        public Guid ContainerTypeId { get; }

        public Guid ZoneId { get; }
    }
}