﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.ContainerTypes;
using HLS.Sales.Domain.Products;
using HLS.Sales.Domain.SeedWork;
using HLS.Sales.Domain.Zones;
using MediatR;

namespace HLS.Sales.Application.Products.CreateContainerRetrievalServiceProduct {
    internal class
        CreateContainerRetrievalServiceProductCommandHandler : ICommandHandler<
            CreateContainerRetrievalServiceProductCommand> {
        private readonly IAggregateStore _aggregateStore;

        public CreateContainerRetrievalServiceProductCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public Task<Unit> Handle(CreateContainerRetrievalServiceProductCommand cmd,
            CancellationToken cancellationToken) {
            var productId = new ContainerRetrievalServiceProductId(cmd.ProductId);
            var sku = ProductSku.Of(cmd.Sku);
            var containerTypeId = new ContainerTypeId(cmd.ContainerTypeId);
            var zoneId = new ZoneId(cmd.ZoneId);

            var product = ContainerRetrievalServiceProduct.Create(
                productId,
                sku,
                containerTypeId,
                zoneId);

            _aggregateStore.AppendChanges(product);

            return Task.FromResult(Unit.Value);
        }
    }
}