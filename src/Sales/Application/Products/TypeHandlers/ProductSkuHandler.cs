﻿using System.Data;
using Dapper;
using HLS.Sales.Domain.Products;

namespace HLS.Sales.Application.Products.TypeHandlers {
    public class ProductSkuHandler : SqlMapper.TypeHandler<ProductSku> {
        public override void SetValue(IDbDataParameter parameter, ProductSku type) {
            parameter.Value = type.Code;
            parameter.DbType = DbType.String;
        }

        public override ProductSku Parse(object value) {
            return ProductSku.Of(value.ToString());
        }
    }
}