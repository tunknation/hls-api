﻿using System;
using System.Data;
using Dapper;
using HLS.Sales.Domain.Products;

namespace HLS.Sales.Application.Products.TypeHandlers {
    public class ContainerDeliveryServiceProductIdHandler : SqlMapper.TypeHandler<ContainerDeliveryServiceProductId> {
        public override void SetValue(IDbDataParameter parameter, ContainerDeliveryServiceProductId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override ContainerDeliveryServiceProductId Parse(object value) {
            return new ContainerDeliveryServiceProductId(Guid.Parse(value.ToString()!));
        }
    }
}