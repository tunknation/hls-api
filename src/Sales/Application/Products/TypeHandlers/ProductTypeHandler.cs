﻿using System.Data;
using Dapper;
using HLS.Sales.Domain.Products;

namespace HLS.Sales.Application.Products.TypeHandlers {
    public class ProductTypeHandler : SqlMapper.TypeHandler<ProductType> {
        public override void SetValue(IDbDataParameter parameter, ProductType type) {
            parameter.Value = type.Code;
            parameter.DbType = DbType.String;
        }

        public override ProductType Parse(object value) {
            return ProductType.Of(value.ToString());
        }
    }
}