﻿using System;
using System.Data;
using Dapper;
using HLS.Sales.Domain.Products;

namespace HLS.Sales.Application.Products.TypeHandlers {
    public class ContainerRetrievalServiceProductIdHandler : SqlMapper.TypeHandler<ContainerRetrievalServiceProductId> {
        public override void SetValue(IDbDataParameter parameter, ContainerRetrievalServiceProductId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override ContainerRetrievalServiceProductId Parse(object value) {
            return new ContainerRetrievalServiceProductId(Guid.Parse(value.ToString()!));
        }
    }
}