﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Application.Products.Exceptions;
using HLS.Sales.Domain.SeedWork;
using MediatR;

namespace HLS.Sales.Application.Products.AdjustContainerDeliveryServiceProductPrice {
    internal class
        AdjustContainerDeliveryServiceProductPriceCommandHandler : ICommandHandler<
            AdjustContainerDeliveryServiceProductPriceCommand> {
        private readonly IAggregateStore _aggregateStore;

        public AdjustContainerDeliveryServiceProductPriceCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(
            AdjustContainerDeliveryServiceProductPriceCommand cmd,
            CancellationToken cancellationToken) {
            var product = await _aggregateStore.Load(cmd.ProductId);

            if (product is null) {
                throw new ProductNotFoundException();
            }

            product.AdjustProductPrice(cmd.Price);

            _aggregateStore.AppendChanges(product);

            return Unit.Value;
        }
    }
}