﻿using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.Products;
using HLS.Sales.Domain.SharedKernel;

namespace HLS.Sales.Application.Products.AdjustContainerDeliveryServiceProductPrice {
    public class AdjustContainerDeliveryServiceProductPriceCommand : InternalCommandBase {
        public AdjustContainerDeliveryServiceProductPriceCommand(
            ContainerDeliveryServiceProductId productId,
            NorwegianKrone price) {
            ProductId = productId;
            Price = price;
        }

        public ContainerDeliveryServiceProductId ProductId { get; }

        public NorwegianKrone Price { get; }
    }
}