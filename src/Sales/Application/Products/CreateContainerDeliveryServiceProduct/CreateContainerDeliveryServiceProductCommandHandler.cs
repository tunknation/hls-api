﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.ContainerTypes;
using HLS.Sales.Domain.Products;
using HLS.Sales.Domain.SeedWork;
using HLS.Sales.Domain.Zones;
using MediatR;

namespace HLS.Sales.Application.Products.CreateContainerDeliveryServiceProduct {
    internal class
        CreateContainerDeliveryServiceProductCommandHandler : ICommandHandler<
            CreateContainerDeliveryServiceProductCommand> {
        private readonly IAggregateStore _aggregateStore;

        public CreateContainerDeliveryServiceProductCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public Task<Unit> Handle(CreateContainerDeliveryServiceProductCommand cmd,
            CancellationToken cancellationToken) {
            var productId = new ContainerDeliveryServiceProductId(cmd.ProductId);
            var sku = ProductSku.Of(cmd.Sku);
            var containerTypeId = new ContainerTypeId(cmd.ContainerTypeId);
            var zoneId = new ZoneId(cmd.ZoneId);

            var product = ContainerDeliveryServiceProduct.Create(
                productId,
                sku,
                containerTypeId,
                zoneId);

            _aggregateStore.AppendChanges(product);

            return Task.FromResult(Unit.Value);
        }
    }
}