﻿using System;
using HLS.Sales.Application.Configuration.Commands;

namespace HLS.Sales.Application.Products.CreateContainerDeliveryServiceProduct {
    public class CreateContainerDeliveryServiceProductCommand : InternalCommandBase {
        public CreateContainerDeliveryServiceProductCommand(
            Guid productId,
            string sku,
            Guid containerTypeId,
            Guid zoneId) {
            ProductId = productId;
            Sku = sku;
            ContainerTypeId = containerTypeId;
            ZoneId = zoneId;
        }

        public Guid ProductId { get; }

        public string Sku { get; }

        public Guid ContainerTypeId { get; }

        public Guid ZoneId { get; }
    }
}