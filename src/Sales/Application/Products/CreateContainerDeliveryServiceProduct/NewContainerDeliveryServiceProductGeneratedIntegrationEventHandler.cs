﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Catalog.IntegrationEvents;
using HLS.Sales.Application.Configuration.Commands;
using MediatR;

namespace HLS.Sales.Application.Products.CreateContainerDeliveryServiceProduct {
    internal class
        NewContainerDeliveryServiceProductGeneratedIntegrationEventHandler : INotificationHandler<
            NewContainerDeliveryServiceProductGeneratedIntegrationEvent> {
        private readonly ICommandsScheduler _commandsScheduler;

        public NewContainerDeliveryServiceProductGeneratedIntegrationEventHandler(
            ICommandsScheduler commandsScheduler) {
            _commandsScheduler = commandsScheduler;
        }

        public async Task Handle(
            NewContainerDeliveryServiceProductGeneratedIntegrationEvent @event,
            CancellationToken cancellationToken) {
            var cmd = new CreateContainerDeliveryServiceProductCommand(
                @event.ProductId,
                @event.Sku,
                @event.ContainerTypeId,
                @event.ZoneId);

            await _commandsScheduler.EnqueueAsync(cmd);
        }
    }
}