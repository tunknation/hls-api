﻿using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Domain.Products;
using HLS.Sales.Domain.SharedKernel;

namespace HLS.Sales.Application.Products.AdjustContainerRetrievalServiceProductPrice {
    public class AdjustContainerRetrievalServiceProductPriceCommand : InternalCommandBase {
        public AdjustContainerRetrievalServiceProductPriceCommand(
            ContainerRetrievalServiceProductId productId,
            NorwegianKrone price) {
            ProductId = productId;
            Price = price;
        }

        public ContainerRetrievalServiceProductId ProductId { get; }

        public NorwegianKrone Price { get; }
    }
}