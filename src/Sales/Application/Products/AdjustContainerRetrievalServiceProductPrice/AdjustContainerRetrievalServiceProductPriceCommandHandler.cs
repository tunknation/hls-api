﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Application.Products.Exceptions;
using HLS.Sales.Domain.SeedWork;
using MediatR;

namespace HLS.Sales.Application.Products.AdjustContainerRetrievalServiceProductPrice {
    internal class
        AdjustContainerRetrievalServiceProductPriceCommandHandler : ICommandHandler<
            AdjustContainerRetrievalServiceProductPriceCommand> {
        private readonly IAggregateStore _aggregateStore;

        public AdjustContainerRetrievalServiceProductPriceCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(
            AdjustContainerRetrievalServiceProductPriceCommand cmd,
            CancellationToken cancellationToken) {
            var product = await _aggregateStore.Load(cmd.ProductId);

            if (product is null) {
                throw new ProductNotFoundException();
            }

            product.AdjustProductPrice(cmd.Price);

            _aggregateStore.AppendChanges(product);

            return Unit.Value;
        }
    }
}