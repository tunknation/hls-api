﻿using System;
using HLS.Sales.Application.Configuration.Commands;

namespace HLS.Sales.Application.Products.AdjustProductPrice {
    public class AdjustProductPriceCommand : CommandBase {
        public AdjustProductPriceCommand(Guid productId, decimal price) {
            ProductId = productId;
            Price = price;
        }

        public Guid ProductId { get; }

        public decimal Price { get; }
    }
}