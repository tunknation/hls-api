﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Application.Products.AdjustContainerDeliveryServiceProductPrice;
using HLS.Sales.Application.Products.AdjustContainerRetrievalServiceProductPrice;
using HLS.Sales.Application.Products.Exceptions;
using HLS.Sales.Domain.Products;
using HLS.Sales.Domain.SharedKernel;
using MediatR;

namespace HLS.Sales.Application.Products.AdjustProductPrice {
    internal class AdjustProductPriceCommandHandler : ICommandHandler<AdjustProductPriceCommand> {
        private readonly IDbConnection _connection;

        private readonly ICommandsScheduler _commandsScheduler;

        public AdjustProductPriceCommandHandler(ISqlConnectionFactory sqlConnectionFactory,
            ICommandsScheduler commandsScheduler) {
            _connection = sqlConnectionFactory.GetOpenConnection();
            _commandsScheduler = commandsScheduler;
        }

        public async Task<Unit> Handle(AdjustProductPriceCommand cmd, CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [P].[Type]
                FROM [sales].[v_Product] AS [P]
                WHERE [P].[Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", cmd.ProductId);

            var type = await _connection.QuerySingleOrDefaultAsync<ProductType>(sql, parameters);

            if (type is null) {
                throw new ProductNotFoundException();
            }

            var productId = new ProductId(cmd.ProductId);
            var price = NorwegianKrone.Of(cmd.Price);

            InternalCommandBase adjustPriceCmd = type.Code switch {
                nameof(ProductType.ContainerDeliveryServiceProduct) =>
                    new AdjustContainerDeliveryServiceProductPriceCommand(productId, price),
                nameof(ProductType.ContainerRetrievalServiceProduct) =>
                    new AdjustContainerRetrievalServiceProductPriceCommand(productId, price),
                _ => throw new Exception("Product type is not supported")
            };

            await _commandsScheduler.EnqueueAsync(adjustPriceCmd);

            return Unit.Value;
        }
    }
}