﻿using System;
using HLS.Sales.Application.Configuration.Queries;
using HLS.Sales.Domain.SharedKernel;

namespace HLS.Sales.Application.Products.GetProductPrice {
    public class GetProductPriceQuery : QueryBase<NorwegianKrone> {
        public GetProductPriceQuery(Guid productId) {
            ProductId = productId;
        }

        public Guid ProductId { get; }
    }
}