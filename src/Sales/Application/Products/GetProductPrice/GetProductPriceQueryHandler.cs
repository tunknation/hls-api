﻿using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Sales.Application.Configuration.Queries;
using HLS.Sales.Domain.SharedKernel;

namespace HLS.Sales.Application.Products.GetProductPrice {
    internal class GetProductPriceQueryHandler : IQueryHandler<GetProductPriceQuery, NorwegianKrone> {
        private readonly IDbConnection _connection;

        public GetProductPriceQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<NorwegianKrone> Handle(GetProductPriceQuery query, CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [P].[Price]
                FROM [sales].[v_Product] AS [P]
                WHERE [P].[Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", query.ProductId);

            return await _connection.QuerySingleOrDefaultAsync<NorwegianKrone>(sql, parameters);
        }
    }
}