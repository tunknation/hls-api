﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Core.Domain;
using HLS.Sales.Application.Configuration.Projections;
using HLS.Sales.Domain.Products.Events;

namespace HLS.Sales.Application.Products.Projections {
    internal class ProductProjector : IProjector {
        private readonly IDbConnection _connection;

        public ProductProjector(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Project(IDomainEvent @event) {
            await _when((dynamic)@event);
        }

        private static Task _when(IDomainEvent @event) {
            return Task.CompletedTask;
        }

        private async Task _when(ContainerDeliveryServiceProductCreatedDomainEvent @event) {
            const string sql = @"
                INSERT INTO [sales].[Product]
                    ([Id], [Type], [Sku], [ContainerTypeId], [ZoneId], [Price])
                VALUES (@Id, @Type, @Sku, @ContainerTypeId, @ZoneId, @Price)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ProductId);
            parameters.Add("@Type", @event.Type);
            parameters.Add("@Sku", @event.Sku);
            parameters.Add("@ContainerTypeId", @event.ContainerTypeId);
            parameters.Add("@ZoneId", @event.ZoneId);
            parameters.Add("@Price", @event.Price);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerRetrievalServiceProductCreatedDomainEvent @event) {
            const string sql = @"
                INSERT INTO [sales].[Product]
                    ([Id], [Type], [Sku], [ContainerTypeId], [ZoneId], [Price])
                VALUES (@Id, @Type, @Sku, @ContainerTypeId, @ZoneId, @Price)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ProductId);
            parameters.Add("@Type", @event.Type);
            parameters.Add("@Sku", @event.Sku);
            parameters.Add("@ContainerTypeId", @event.ContainerTypeId);
            parameters.Add("@ZoneId", @event.ZoneId);
            parameters.Add("@Price", @event.Price);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerDeliveryServiceProductPriceAdjustedDomainEvent @event) {
            const string sql = @"
                UPDATE [sales].[Product]
                SET [Price] = @Price
                WHERE [Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ProductId);
            parameters.Add("@Price", @event.Price);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerRetrievalServiceProductPriceAdjustedDomainEvent @event) {
            const string sql = @"
                UPDATE [sales].[Product]
                SET [Price] = @Price
                WHERE [Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ProductId);
            parameters.Add("@Price", @event.Price);

            await _connection.ExecuteAsync(sql, parameters);
        }
    }
}