﻿using System;

namespace HLS.Sales.Application.Products.Exceptions {
    public class ProductNotFoundException : Exception {
        private const string ErrorMessage = "Product not found";

        public ProductNotFoundException() : base(ErrorMessage) { }
    }
}