﻿using System;
using HLS.Core.EventBus;

namespace HLS.Sales.IntegrationEvents {
    public class ContainerDeliveryOrderFulfilledIntegrationEvent : IntegrationEvent {
        public ContainerDeliveryOrderFulfilledIntegrationEvent(
            Guid id,
            DateTime occurredOn,
            Guid orderId,
            Guid customerId,
            Guid containerTypeId,
            DateTime fulfillmentDate,
            string city,
            string postalCode,
            string streetAddress) : base(id, occurredOn) {
            OrderId = orderId;
            CustomerId = customerId;
            ContainerTypeId = containerTypeId;
            FulfillmentDate = fulfillmentDate;
            City = city;
            PostalCode = postalCode;
            StreetAddress = streetAddress;
        }

        public Guid OrderId { get; }

        public Guid CustomerId { get; }

        public Guid ContainerTypeId { get; }

        public DateTime FulfillmentDate { get; }

        public string City { get; }

        public string PostalCode { get; }

        public string StreetAddress { get; }
    }
}