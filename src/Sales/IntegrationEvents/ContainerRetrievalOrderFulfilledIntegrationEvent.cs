﻿using System;
using HLS.Core.EventBus;

namespace HLS.Sales.IntegrationEvents {
    public class ContainerRetrievalOrderFulfilledIntegrationEvent : IntegrationEvent {
        public ContainerRetrievalOrderFulfilledIntegrationEvent(
            Guid id,
            DateTime occurredOn,
            Guid orderId,
            Guid projectId,
            Guid containerId,
            DateTime fulfillmentDate) : base(id, occurredOn) {
            OrderId = orderId;
            ProjectId = projectId;
            ContainerId = containerId;
            FulfillmentDate = fulfillmentDate;
        }

        public Guid OrderId { get; }

        public Guid ProjectId { get; }

        public Guid ContainerId { get; }

        public DateTime FulfillmentDate { get; }
    }
}