﻿using HotChocolate.Types;

namespace HLS.Sales.API.GraphQL {
    [ExtendObjectType(Name = "Query")]
    public class HelloWorldQuery {
        public string GetHelloWorld() {
            return "Hello World!";
        }
    }
}