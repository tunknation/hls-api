﻿using System;
using System.Threading.Tasks;
using HLS.Sales.Application.Contracts;
using HLS.Sales.Application.Products.GetProductPrice;
using HotChocolate.Types;

namespace HLS.Sales.API.GraphQL.Products {
    [ExtendObjectType(Name = "Query")]
    public class ProductsQuery {
        private readonly ISalesModule _module;

        public ProductsQuery(ISalesModule module) {
            _module = module;
        }

        public async Task<decimal> GetProductPrice(Guid productId) {
            var query = new GetProductPriceQuery(productId);

            var price = await _module.ExecuteQueryAsync(query);

            if (price is null) {
                return decimal.Zero;
            }

            return (decimal)price;
        }
    }
}