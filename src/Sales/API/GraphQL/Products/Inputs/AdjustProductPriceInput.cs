﻿using System;

namespace HLS.Sales.API.GraphQL.Products.Inputs {
    public class AdjustProductPriceInput {
        public Guid ProductId { get; set; }

        public decimal Price { get; set; }
    }
}