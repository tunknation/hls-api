﻿namespace HLS.Sales.API.GraphQL.Products.Payloads {
    public class AdjustProductPricePayload {
        public bool Success { get; set; }
    }
}