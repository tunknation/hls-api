﻿using System.Threading.Tasks;
using HLS.Sales.API.GraphQL.Products.Inputs;
using HLS.Sales.API.GraphQL.Products.Payloads;
using HLS.Sales.Application.Contracts;
using HLS.Sales.Application.Products.AdjustProductPrice;
using HotChocolate.Types;

namespace HLS.Sales.API.GraphQL.Products {
    [ExtendObjectType(Name = "Mutation")]
    public class ProductsMutation {
        private readonly ISalesModule _module;

        public ProductsMutation(ISalesModule module) {
            _module = module;
        }

        public async Task<AdjustProductPricePayload> AdjustProductPrice(AdjustProductPriceInput input) {
            var cmd = new AdjustProductPriceCommand(
                input.ProductId,
                input.Price);

            await _module.ExecuteCommandAsync(cmd);

            return new AdjustProductPricePayload {
                Success = true
            };
        }
    }
}