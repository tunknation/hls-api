﻿using System;

namespace HLS.Sales.API.GraphQL.Orders.Inputs {
    public class FulfillContainerRetrievalOrderInput {
        public Guid OrderId { get; set; }

        public DateTime? FulfillmentDate { get; set; }
    }
}