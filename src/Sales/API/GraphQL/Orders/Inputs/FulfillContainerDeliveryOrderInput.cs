﻿using System;

namespace HLS.Sales.API.GraphQL.Orders.Inputs {
    public class FulfillContainerDeliveryOrderInput {
        public Guid OrderId { get; set; }

        public DateTime? FulfillmentDate { get; set; }
    }
}