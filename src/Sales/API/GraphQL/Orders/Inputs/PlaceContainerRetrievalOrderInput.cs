﻿using System;

namespace HLS.Sales.API.GraphQL.Orders.Inputs {
    public class PlaceContainerRetrievalOrderInput {
        public Guid ProjectId { get; set; }

        public Guid ContainerId { get; set; }
    }
}