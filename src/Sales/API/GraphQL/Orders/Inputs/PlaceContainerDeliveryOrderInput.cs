﻿using System;

namespace HLS.Sales.API.GraphQL.Orders.Inputs {
    public class PlaceContainerDeliveryOrderInput {
        public Guid CustomerId { get; set; }

        public Guid ContainerTypeId { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }

        public string StreetAddress { get; set; }
    }
}