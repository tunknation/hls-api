﻿using System;
using HLS.Sales.Domain.Orders;

namespace HLS.Sales.API.GraphQL.Orders.Types {
    public class ContainerRetrievalOrder : Order {
        public Guid ProjectId { get; set; }

        public Guid CustomerId { get; set; }

        public Guid ContainerId { get; set; }

        public OrderAddress Address { get; set; }

        public DateTime? FulfillmentDate { get; set; }
    }
}