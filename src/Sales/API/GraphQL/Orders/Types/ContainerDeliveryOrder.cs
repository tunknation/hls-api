﻿using System;
using HLS.Sales.Domain.Orders;

namespace HLS.Sales.API.GraphQL.Orders.Types {
    public class ContainerDeliveryOrder : Order {
        public Guid CustomerId { get; set; }

        public Guid ContainerTypeId { get; set; }

        public OrderAddress Address { get; set; }

        public DateTime? FulfillmentDate { get; set; }
    }
}