﻿using System;
using HLS.Sales.Application.Orders.Dtos;
using HotChocolate.Types;

namespace HLS.Sales.API.GraphQL.Orders.Types {
    [UnionType(Name = "Order")]
    public abstract class Order {
        public Guid Id { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }

        public static explicit operator Order(OrderDto orderDto) {
            return orderDto switch {
                ContainerDeliveryOrderDto dto => (ContainerDeliveryOrder)dto,
                ContainerRetrievalOrderDto dto => (ContainerRetrievalOrder)dto,
                _ => throw new Exception("Order type is not supported")
            };
        }

        public static explicit operator Order(ContainerDeliveryOrderDto dto) {
            return new ContainerDeliveryOrder {
                Id = dto.Id,
                Type = dto.Type,
                Status = dto.Status,
                CustomerId = dto.CustomerId,
                ContainerTypeId = dto.ContainerTypeId,
                Address = dto.Address,
                FulfillmentDate = dto.FulfillmentDate
            };
        }

        public static explicit operator Order(ContainerRetrievalOrderDto dto) {
            return new ContainerRetrievalOrder {
                Id = dto.Id,
                Type = dto.Type,
                Status = dto.Status,
                ProjectId = dto.ProjectId,
                CustomerId = dto.CustomerId,
                ContainerId = dto.ContainerId,
                Address = dto.Address,
                FulfillmentDate = dto.FulfillmentDate
            };
        }
    }
}