﻿using System.Threading.Tasks;
using HLS.Sales.API.GraphQL.Orders.Inputs;
using HLS.Sales.API.GraphQL.Orders.Payloads;
using HLS.Sales.Application.Contracts;
using HLS.Sales.Application.Orders.FulfillContainerDeliveryOrder;
using HLS.Sales.Application.Orders.FulfillContainerRetrievalOrder;
using HLS.Sales.Application.Orders.PlaceContainerDeliveryOrder;
using HLS.Sales.Application.Orders.PlaceContainerRetrievalOrder;
using HotChocolate.Types;

namespace HLS.Sales.API.GraphQL.Orders {
    [ExtendObjectType(Name = "Mutation")]
    public class OrdersMutation {
        private readonly ISalesModule _module;

        public OrdersMutation(ISalesModule module) {
            _module = module;
        }

        public async Task<PlaceContainerDeliveryOrderPayload> PlaceContainerDeliveryOrder(
            PlaceContainerDeliveryOrderInput input) {
            var cmd = new PlaceContainerDeliveryOrderCommand(
                input.CustomerId,
                input.ContainerTypeId,
                input.City,
                input.PostalCode,
                input.StreetAddress);

            var orderId = await _module.ExecuteCommandAsync(cmd);

            return new PlaceContainerDeliveryOrderPayload {
                Success = true,
                OrderId = orderId.Value
            };
        }

        public async Task<FulfillContainerDeliveryOrderPayload> FulfillContainerDeliveryOrder(
            FulfillContainerDeliveryOrderInput input) {
            var cmd = new FulfillContainerDeliveryOrderCommand(
                input.OrderId,
                input.FulfillmentDate);

            await _module.ExecuteCommandAsync(cmd);

            return new FulfillContainerDeliveryOrderPayload {
                Success = true
            };
        }

        public async Task<PlaceContainerRetrievalOrderPayload> PlaceContainerRetrievalOrder(
            PlaceContainerRetrievalOrderInput input) {
            var cmd = new PlaceContainerRetrievalOrderCommand(
                input.ProjectId,
                input.ContainerId);

            var orderId = await _module.ExecuteCommandAsync(cmd);

            return new PlaceContainerRetrievalOrderPayload {
                Success = true,
                OrderId = orderId.Value
            };
        }

        public async Task<FulfillContainerRetrievalOrderPayload> FulfillContainerRetrievalOrder(
            FulfillContainerRetrievalOrderInput input) {
            var cmd = new FulfillContainerRetrievalOrderCommand(
                input.OrderId,
                input.FulfillmentDate);

            await _module.ExecuteCommandAsync(cmd);

            return new FulfillContainerRetrievalOrderPayload {
                Success = true
            };
        }
    }
}