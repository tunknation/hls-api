﻿namespace HLS.Sales.API.GraphQL.Orders.Payloads {
    public class FulfillContainerDeliveryOrderPayload {
        public bool Success { get; set; }
    }
}