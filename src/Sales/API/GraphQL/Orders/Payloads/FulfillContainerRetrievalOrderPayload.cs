﻿namespace HLS.Sales.API.GraphQL.Orders.Payloads {
    public class FulfillContainerRetrievalOrderPayload {
        public bool Success { get; set; }
    }
}