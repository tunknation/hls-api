﻿using System;

namespace HLS.Sales.API.GraphQL.Orders.Payloads {
    public class PlaceContainerRetrievalOrderPayload {
        public bool Success { get; set; }

        public Guid OrderId { get; set; }
    }
}