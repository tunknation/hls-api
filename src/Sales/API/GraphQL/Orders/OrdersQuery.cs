﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HLS.Sales.API.GraphQL.Orders.Types;
using HLS.Sales.Application.Contracts;
using HLS.Sales.Application.Orders.GetOrder;
using HLS.Sales.Application.Orders.GetOrders;
using HotChocolate.Types;

namespace HLS.Sales.API.GraphQL.Orders {
    [ExtendObjectType(Name = "Query")]
    public class OrdersQuery {
        private readonly ISalesModule _module;

        public OrdersQuery(ISalesModule module) {
            _module = module;
        }

        public async Task<Order> GetOrder(Guid orderId) {
            var query = new GetOrderQuery(orderId);

            var order = await _module.ExecuteQueryAsync(query);

            return (Order)order;
        }

        public async Task<List<Order>> GetOrders(int? page, int? perPage) {
            var query = new GetOrdersQuery(page, perPage);

            var orders = await _module.ExecuteQueryAsync(query);

            return (from order in orders select (Order)order).ToList();
        }
    }
}