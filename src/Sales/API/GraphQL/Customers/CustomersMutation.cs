﻿using System.Threading.Tasks;
using HLS.Sales.API.GraphQL.Customers.Payloads;
using HLS.Sales.Application.Contracts;
using HLS.Sales.Application.Customers.RegisterNewBusinessCustomer;
using HLS.Sales.Application.Customers.RegisterNewPrivateCustomer;
using HotChocolate.Types;

namespace HLS.Sales.API.GraphQL.Customers {
    [ExtendObjectType(Name = "Mutation")]
    public class CustomersMutation {
        private readonly ISalesModule _module;

        public CustomersMutation(ISalesModule module) {
            _module = module;
        }

        public async Task<RegisterNewBusinessCustomerPayload> RegisterNewBusinessCustomer() {
            var cmd = new RegisterNewBusinessCustomerCommand();

            var customerId = await _module.ExecuteCommandAsync(cmd);

            return new RegisterNewBusinessCustomerPayload {
                Success = true,
                CustomerId = customerId.Value
            };
        }

        public async Task<RegisterNewPrivateCustomerPayload> RegisterNewPrivateCustomer() {
            var cmd = new RegisterNewPrivateCustomerCommand();

            var customerId = await _module.ExecuteCommandAsync(cmd);

            return new RegisterNewPrivateCustomerPayload {
                Success = true,
                CustomerId = customerId.Value
            };
        }
    }
}