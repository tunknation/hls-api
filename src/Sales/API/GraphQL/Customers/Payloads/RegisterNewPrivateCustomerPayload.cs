﻿using System;

namespace HLS.Sales.API.GraphQL.Customers.Payloads {
    public class RegisterNewPrivateCustomerPayload {
        public bool Success { get; set; }

        public Guid CustomerId { get; set; }
    }
}