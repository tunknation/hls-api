﻿using System;
using HLS.Sales.Application.Customers.Dtos;
using HotChocolate.Types;

namespace HLS.Sales.API.GraphQL.Customers.Types {
    [UnionType(Name = "Customer")]
    public abstract class Customer {
        public Guid Id { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }

        public static explicit operator Customer(CustomerDto customerDto) {
            return customerDto switch {
                BusinessCustomerDto dto => (BusinessCustomer)dto,
                PrivateCustomerDto dto => (PrivateCustomer)dto,
                _ => throw new Exception("Customer type is not supported")
            };
        }

        public static explicit operator Customer(BusinessCustomerDto dto) {
            return new BusinessCustomer {
                Id = dto.Id,
                Type = dto.Type,
                Status = dto.Status
            };
        }

        public static explicit operator Customer(PrivateCustomerDto dto) {
            return new PrivateCustomer {
                Id = dto.Id,
                Type = dto.Type,
                Status = dto.Status
            };
        }
    }
}