﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HLS.Sales.API.GraphQL.Customers.Types;
using HLS.Sales.Application.Contracts;
using HLS.Sales.Application.Customers.GetCustomer;
using HLS.Sales.Application.Customers.GetCustomers;
using HotChocolate.Types;

namespace HLS.Sales.API.GraphQL.Customers {
    [ExtendObjectType(Name = "Query")]
    public class CustomersQuery {
        private readonly ISalesModule _module;

        public CustomersQuery(ISalesModule module) {
            _module = module;
        }

        public async Task<Customer> GetCustomer(Guid customerId) {
            var query = new GetCustomerQuery(customerId);

            var customer = await _module.ExecuteQueryAsync(query);

            return (Customer)customer;
        }

        public async Task<List<Customer>> GetCustomers(int? page, int? perPage) {
            var query = new GetCustomersQuery(page, perPage);

            var customers = await _module.ExecuteQueryAsync(query);

            return (from customer in customers select (Customer)customer).ToList();
        }
    }
}