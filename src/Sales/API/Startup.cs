using System;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using HLS.Core.EventBus;
using HLS.Sales.API.GraphQL;
using HLS.Sales.API.GraphQL.Customers;
using HLS.Sales.API.GraphQL.Customers.Types;
using HLS.Sales.API.GraphQL.Orders;
using HLS.Sales.API.GraphQL.Orders.Types;
using HLS.Sales.API.GraphQL.Products;
using HLS.Sales.API.Modules;
using HLS.Sales.Infrastructure;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Serilog.Formatting.Compact;
using StackExchange.Redis;

namespace HLS.Sales.API {
    public class Startup {
        private const string ConnectionString = "ConnectionString";
        private const string RedisUri = "RedisUri";

        private static ILogger _logger;
        private static ILogger _loggerForApi;

        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _configuration;

        public Startup(IWebHostEnvironment env) {
            ConfigureLogger();

            _env = env;
            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json")
                .AddUserSecrets<Startup>()
                .AddEnvironmentVariables("Sales_")
                .Build();
        }

        public void ConfigureServices(IServiceCollection services) {
            services.AddCors();

            services
                .AddAuthentication(options => {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options => {
                    options.Authority = _configuration["Oidc:Authority"];
                    options.Audience = _configuration["Oidc:ClientId"];
                    options.IncludeErrorDetails = true;

                    if (_env.IsDevelopment()) {
                        options.RequireHttpsMetadata = false;
                    }

                    options.TokenValidationParameters = new TokenValidationParameters {
                        ValidateAudience = false,
                        ValidateIssuer = true,
                        ValidIssuer = _configuration["Oidc:Authority"],
                        ValidateLifetime = true
                    };

                    options.Events = new JwtBearerEvents {
                        OnAuthenticationFailed = context => {
                            context.NoResult();
                            context.Response.StatusCode = StatusCodes.Status401Unauthorized;

                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddAuthorization();

            services.AddMassTransit(x => { x.UsingRabbitMq((context, cfg) => { cfg.ConfigureEndpoints(context); }); });

            services.AddMassTransitHostedService();

            services.AddSingleton(ConnectionMultiplexer.Connect(_configuration[RedisUri]));

            services
                .AddGraphQLServer()
                .AddAuthorization()
                .AddQueryType(d => d.Name("Query"))
                .AddMutationType(d => d.Name("Mutation"))
                .AddType<HelloWorldQuery>()
                .AddType<CustomersQuery>()
                .AddType<OrdersQuery>()
                .AddType<ProductsQuery>()
                .AddType<OrdersMutation>()
                .AddType<CustomersMutation>()
                .AddType<ProductsMutation>()
                .AddType<BusinessCustomer>()
                .AddType<PrivateCustomer>()
                .AddType<ContainerDeliveryOrder>()
                .AddType<ContainerRetrievalOrder>()
                .InitializeOnStartup()
                .PublishSchemaDefinition(c => c
                    .SetName("Sales")
                    .IgnoreRootTypes()
                    .AddTypeExtensionsFromFile("./GraphQL/Stitching.gql")
                    .PublishToRedis(
                        "Hls",
                        sp => sp.GetRequiredService<ConnectionMultiplexer>()));
        }

        public void ConfigureContainer(ContainerBuilder containerBuilder) {
            containerBuilder.RegisterType<MassTransitClient>()
                .As<IEventsBus>()
                .SingleInstance();

            containerBuilder.RegisterModule(new SalesAutofacModule());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider) {
            var container = app.ApplicationServices.GetAutofacRoot();

            app.UseCors(builder =>
                builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            InitializeModules(container);

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapGraphQL(); });
        }

        private static void ConfigureLogger() {
            _logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.Console(
                    outputTemplate:
                    "[{Timestamp:HH:mm:ss} {Level:u3}] [{Module}] [{Context}] {Message:lj}{NewLine}{Exception}")
                .WriteTo.File(new CompactJsonFormatter(), "logs/logs")
                .CreateLogger();

            _loggerForApi = _logger.ForContext("Module", "SalesAPI");

            _loggerForApi.Information("Logger configured");
        }

        private void InitializeModules(IComponentContext container) {
            var eventBus = container.Resolve<IEventsBus>();

            SalesStartup.Initialize(
                _configuration[ConnectionString],
                _logger,
                eventBus);
        }
    }
}