﻿using Autofac;
using Dapper;
using HLS.Core.EventBus;
using HLS.Sales.Application.Containers.TypeHandlers;
using HLS.Sales.Application.ContainerTypes.TypeHandlers;
using HLS.Sales.Application.Customers.TypeHandlers;
using HLS.Sales.Application.Orders.TypeHandlers;
using HLS.Sales.Application.Products.TypeHandlers;
using HLS.Sales.Application.Projects.TypeHandlers;
using HLS.Sales.Application.SharedKernel.TypeHandlers;
using HLS.Sales.Application.Zones.TypeHandlers;
using HLS.Sales.Infrastructure.AggregateStore;
using HLS.Sales.Infrastructure.Configuration.DataAccess;
using HLS.Sales.Infrastructure.Configuration.EventsBus;
using HLS.Sales.Infrastructure.Configuration.Logging;
using HLS.Sales.Infrastructure.Configuration.Mediation;
using HLS.Sales.Infrastructure.Configuration.Processing;
using HLS.Sales.Infrastructure.Configuration.Processing.Outbox;
using HLS.Sales.Infrastructure.Configuration.Quartz;
using Serilog;
using Serilog.Extensions.Logging;

namespace HLS.Sales.Infrastructure {
    public static class SalesStartup {
        private static IContainer _container;

        private static SubscriptionsManager _subscriptionsManager;

        public static void Initialize(
            string connectionString,
            ILogger logger,
            IEventsBus eventsBus,
            bool runQuartz = true) {
            var moduleLogger = logger.ForContext("Module", "Sales");

            ConfigureCompositionRoot(connectionString, moduleLogger, eventsBus, runQuartz);

            if (runQuartz) {
                QuartzStartup.Initialize(moduleLogger);
            }

            EventsBusStartup.Initialize(moduleLogger);
        }

        public static void Stop() {
            _subscriptionsManager.Stop();
            QuartzStartup.StopQuartz();
        }

        private static void ConfigureCompositionRoot(
            string connectionString,
            ILogger logger,
            IEventsBus eventsBus,
            bool runQuartz = true) {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new LoggingModule(logger));

            var loggerFactory = new SerilogLoggerFactory(logger);
            containerBuilder.RegisterModule(new DataAccessModule(connectionString, loggerFactory));

            containerBuilder.RegisterModule(new ProcessingModule());
            containerBuilder.RegisterModule(new EventsBusModule(eventsBus));
            containerBuilder.RegisterModule(new MediatorModule());
            containerBuilder.RegisterModule(new OutboxModule());

            if (runQuartz) {
                containerBuilder.RegisterModule(new QuartzModule());
            }

            _container = containerBuilder.Build();

            SalesCompositionRoot.SetContainer(_container);

            AddSqlTypeHandlers();

            RunEventsProjectors();
        }

        private static void AddSqlTypeHandlers() {
            SqlMapper.AddTypeHandler(new ContainerIdHandler());
            SqlMapper.AddTypeHandler(new ContainerTypeIdHandler());
            SqlMapper.AddTypeHandler(new BusinessCustomerIdHandler());
            SqlMapper.AddTypeHandler(new CustomerIdHandler());
            SqlMapper.AddTypeHandler(new CustomerStatusHandler());
            SqlMapper.AddTypeHandler(new CustomerTypeHandler());
            SqlMapper.AddTypeHandler(new PrivateCustomerIdHandler());
            SqlMapper.AddTypeHandler(new ContainerDeliveryOrderIdHandler());
            SqlMapper.AddTypeHandler(new ContainerRetrievalOrderIdHandler());
            SqlMapper.AddTypeHandler(new OrderAddressHandler());
            SqlMapper.AddTypeHandler(new OrderIdHandler());
            SqlMapper.AddTypeHandler(new OrderStatusHandler());
            SqlMapper.AddTypeHandler(new OrderTypeHandler());
            SqlMapper.AddTypeHandler(new ContainerDeliveryServiceProductIdHandler());
            SqlMapper.AddTypeHandler(new ContainerRetrievalServiceProductIdHandler());
            SqlMapper.AddTypeHandler(new ProductIdHandler());
            SqlMapper.AddTypeHandler(new ProductSkuHandler());
            SqlMapper.AddTypeHandler(new ProductTypeHandler());
            SqlMapper.AddTypeHandler(new ProjectAddressHandler());
            SqlMapper.AddTypeHandler(new ProjectIdHandler());
            SqlMapper.AddTypeHandler(new ZoneIdHandler());
            SqlMapper.AddTypeHandler(new NorwegianKroneHandler());
        }

        private static void RunEventsProjectors() {
            _subscriptionsManager = _container.Resolve<SubscriptionsManager>();

            _subscriptionsManager.Start();
        }
    }
}