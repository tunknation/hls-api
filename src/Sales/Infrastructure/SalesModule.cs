﻿using System.Threading.Tasks;
using Autofac;
using HLS.Sales.Application.Configuration.Commands;
using HLS.Sales.Application.Configuration.Queries;
using HLS.Sales.Application.Contracts;
using HLS.Sales.Infrastructure.Configuration.Processing;
using MediatR;

namespace HLS.Sales.Infrastructure {
    public class SalesModule : ISalesModule {
        public async Task<TResult> ExecuteCommandAsync<TResult>(ICommand<TResult> command) {
            return await CommandsExecutor.Execute(command);
        }

        public async Task ExecuteCommandAsync(ICommand command) {
            await CommandsExecutor.Execute(command);
        }

        public async Task<TResult> ExecuteQueryAsync<TResult>(IQuery<TResult> query) {
            using var scope = SalesCompositionRoot.BeginLifetimeScope();
            var mediator = scope.Resolve<IMediator>();

            return await mediator.Send(query);
        }
    }
}