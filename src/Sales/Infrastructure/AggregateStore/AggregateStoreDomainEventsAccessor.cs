﻿using System.Collections.Generic;
using System.Linq;
using HLS.Core.Domain;
using HLS.Core.Infrastructure.DomainEvents;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Infrastructure.AggregateStore {
    public class AggregateStoreDomainEventsAccessor : IDomainEventsAccessor {
        private readonly IAggregateStore _aggregateStore;

        public AggregateStoreDomainEventsAccessor(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public IReadOnlyCollection<IDomainEvent> GetAllDomainEvents() {
            return _aggregateStore
                .GetChanges()
                .ToList()
                .AsReadOnly();
        }

        public void ClearAllDomainEvents() {
            _aggregateStore.ClearChanges();
        }
    }
}