﻿using System.Threading.Tasks;

namespace HLS.Sales.Infrastructure.AggregateStore {
    public interface ICheckpointStore {
        long? GetCheckpoint(SubscriptionCode subscriptionCode);

        Task StoreCheckpoint(SubscriptionCode subscriptionCode, long checkpoint);
    }
}