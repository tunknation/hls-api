﻿using System;
using System.Collections.Generic;
using HLS.Sales.Domain.Customers.Events;
using HLS.Sales.Domain.Orders.Events;
using HLS.Sales.Domain.Products.Events;
using HLS.Sales.Domain.Projects.Events;

namespace HLS.Sales.Infrastructure.AggregateStore {
    internal static class DomainEventTypeMappings {
        static DomainEventTypeMappings() {
            Dictionary = new Dictionary<string, Type> {
                { nameof(NewBusinessCustomerRegisteredDomainEvent), typeof(NewBusinessCustomerRegisteredDomainEvent) },
                { nameof(NewPrivateCustomerRegisteredDomainEvent), typeof(NewPrivateCustomerRegisteredDomainEvent) },
                { nameof(ContainerDeliveryOrderPlacedDomainEvent), typeof(ContainerDeliveryOrderPlacedDomainEvent) }, {
                    nameof(ContainerDeliveryOrderFulfilledDomainEvent),
                    typeof(ContainerDeliveryOrderFulfilledDomainEvent)
                },
                { nameof(ContainerRetrievalOrderPlacedDomainEvent), typeof(ContainerRetrievalOrderPlacedDomainEvent) },
                { nameof(ProjectCreatedDomainEvent), typeof(ProjectCreatedDomainEvent) }, {
                    nameof(ContainerRetrievalOrderFulfilledDomainEvent),
                    typeof(ContainerRetrievalOrderFulfilledDomainEvent)
                }, {
                    nameof(ContainerDeliveryServiceProductCreatedDomainEvent),
                    typeof(ContainerDeliveryServiceProductCreatedDomainEvent)
                }, {
                    nameof(ContainerRetrievalServiceProductCreatedDomainEvent),
                    typeof(ContainerRetrievalServiceProductCreatedDomainEvent)
                }, {
                    nameof(ContainerDeliveryServiceProductPriceAdjustedDomainEvent),
                    typeof(ContainerDeliveryServiceProductPriceAdjustedDomainEvent)
                }, {
                    nameof(ContainerRetrievalServiceProductPriceAdjustedDomainEvent),
                    typeof(ContainerRetrievalServiceProductPriceAdjustedDomainEvent)
                }
            };
        }

        internal static IDictionary<string, Type> Dictionary { get; }
    }
}