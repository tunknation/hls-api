﻿using System;
using HLS.Core.Infrastructure;
using HLS.Sales.Application.Orders.FulfillContainerDeliveryOrder;
using HLS.Sales.Application.Orders.FulfillContainerRetrievalOrder;

namespace HLS.Sales.Infrastructure.Configuration.Processing {
    internal static class DomainNotificationTypeMappings {
        static DomainNotificationTypeMappings() {
            Dictionary = new BiDictionary<string, Type>();

            Dictionary.Add(
                nameof(ContainerDeliveryOrderFulfilledNotification),
                typeof(ContainerDeliveryOrderFulfilledNotification));
            Dictionary.Add(
                nameof(ContainerRetrievalOrderFulfilledNotification),
                typeof(ContainerRetrievalOrderFulfilledNotification));
        }

        internal static BiDictionary<string, Type> Dictionary { get; }
    }
}