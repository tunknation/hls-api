﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using HLS.Core.Application.Events;
using HLS.Core.Infrastructure.DomainEvents;
using HLS.Core.Infrastructure.Outbox;
using HLS.Sales.Infrastructure.AggregateStore;

namespace HLS.Sales.Infrastructure.Configuration.Processing.Outbox {
    internal class OutboxModule : Module {
        protected override void Load(ContainerBuilder builder) {
            builder.RegisterType<SqlOutboxAccessor>()
                .As<IOutbox>()
                .FindConstructorsWith(new AllConstructorFinder())
                .InstancePerLifetimeScope();

            CheckMappings();

            builder.RegisterType<DomainNotificationsMapper>()
                .As<IDomainNotificationsMapper>()
                .FindConstructorsWith(new AllConstructorFinder())
                .WithParameter("domainNotificationsMap", DomainNotificationTypeMappings.Dictionary)
                .SingleInstance();
        }

        private static void CheckMappings() {
            var domainEventNotifications = Assemblies.Application
                .GetTypes()
                .Where(x => x.GetInterfaces().Contains(typeof(IDomainEventNotification)))
                .ToList();

            var notMappedNotifications = new List<Type>();
            foreach (var domainEventNotification in domainEventNotifications) {
                DomainNotificationTypeMappings.Dictionary.TryGetBySecond(domainEventNotification, out var name);

                if (name == null) {
                    notMappedNotifications.Add(domainEventNotification);
                }
            }

            if (notMappedNotifications.Any()) {
                throw new ApplicationException(
                    $"Domain Event Notifications {notMappedNotifications.Select(x => x.FullName).Aggregate((x, y) => x + "," + y)} not mapped");
            }
        }
    }
}