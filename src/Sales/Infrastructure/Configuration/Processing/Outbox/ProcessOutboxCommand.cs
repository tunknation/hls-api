﻿using HLS.Sales.Application.Configuration.Commands;

namespace HLS.Sales.Infrastructure.Configuration.Processing.Outbox {
    public class ProcessOutboxCommand : CommandBase, IRecurringCommand { }
}