﻿using HLS.Sales.Application.Configuration.Commands;

namespace HLS.Sales.Infrastructure.Configuration.Processing.Inbox {
    public class ProcessInboxCommand : CommandBase, IRecurringCommand { }
}