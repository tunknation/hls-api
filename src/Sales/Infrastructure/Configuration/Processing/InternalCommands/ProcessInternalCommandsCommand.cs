﻿using HLS.Sales.Application.Configuration.Commands;

namespace HLS.Sales.Infrastructure.Configuration.Processing.InternalCommands {
    internal class ProcessInternalCommandsCommand : CommandBase, IRecurringCommand { }
}