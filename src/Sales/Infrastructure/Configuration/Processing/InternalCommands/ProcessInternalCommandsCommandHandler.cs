﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Sales.Application.Configuration.Commands;
using MediatR;
using Newtonsoft.Json;
using Polly;

namespace HLS.Sales.Infrastructure.Configuration.Processing.InternalCommands {
    internal class ProcessInternalCommandsCommandHandler : ICommandHandler<ProcessInternalCommandsCommand> {
        private readonly ISqlConnectionFactory _sqlConnectionFactory;

        public ProcessInternalCommandsCommandHandler(
            ISqlConnectionFactory sqlConnectionFactory) {
            _sqlConnectionFactory = sqlConnectionFactory;
        }

        public async Task<Unit> Handle(ProcessInternalCommandsCommand command, CancellationToken cancellationToken) {
            var connection = _sqlConnectionFactory.GetOpenConnection();

            const string sql = @"SELECT [Command].[Id],
                                        [Command].[Type],
                                        [Command].[Data]
                                 FROM [sales].[InternalCommands] AS [Command]
                                 WHERE [Command].[ProcessedDate] IS NULL
                                 ORDER BY [Command].[EnqueueDate]";

            var internalCommands = await connection.QueryAsync<InternalCommandDto>(sql);
            var internalCommandsList = internalCommands.AsList();

            var policy = Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(new[] {
                    TimeSpan.FromSeconds(1),
                    TimeSpan.FromSeconds(2),
                    TimeSpan.FromSeconds(3)
                });

            const string sqlUpdateProcessedDate = @"UPDATE [sales].[InternalCommands]
                                                    SET [ProcessedDate] = @Date
                                                    WHERE [Id] = @Id";

            const string sqlUpdateError = @"UPDATE [sales].[InternalCommands]
                                            SET [ProcessedDate] = @Date,
                                                [Error] = @Error
                                            WHERE [Id] = @Id";

            foreach (var internalCommand in internalCommandsList) {
                var result = await policy.ExecuteAndCaptureAsync(() => ProcessCommand(internalCommand));

                if (result.Outcome == OutcomeType.Failure) {
                    await connection.ExecuteScalarAsync(
                        sqlUpdateError,
                        new {
                            Date = DateTime.UtcNow,
                            Error = result.FinalException.ToString(),
                            internalCommand.Id
                        });
                } else {
                    await connection.ExecuteScalarAsync(
                        sqlUpdateProcessedDate,
                        new {
                            Date = DateTime.UtcNow,
                            internalCommand.Id
                        });
                }
            }

            return Unit.Value;
        }

        private static async Task ProcessCommand(InternalCommandDto internalCommand) {
            var type = Assemblies.Application.GetType(internalCommand.Type);
            dynamic commandToProcess = JsonConvert.DeserializeObject(internalCommand.Data, type!);

            await CommandsExecutor.Execute(commandToProcess);
        }
    }
}