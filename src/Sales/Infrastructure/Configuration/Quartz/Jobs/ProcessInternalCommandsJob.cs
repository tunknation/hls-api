﻿using System.Threading.Tasks;
using HLS.Sales.Infrastructure.Configuration.Processing;
using HLS.Sales.Infrastructure.Configuration.Processing.InternalCommands;
using Quartz;

namespace HLS.Sales.Infrastructure.Configuration.Quartz.Jobs {
    [DisallowConcurrentExecution]
    public class ProcessInternalCommandsJob : IJob {
        public async Task Execute(IJobExecutionContext context) {
            await CommandsExecutor.Execute(new ProcessInternalCommandsCommand());
        }
    }
}