﻿using System.Threading.Tasks;
using HLS.Sales.Infrastructure.Configuration.Processing;
using HLS.Sales.Infrastructure.Configuration.Processing.Outbox;
using Quartz;

namespace HLS.Sales.Infrastructure.Configuration.Quartz.Jobs {
    [DisallowConcurrentExecution]
    public class ProcessOutboxJob : IJob {
        public async Task Execute(IJobExecutionContext context) {
            await CommandsExecutor.Execute(new ProcessOutboxCommand());
        }
    }
}