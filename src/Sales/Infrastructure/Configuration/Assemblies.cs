﻿using System.Reflection;
using HLS.Sales.Application.Configuration.Commands;

namespace HLS.Sales.Infrastructure.Configuration {
    internal static class Assemblies {
        public static readonly Assembly Application = typeof(InternalCommandBase).Assembly;
    }
}