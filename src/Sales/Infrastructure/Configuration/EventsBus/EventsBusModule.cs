﻿using Autofac;
using HLS.Core.EventBus;

namespace HLS.Sales.Infrastructure.Configuration.EventsBus {
    internal class EventsBusModule : Module {
        private readonly IEventsBus _eventsBus;

        public EventsBusModule(IEventsBus eventsBus) {
            _eventsBus = eventsBus;
        }

        protected override void Load(ContainerBuilder builder) {
            if (_eventsBus != null) {
                builder.RegisterInstance(_eventsBus)
                    .As<IEventsBus>();
            } else {
                builder.RegisterType<InMemoryEventBusClient>()
                    .As<IEventsBus>()
                    .SingleInstance();
            }
        }
    }
}