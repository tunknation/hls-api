﻿using Autofac;
using HLS.Catalog.IntegrationEvents;
using HLS.ContainerManagement.IntegrationEvents;
using HLS.Core.EventBus;
using Serilog;

namespace HLS.Sales.Infrastructure.Configuration.EventsBus {
    public static class EventsBusStartup {
        public static void Initialize(ILogger logger) {
            SubscribeToIntegrationEvents(logger);
        }

        private static void SubscribeToIntegrationEvents(ILogger logger) {
            var eventBus = SalesCompositionRoot.BeginLifetimeScope().Resolve<IEventsBus>();

            SubscribeToIntegrationEvent<NewProjectStartedIntegrationEvent>(eventBus, logger);
            SubscribeToIntegrationEvent<NewContainerDeliveryServiceProductGeneratedIntegrationEvent>(eventBus, logger);
            SubscribeToIntegrationEvent<NewContainerRetrievalServiceProductGeneratedIntegrationEvent>(eventBus, logger);
        }

        private static void SubscribeToIntegrationEvent<T>(IEventsBus eventBus, ILogger logger)
            where T : IntegrationEvent {
            logger.Information("Subscribe to {@IntegrationEvent}", typeof(T).FullName);
            eventBus.Subscribe(
                new IntegrationEventGenericHandler<T>());
        }
    }
}