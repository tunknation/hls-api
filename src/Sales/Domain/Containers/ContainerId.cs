﻿using System;
using HLS.Core.Domain;

namespace HLS.Sales.Domain.Containers {
    public class ContainerId : TypedIdValueBase {
        public ContainerId(Guid value) : base(value) { }
    }
}