﻿using System;
using HLS.Core.Domain;

namespace HLS.Sales.Domain.SeedWork {
    public abstract class AggregateId : TypedIdValueBase {
        protected AggregateId(Guid value) : base(value) { }
    }

    public abstract class AggregateId<T> : AggregateId where T : AggregateRoot {
        protected AggregateId(Guid value) : base(value) { }
    }
}