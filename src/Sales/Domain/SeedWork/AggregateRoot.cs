﻿using System.Collections.Generic;
using HLS.Core.Domain;

namespace HLS.Sales.Domain.SeedWork {
    public abstract class AggregateRoot {
        private readonly List<IDomainEvent> _domainEvents;

        protected AggregateRoot() {
            _domainEvents = new List<IDomainEvent>();

            Version = -1;
        }

        public AggregateId<AggregateRoot> Id { get; protected set; }

        public int Version { get; private set; }

        public IReadOnlyCollection<IDomainEvent> GetDomainEvents() {
            return _domainEvents.AsReadOnly();
        }

        protected void AddDomainEvent(IDomainEvent @event) {
            _domainEvents.Add(@event);
        }

        public void Load(IEnumerable<IDomainEvent> history) {
            foreach (var @event in history) {
                Apply(@event);
                Version++;
            }
        }

        protected abstract void Apply(IDomainEvent @event);

        protected static void CheckRule(IBusinessRule rule) {
            if (rule.IsBroken()) {
                throw new BusinessRuleValidationException(rule);
            }
        }
    }

    public abstract class AggregateRoot<T> : AggregateRoot where T : AggregateId {
        public new T Id { get; protected set; }
    }
}