﻿using HLS.Core.Domain;

namespace HLS.Sales.Domain.SeedWork {
    public abstract class Entity<T> where T : TypedIdValueBase {
        public T Id { get; protected set; }
    }
}