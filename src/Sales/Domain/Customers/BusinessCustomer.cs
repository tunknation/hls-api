﻿using System;
using HLS.Core.Domain;
using HLS.Sales.Domain.Customers.Events;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Domain.Customers {
    public class BusinessCustomer : AggregateRoot<BusinessCustomerId> {
        private CustomerType _type;

        private CustomerStatus _status;

        public static BusinessCustomer RegisterNewBusinessCustomer() {
            var customer = new BusinessCustomer();

            var id = new BusinessCustomerId(Guid.NewGuid());
            var type = CustomerType.Business;
            var status = CustomerStatus.Active;

            var @event = new NewBusinessCustomerRegisteredDomainEvent(
                id,
                type,
                status);

            customer.Apply(@event);
            customer.AddDomainEvent(@event);

            return customer;
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(NewBusinessCustomerRegisteredDomainEvent @event) {
            Id = @event.CustomerId;
            _type = @event.Type;
            _status = @event.Status;
        }
    }
}