﻿using System;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Domain.Customers {
    public class PrivateCustomerId : AggregateId<PrivateCustomer> {
        public PrivateCustomerId(Guid value) : base(value) { }
    }
}