﻿using System;
using HLS.Core.Domain;

namespace HLS.Sales.Domain.Customers {
    public class CustomerId : TypedIdValueBase {
        public CustomerId(Guid value) : base(value) { }
    }
}