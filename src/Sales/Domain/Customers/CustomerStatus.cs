﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.Sales.Domain.Customers {
    public class CustomerStatus : ValueObject {
        [JsonConstructor]
        private CustomerStatus(string code) {
            Code = code;
        }

        public static CustomerStatus Active => new(nameof(Active));

        public string Code { get; }

        public static CustomerStatus Of(string code) {
            return new CustomerStatus(code);
        }

        public static implicit operator string(CustomerStatus status) {
            return status.Code;
        }
    }
}