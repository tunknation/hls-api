﻿using HLS.Core.Domain;

namespace HLS.Sales.Domain.Customers.Events {
    public class NewPrivateCustomerRegisteredDomainEvent : DomainEventBase {
        public NewPrivateCustomerRegisteredDomainEvent(
            PrivateCustomerId customerId,
            CustomerType type,
            CustomerStatus status) {
            CustomerId = customerId;
            Type = type;
            Status = status;
        }

        public PrivateCustomerId CustomerId { get; }

        public CustomerType Type { get; }

        public CustomerStatus Status { get; }
    }
}