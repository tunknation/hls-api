﻿using HLS.Core.Domain;

namespace HLS.Sales.Domain.Customers.Events {
    public class NewBusinessCustomerRegisteredDomainEvent : DomainEventBase {
        public NewBusinessCustomerRegisteredDomainEvent(
            BusinessCustomerId customerId,
            CustomerType type,
            CustomerStatus status) {
            CustomerId = customerId;
            Type = type;
            Status = status;
        }

        public BusinessCustomerId CustomerId { get; }

        public CustomerType Type { get; }

        public CustomerStatus Status { get; }
    }
}