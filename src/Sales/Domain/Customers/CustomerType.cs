﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.Sales.Domain.Customers {
    public class CustomerType : ValueObject {
        [JsonConstructor]
        private CustomerType(string code) {
            Code = code;
        }

        public static CustomerType Business => new(nameof(Business));

        public static CustomerType Private => new(nameof(Private));

        public string Code { get; }

        public static CustomerType Of(string code) {
            return new CustomerType(code);
        }

        public static implicit operator string(CustomerType type) {
            return type.Code;
        }
    }
}