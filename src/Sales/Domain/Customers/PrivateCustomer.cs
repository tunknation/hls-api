﻿using System;
using HLS.Core.Domain;
using HLS.Sales.Domain.Customers.Events;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Domain.Customers {
    public class PrivateCustomer : AggregateRoot<PrivateCustomerId> {
        private CustomerType _type;

        private CustomerStatus _status;

        public static PrivateCustomer RegisterNewPrivateCustomer() {
            var customer = new PrivateCustomer();

            var id = new PrivateCustomerId(Guid.NewGuid());
            var type = CustomerType.Private;
            var status = CustomerStatus.Active;

            var @event = new NewPrivateCustomerRegisteredDomainEvent(
                id,
                type,
                status);

            customer.Apply(@event);
            customer.AddDomainEvent(@event);

            return customer;
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(NewPrivateCustomerRegisteredDomainEvent @event) {
            Id = @event.CustomerId;
            _type = @event.Type;
            _status = @event.Status;
        }
    }
}