﻿using System;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Domain.Customers {
    public class BusinessCustomerId : AggregateId<BusinessCustomer> {
        public BusinessCustomerId(Guid value) : base(value) { }
    }
}