﻿using System;
using HLS.Core.Domain;

namespace HLS.Sales.Domain.ContainerTypes {
    public class ContainerTypeId : TypedIdValueBase {
        public ContainerTypeId(Guid value) : base(value) { }
    }
}