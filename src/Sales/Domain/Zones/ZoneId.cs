﻿using System;
using HLS.Core.Domain;

namespace HLS.Sales.Domain.Zones {
    public class ZoneId : TypedIdValueBase {
        public ZoneId(Guid value) : base(value) { }
    }
}