﻿using HLS.Core.Domain;
using HLS.Sales.Domain.Customers;
using HLS.Sales.Domain.Projects.Events;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Domain.Projects {
    public class Project : AggregateRoot<ProjectId> {
        private CustomerId _customerId;

        private ProjectAddress _address;

        public static Project Create(
            ProjectId id,
            CustomerId customerId,
            ProjectAddress address) {
            var project = new Project();

            var @event = new ProjectCreatedDomainEvent(
                id,
                customerId,
                address);

            project.Apply(@event);
            project.AddDomainEvent(@event);

            return project;
        }

        public ProjectSnapshot GetSnapshot() {
            return new ProjectSnapshot(
                Id,
                _customerId,
                _address);
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(ProjectCreatedDomainEvent @event) {
            Id = @event.ProjectId;
            _customerId = @event.CustomerId;
            _address = @event.Address;
        }
    }
}