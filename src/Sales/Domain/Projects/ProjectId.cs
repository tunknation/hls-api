﻿using System;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Domain.Projects {
    public class ProjectId : AggregateId<Project> {
        public ProjectId(Guid value) : base(value) { }
    }
}