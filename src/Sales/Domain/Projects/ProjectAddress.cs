﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.Sales.Domain.Projects {
    public class ProjectAddress : ValueObject {
        [JsonConstructor]
        private ProjectAddress(string city, string postalCode, string streetAddress) {
            City = city;
            PostalCode = postalCode;
            StreetAddress = streetAddress;
        }

        public string City { get; }

        public string PostalCode { get; }

        public string StreetAddress { get; }

        public static ProjectAddress Of(string city, string postalCode, string streetAddress) {
            return new ProjectAddress(city, postalCode, streetAddress);
        }
    }
}