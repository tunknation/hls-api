﻿using HLS.Core.Domain;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Domain.Projects.Events {
    public class ProjectCreatedDomainEvent : DomainEventBase {
        public ProjectCreatedDomainEvent(
            ProjectId projectId,
            CustomerId customerId,
            ProjectAddress address) {
            ProjectId = projectId;
            CustomerId = customerId;
            Address = address;
        }

        public ProjectId ProjectId { get; }

        public CustomerId CustomerId { get; }

        public ProjectAddress Address { get; }
    }
}