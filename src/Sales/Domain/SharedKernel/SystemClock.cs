﻿using System;

namespace HLS.Sales.Domain.SharedKernel {
    public static class SystemClock {
        private static DateTime? _customDate;

        public static DateTime Now => _customDate ?? DateTime.UtcNow;

        public static DateTime Today => _customDate ?? DateTime.UtcNow.Date;

        public static void Set(DateTime customDate) {
            _customDate = customDate;
        }

        public static void Reset() {
            _customDate = null;
        }
    }
}