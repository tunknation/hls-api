﻿using System;

namespace HLS.Sales.Domain.SharedKernel {
    public static class DateTimeHelper {
        public static DateTime AddMonths(DateTime date, int months) {
            var next = date.AddMonths(months);

            if (date.Day != DateTime.DaysInMonth(date.Year, date.Month)) {
                return next;
            }

            var addDays = DateTime.DaysInMonth(next.Year, next.Month) - next.Day;
            return next.AddDays(addDays);
        }
    }
}