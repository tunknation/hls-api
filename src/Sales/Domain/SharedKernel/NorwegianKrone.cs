﻿using HLS.Core.Domain;
using HLS.Sales.Domain.SharedKernel.Rules;
using Newtonsoft.Json;

namespace HLS.Sales.Domain.SharedKernel {
    public class NorwegianKrone : ValueObject {
        [JsonConstructor]
        private NorwegianKrone(decimal value) {
            Value = value;
        }

        public static NorwegianKrone Zero => new(decimal.Zero);

        public decimal Value { get; }

        public static NorwegianKrone Of(decimal value) {
            CheckRule(new NorwegianKroneCannotBeNegativeRule(value));
            CheckRule(new NorwegianKroneCannotContainPartOfOreRule(value));

            return new NorwegianKrone(value);
        }

        public static implicit operator decimal(NorwegianKrone kr) {
            return kr.Value;
        }
    }
}