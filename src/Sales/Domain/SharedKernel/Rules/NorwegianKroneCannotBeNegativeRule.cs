﻿using HLS.Core.Domain;

namespace HLS.Sales.Domain.SharedKernel.Rules {
    public class NorwegianKroneCannotBeNegativeRule : IBusinessRule {
        private readonly decimal _value;

        public NorwegianKroneCannotBeNegativeRule(decimal value) {
            _value = value;
        }

        public bool IsBroken() {
            return _value < 0;
        }

        public string Message => "Norwegian krone cannot be negative";
    }
}