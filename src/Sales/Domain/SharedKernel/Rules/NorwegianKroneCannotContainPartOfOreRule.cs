﻿using HLS.Core.Domain;

namespace HLS.Sales.Domain.SharedKernel.Rules {
    public class NorwegianKroneCannotContainPartOfOreRule : IBusinessRule {
        private readonly decimal _value;

        public NorwegianKroneCannotContainPartOfOreRule(decimal value) {
            _value = value;
        }

        public bool IsBroken() {
            return _value % 0.01m > 0;
        }

        public string Message => "Norwegian krone cannot contain part of a ore";
    }
}