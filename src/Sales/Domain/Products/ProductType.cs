﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.Sales.Domain.Products {
    public class ProductType : ValueObject {
        [JsonConstructor]
        private ProductType(string code) {
            Code = code;
        }

        public static ProductType ContainerDeliveryServiceProduct => new(nameof(ContainerDeliveryServiceProduct));

        public static ProductType ContainerRetrievalServiceProduct => new(nameof(ContainerRetrievalServiceProduct));

        public string Code { get; }

        public static ProductType Of(string code) {
            return new ProductType(code);
        }

        public static implicit operator string(ProductType type) {
            return type.Code;
        }
    }
}