﻿using HLS.Core.Domain;
using HLS.Sales.Domain.ContainerTypes;
using HLS.Sales.Domain.Products.Events;
using HLS.Sales.Domain.SeedWork;
using HLS.Sales.Domain.SharedKernel;
using HLS.Sales.Domain.Zones;

namespace HLS.Sales.Domain.Products {
    public class ContainerDeliveryServiceProduct : AggregateRoot<ContainerDeliveryServiceProductId> {
        private ProductType _type;

        private ProductSku _sku;

        private ContainerTypeId _containerTypeId;

        private ZoneId _zoneId;

        private NorwegianKrone _price;

        public static ContainerDeliveryServiceProduct Create(
            ContainerDeliveryServiceProductId id,
            ProductSku sku,
            ContainerTypeId containerTypeId,
            ZoneId zoneId) {
            var product = new ContainerDeliveryServiceProduct();

            var type = ProductType.ContainerDeliveryServiceProduct;
            var price = NorwegianKrone.Zero;

            var @event = new ContainerDeliveryServiceProductCreatedDomainEvent(
                id,
                type,
                sku,
                containerTypeId,
                zoneId,
                price);

            product.Apply(@event);
            product.AddDomainEvent(@event);

            return product;
        }

        public void AdjustProductPrice(NorwegianKrone price) {
            var @event = new ContainerDeliveryServiceProductPriceAdjustedDomainEvent(
                Id,
                price);

            Apply(@event);
            AddDomainEvent(@event);
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(ContainerDeliveryServiceProductCreatedDomainEvent @event) {
            Id = @event.ProductId;
            _type = @event.Type;
            _sku = @event.Sku;
            _containerTypeId = @event.ContainerTypeId;
            _zoneId = @event.ZoneId;
            _price = @event.Price;
        }

        private void _when(ContainerDeliveryServiceProductPriceAdjustedDomainEvent @event) {
            _price = @event.Price;
        }
    }
}