﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.Sales.Domain.Products {
    public class ProductSku : ValueObject {
        [JsonConstructor]
        private ProductSku(string code) {
            Code = code;
        }

        public string Code { get; }

        public static ProductSku Of(string code) {
            return new ProductSku(code);
        }

        public static implicit operator string(ProductSku sku) {
            return sku.Code;
        }
    }
}