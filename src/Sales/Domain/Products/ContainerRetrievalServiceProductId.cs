﻿using System;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Domain.Products {
    public class ContainerRetrievalServiceProductId : AggregateId<ContainerRetrievalServiceProduct> {
        public ContainerRetrievalServiceProductId(Guid value) : base(value) { }

        public static implicit operator ContainerRetrievalServiceProductId(ProductId id) {
            return new ContainerRetrievalServiceProductId(id.Value);
        }

        public static implicit operator ProductId(ContainerRetrievalServiceProductId id) {
            return new ProductId(id.Value);
        }
    }
}