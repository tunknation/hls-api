﻿using System;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Domain.Products {
    public class ContainerDeliveryServiceProductId : AggregateId<ContainerDeliveryServiceProduct> {
        public ContainerDeliveryServiceProductId(Guid value) : base(value) { }

        public static implicit operator ContainerDeliveryServiceProductId(ProductId id) {
            return new ContainerDeliveryServiceProductId(id.Value);
        }

        public static implicit operator ProductId(ContainerDeliveryServiceProductId id) {
            return new ProductId(id.Value);
        }
    }
}