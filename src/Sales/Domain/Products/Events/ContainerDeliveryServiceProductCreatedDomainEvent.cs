﻿using HLS.Core.Domain;
using HLS.Sales.Domain.ContainerTypes;
using HLS.Sales.Domain.SharedKernel;
using HLS.Sales.Domain.Zones;

namespace HLS.Sales.Domain.Products.Events {
    public class ContainerDeliveryServiceProductCreatedDomainEvent : DomainEventBase {
        public ContainerDeliveryServiceProductCreatedDomainEvent(
            ContainerDeliveryServiceProductId productId,
            ProductType type,
            ProductSku sku,
            ContainerTypeId containerTypeId,
            ZoneId zoneId,
            NorwegianKrone price) {
            ProductId = productId;
            Type = type;
            Sku = sku;
            ContainerTypeId = containerTypeId;
            ZoneId = zoneId;
            Price = price;
        }

        public ContainerDeliveryServiceProductId ProductId { get; }

        public ProductType Type { get; }

        public ProductSku Sku { get; }

        public ContainerTypeId ContainerTypeId { get; }

        public ZoneId ZoneId { get; }

        public NorwegianKrone Price { get; }
    }
}