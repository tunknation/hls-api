﻿using HLS.Core.Domain;
using HLS.Sales.Domain.SharedKernel;

namespace HLS.Sales.Domain.Products.Events {
    public class ContainerDeliveryServiceProductPriceAdjustedDomainEvent : DomainEventBase {
        public ContainerDeliveryServiceProductPriceAdjustedDomainEvent(
            ContainerDeliveryServiceProductId productId,
            NorwegianKrone price) {
            ProductId = productId;
            Price = price;
        }

        public ContainerDeliveryServiceProductId ProductId { get; }

        public NorwegianKrone Price { get; }
    }
}