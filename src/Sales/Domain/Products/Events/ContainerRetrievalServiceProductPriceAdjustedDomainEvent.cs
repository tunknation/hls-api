﻿using HLS.Core.Domain;
using HLS.Sales.Domain.SharedKernel;

namespace HLS.Sales.Domain.Products.Events {
    public class ContainerRetrievalServiceProductPriceAdjustedDomainEvent : DomainEventBase {
        public ContainerRetrievalServiceProductPriceAdjustedDomainEvent(
            ContainerRetrievalServiceProductId productId,
            NorwegianKrone price) {
            ProductId = productId;
            Price = price;
        }

        public ContainerRetrievalServiceProductId ProductId { get; }

        public NorwegianKrone Price { get; }
    }
}