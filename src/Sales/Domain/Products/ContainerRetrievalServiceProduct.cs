﻿using HLS.Core.Domain;
using HLS.Sales.Domain.ContainerTypes;
using HLS.Sales.Domain.Products.Events;
using HLS.Sales.Domain.SeedWork;
using HLS.Sales.Domain.SharedKernel;
using HLS.Sales.Domain.Zones;

namespace HLS.Sales.Domain.Products {
    public class ContainerRetrievalServiceProduct : AggregateRoot<ContainerRetrievalServiceProductId> {
        private ProductType _type;

        private ProductSku _sku;

        private ContainerTypeId _containerTypeId;

        private ZoneId _zoneId;

        private NorwegianKrone _price;

        public static ContainerRetrievalServiceProduct Create(
            ContainerRetrievalServiceProductId id,
            ProductSku sku,
            ContainerTypeId containerTypeId,
            ZoneId zoneId) {
            var product = new ContainerRetrievalServiceProduct();

            var type = ProductType.ContainerRetrievalServiceProduct;
            var price = NorwegianKrone.Zero;

            var @event = new ContainerRetrievalServiceProductCreatedDomainEvent(
                id,
                type,
                sku,
                containerTypeId,
                zoneId,
                price);

            product.Apply(@event);
            product.AddDomainEvent(@event);

            return product;
        }

        public void AdjustProductPrice(NorwegianKrone price) {
            var @event = new ContainerRetrievalServiceProductPriceAdjustedDomainEvent(
                Id,
                price);

            Apply(@event);
            AddDomainEvent(@event);
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(ContainerRetrievalServiceProductCreatedDomainEvent @event) {
            Id = @event.ProductId;
            _type = @event.Type;
            _sku = @event.Sku;
            _containerTypeId = @event.ContainerTypeId;
            _zoneId = @event.ZoneId;
            _price = @event.Price;
        }

        private void _when(ContainerRetrievalServiceProductPriceAdjustedDomainEvent @event) {
            _price = @event.Price;
        }
    }
}