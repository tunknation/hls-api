﻿using System;
using HLS.Core.Domain;

namespace HLS.Sales.Domain.Products {
    public class ProductId : TypedIdValueBase {
        public ProductId(Guid value) : base(value) { }
    }
}