﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.Sales.Domain.Orders {
    public class OrderAddress : ValueObject {
        [JsonConstructor]
        private OrderAddress(string city, string postalCode, string streetAddress) {
            City = city;
            PostalCode = postalCode;
            StreetAddress = streetAddress;
        }

        public string City { get; }

        public string PostalCode { get; }

        public string StreetAddress { get; }

        public static OrderAddress Of(string city, string postalCode, string streetAddress) {
            return new OrderAddress(city, postalCode, streetAddress);
        }
    }
}