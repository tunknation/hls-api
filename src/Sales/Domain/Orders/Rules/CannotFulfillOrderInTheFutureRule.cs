﻿using System;
using HLS.Core.Domain;
using HLS.Sales.Domain.SharedKernel;

namespace HLS.Sales.Domain.Orders.Rules {
    public class CannotFulfillOrderInTheFutureRule : IBusinessRule {
        private readonly DateTime _fulfillmentDate;

        public CannotFulfillOrderInTheFutureRule(DateTime? fulfillmentDate) {
            _fulfillmentDate = fulfillmentDate ?? SystemClock.Now;
        }

        public bool IsBroken() {
            return SystemClock.Now < _fulfillmentDate;
        }

        public string Message => "Cannot fulfill order in the future";
    }
}