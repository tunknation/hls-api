﻿using System;
using HLS.Sales.Domain.Containers;
using HLS.Sales.Domain.Projects;

namespace HLS.Sales.Domain.Orders {
    public class ContainerRetrievalOrderSnapshot {
        public ContainerRetrievalOrderSnapshot(
            ContainerRetrievalOrderId orderId,
            ProjectId projectId,
            ContainerId containerId,
            DateTime? fulfillmentDate) {
            OrderId = orderId;
            ProjectId = projectId;
            ContainerId = containerId;
            FulfillmentDate = fulfillmentDate;
        }

        public ContainerRetrievalOrderId OrderId { get; }

        public ProjectId ProjectId { get; }

        public ContainerId ContainerId { get; }

        public DateTime? FulfillmentDate { get; }
    }
}