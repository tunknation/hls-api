﻿using System;
using HLS.Core.Domain;
using HLS.Sales.Domain.ContainerTypes;
using HLS.Sales.Domain.Customers;
using HLS.Sales.Domain.Orders.Events;
using HLS.Sales.Domain.Orders.Rules;
using HLS.Sales.Domain.SeedWork;
using HLS.Sales.Domain.SharedKernel;

namespace HLS.Sales.Domain.Orders {
    public class ContainerDeliveryOrder : AggregateRoot<ContainerDeliveryOrderId> {
        private OrderType _type;

        private OrderStatus _status;

        private CustomerId _customerId;

        private ContainerTypeId _containerTypeId;

        private OrderAddress _address;

        private DateTime? _fulfillmentDate;

        public static ContainerDeliveryOrder PlaceContainerDeliveryOrder(
            CustomerId customerId,
            ContainerTypeId containerTypeId,
            OrderAddress address) {
            var order = new ContainerDeliveryOrder();

            var id = new ContainerDeliveryOrderId(Guid.NewGuid());
            var type = OrderType.ContainerDelivery;
            var status = OrderStatus.Placed;

            var @event = new ContainerDeliveryOrderPlacedDomainEvent(
                id,
                type,
                status,
                customerId,
                containerTypeId,
                address);

            order.Apply(@event);
            order.AddDomainEvent(@event);

            return order;
        }

        public ContainerDeliveryOrderSnapshot GetSnapshot() {
            return new ContainerDeliveryOrderSnapshot(
                Id,
                _customerId,
                _containerTypeId,
                _address,
                _fulfillmentDate);
        }

        public void FulfillContainerDeliveryOrder(DateTime? fulfillmentDate) {
            CheckRule(new CannotFulfillOrderInTheFutureRule(fulfillmentDate));

            var status = OrderStatus.Fulfilled;
            var date = fulfillmentDate ?? SystemClock.Now;

            var @event = new ContainerDeliveryOrderFulfilledDomainEvent(
                Id,
                status,
                date);

            Apply(@event);
            AddDomainEvent(@event);
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(ContainerDeliveryOrderPlacedDomainEvent @event) {
            Id = @event.OrderId;
            _type = @event.Type;
            _status = @event.Status;
            _customerId = @event.CustomerId;
            _containerTypeId = @event.ContainerTypeId;
            _address = @event.Address;
        }

        private void _when(ContainerDeliveryOrderFulfilledDomainEvent @event) {
            _status = @event.Status;
            _fulfillmentDate = @event.FulfillmentDate;
        }
    }
}