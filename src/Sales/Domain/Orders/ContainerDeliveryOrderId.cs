﻿using System;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Domain.Orders {
    public class ContainerDeliveryOrderId : AggregateId<ContainerDeliveryOrder> {
        public ContainerDeliveryOrderId(Guid value) : base(value) { }
    }
}