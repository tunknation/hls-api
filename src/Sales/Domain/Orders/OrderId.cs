﻿using System;
using HLS.Core.Domain;

namespace HLS.Sales.Domain.Orders {
    public class OrderId : TypedIdValueBase {
        public OrderId(Guid value) : base(value) { }
    }
}