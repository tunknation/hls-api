﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.Sales.Domain.Orders {
    public class OrderType : ValueObject {
        [JsonConstructor]
        private OrderType(string code) {
            Code = code;
        }

        public static OrderType ContainerDelivery => new(nameof(ContainerDelivery));

        public static OrderType ContainerRetrieval => new(nameof(ContainerRetrieval));

        public string Code { get; }

        public static OrderType Of(string code) {
            return new OrderType(code);
        }

        public static implicit operator string(OrderType type) {
            return type.Code;
        }
    }
}