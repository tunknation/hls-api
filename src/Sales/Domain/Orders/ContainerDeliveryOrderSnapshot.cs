﻿using System;
using HLS.Sales.Domain.ContainerTypes;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Domain.Orders {
    public class ContainerDeliveryOrderSnapshot {
        public ContainerDeliveryOrderSnapshot(
            ContainerDeliveryOrderId orderId,
            CustomerId customerId,
            ContainerTypeId containerTypeId,
            OrderAddress address,
            DateTime? fulfillmentDate) {
            OrderId = orderId;
            CustomerId = customerId;
            ContainerTypeId = containerTypeId;
            Address = address;
            FulfillmentDate = fulfillmentDate;
        }

        public ContainerDeliveryOrderId OrderId { get; }

        public CustomerId CustomerId { get; }

        public ContainerTypeId ContainerTypeId { get; }

        public OrderAddress Address { get; }

        public DateTime? FulfillmentDate { get; }
    }
}