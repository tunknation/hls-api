﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.Sales.Domain.Orders {
    public class OrderStatus : ValueObject {
        [JsonConstructor]
        private OrderStatus(string code) {
            Code = code;
        }

        public static OrderStatus Placed => new(nameof(Placed));

        public static OrderStatus Fulfilled => new(nameof(Fulfilled));

        public string Code { get; }

        public static OrderStatus Of(string code) {
            return new OrderStatus(code);
        }

        public static implicit operator string(OrderStatus status) {
            return status.Code;
        }
    }
}