﻿using System;
using HLS.Core.Domain;
using HLS.Sales.Domain.Containers;
using HLS.Sales.Domain.Customers;
using HLS.Sales.Domain.Orders.Events;
using HLS.Sales.Domain.Orders.Rules;
using HLS.Sales.Domain.Projects;
using HLS.Sales.Domain.SeedWork;
using HLS.Sales.Domain.SharedKernel;

namespace HLS.Sales.Domain.Orders {
    public class ContainerRetrievalOrder : AggregateRoot<ContainerRetrievalOrderId> {
        private OrderType _type;

        private OrderStatus _status;

        private ProjectId _projectId;

        private CustomerId _customerId;

        private ContainerId _containerId;

        private OrderAddress _address;

        private DateTime? _fulfillmentDate;

        public static ContainerRetrievalOrder PlaceContainerRetrievalOrder(
            ProjectSnapshot project,
            ContainerId containerId) {
            var order = new ContainerRetrievalOrder();

            var id = new ContainerRetrievalOrderId(Guid.NewGuid());
            var type = OrderType.ContainerRetrieval;
            var status = OrderStatus.Placed;
            var address = OrderAddress.Of(
                project.Address.City,
                project.Address.PostalCode,
                project.Address.StreetAddress);

            var @event = new ContainerRetrievalOrderPlacedDomainEvent(
                id,
                type,
                status,
                project.ProjectId,
                project.CustomerId,
                containerId,
                address);

            order.Apply(@event);
            order.AddDomainEvent(@event);

            return order;
        }

        public void FulfillContainerRetrievalOrder(DateTime? fulfillmentDate) {
            CheckRule(new CannotFulfillOrderInTheFutureRule(fulfillmentDate));

            var status = OrderStatus.Fulfilled;
            var date = fulfillmentDate ?? SystemClock.Now;

            var @event = new ContainerRetrievalOrderFulfilledDomainEvent(
                Id,
                status,
                date);

            Apply(@event);
            AddDomainEvent(@event);
        }

        public ContainerRetrievalOrderSnapshot GetSnapshot() {
            return new ContainerRetrievalOrderSnapshot(
                Id,
                _projectId,
                _containerId,
                _fulfillmentDate);
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(ContainerRetrievalOrderPlacedDomainEvent @event) {
            Id = @event.OrderId;
            _type = @event.Type;
            _status = @event.Status;
            _projectId = @event.ProjectId;
            _customerId = @event.CustomerId;
            _containerId = @event.ContainerId;
            _address = @event.Address;
        }

        private void _when(ContainerRetrievalOrderFulfilledDomainEvent @event) {
            _status = @event.Status;
            _fulfillmentDate = @event.FulfillmentDate;
        }
    }
}