﻿using System;
using HLS.Sales.Domain.SeedWork;

namespace HLS.Sales.Domain.Orders {
    public class ContainerRetrievalOrderId : AggregateId<ContainerRetrievalOrder> {
        public ContainerRetrievalOrderId(Guid value) : base(value) { }
    }
}