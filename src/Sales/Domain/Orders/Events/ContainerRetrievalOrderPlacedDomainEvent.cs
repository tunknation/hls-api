﻿using HLS.Core.Domain;
using HLS.Sales.Domain.Containers;
using HLS.Sales.Domain.Customers;
using HLS.Sales.Domain.Projects;

namespace HLS.Sales.Domain.Orders.Events {
    public class ContainerRetrievalOrderPlacedDomainEvent : DomainEventBase {
        public ContainerRetrievalOrderPlacedDomainEvent(
            ContainerRetrievalOrderId orderId,
            OrderType type,
            OrderStatus status,
            ProjectId projectId,
            CustomerId customerId,
            ContainerId containerId,
            OrderAddress address) {
            OrderId = orderId;
            Type = type;
            Status = status;
            ProjectId = projectId;
            CustomerId = customerId;
            ContainerId = containerId;
            Address = address;
        }

        public ContainerRetrievalOrderId OrderId { get; }

        public OrderType Type { get; }

        public OrderStatus Status { get; }

        public ProjectId ProjectId { get; }

        public CustomerId CustomerId { get; }

        public ContainerId ContainerId { get; }

        public OrderAddress Address { get; }
    }
}