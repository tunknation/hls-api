﻿using System;
using HLS.Core.Domain;

namespace HLS.Sales.Domain.Orders.Events {
    public class ContainerRetrievalOrderFulfilledDomainEvent : DomainEventBase {
        public ContainerRetrievalOrderFulfilledDomainEvent(
            ContainerRetrievalOrderId orderId,
            OrderStatus status,
            DateTime fulfillmentDate) {
            OrderId = orderId;
            Status = status;
            FulfillmentDate = fulfillmentDate;
        }

        public ContainerRetrievalOrderId OrderId { get; }

        public OrderStatus Status { get; }

        public DateTime FulfillmentDate { get; }
    }
}