﻿using System;
using HLS.Core.Domain;

namespace HLS.Sales.Domain.Orders.Events {
    public class ContainerDeliveryOrderFulfilledDomainEvent : DomainEventBase {
        public ContainerDeliveryOrderFulfilledDomainEvent(
            ContainerDeliveryOrderId orderId,
            OrderStatus status,
            DateTime fulfillmentDate) {
            OrderId = orderId;
            Status = status;
            FulfillmentDate = fulfillmentDate;
        }

        public ContainerDeliveryOrderId OrderId { get; }

        public OrderStatus Status { get; }

        public DateTime FulfillmentDate { get; }
    }
}