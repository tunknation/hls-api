﻿using HLS.Core.Domain;
using HLS.Sales.Domain.ContainerTypes;
using HLS.Sales.Domain.Customers;

namespace HLS.Sales.Domain.Orders.Events {
    public class ContainerDeliveryOrderPlacedDomainEvent : DomainEventBase {
        public ContainerDeliveryOrderPlacedDomainEvent(
            ContainerDeliveryOrderId orderId,
            OrderType type,
            OrderStatus status,
            CustomerId customerId,
            ContainerTypeId containerTypeId,
            OrderAddress address) {
            OrderId = orderId;
            Type = type;
            Status = status;
            CustomerId = customerId;
            ContainerTypeId = containerTypeId;
            Address = address;
        }

        public ContainerDeliveryOrderId OrderId { get; }

        public OrderType Type { get; }

        public OrderStatus Status { get; }

        public CustomerId CustomerId { get; }

        public ContainerTypeId ContainerTypeId { get; }

        public OrderAddress Address { get; }
    }
}