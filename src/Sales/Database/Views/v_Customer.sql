﻿CREATE VIEW [sales].[v_Customer]
AS
SELECT [C].[Id],
       [C].[Type],
       [C].[Status]
FROM [sales].[Customer] AS [C]
GO