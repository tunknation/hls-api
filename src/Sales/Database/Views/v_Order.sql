﻿CREATE VIEW [sales].[v_Order]
AS
SELECT [O].[Id],
       [O].[Type],
       [O].[Status],
       [O].[CustomerId],
       [O].[ContainerTypeId],
       [O].[ProjectId],
       [O].[ContainerId],
       [O].[Address],
       [O].[FulfillmentDate]
FROM [sales].[Order] AS [O]
GO