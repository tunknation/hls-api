﻿CREATE VIEW [sales].[v_Product]
AS
SELECT [P].[Id],
       [P].[Type],
       [P].[Sku],
       [P].[ContainerTypeId],
       [P].[ZoneId],
       [P].[Price]
FROM [sales].[Product] AS [P]
GO