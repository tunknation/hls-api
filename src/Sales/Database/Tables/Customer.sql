﻿CREATE TABLE [sales].[Customer]
(
    [Id]     UNIQUEIDENTIFIER NOT NULL,
    [Type]   VARCHAR(50)      NOT NULL,
    [Status] VARCHAR(50)      NOT NULL,
    CONSTRAINT [PK_sales_Customer_Id] PRIMARY KEY ([Id] ASC)
)