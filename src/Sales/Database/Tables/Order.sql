﻿CREATE TABLE [sales].[Order]
(
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [Type]            VARCHAR(50)      NOT NULL,
    [Status]          VARCHAR(50)      NOT NULL,
    [CustomerId]      UNIQUEIDENTIFIER NULL,
    [ContainerTypeId] UNIQUEIDENTIFIER NULL,
    [ProjectId]       UNIQUEIDENTIFIER NULL,
    [ContainerId]     UNIQUEIDENTIFIER NULL,
    [Address]         VARCHAR(max)     NULL,
    [FulfillmentDate] DATETIME2        NULL,
    CONSTRAINT [PK_sales_Order_Id] PRIMARY KEY ([Id] ASC)
)