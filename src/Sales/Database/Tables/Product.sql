﻿CREATE TABLE [sales].[Product]
(
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [Type]            VARCHAR(255)     NOT NULL,
    [Sku]             VARCHAR(255)     NOT NULL,
    [ContainerTypeId] UNIQUEIDENTIFIER NULL,
    [ZoneId]          UNIQUEIDENTIFIER NULL,
    [Price]           DECIMAL          NOT NULL,
    CONSTRAINT [PK_sales_Product_Id] PRIMARY KEY ([Id] ASC)
)