﻿CREATE TABLE [sales].[Project]
(
    [Id]         UNIQUEIDENTIFIER NOT NULL,
    [CustomerId] UNIQUEIDENTIFIER NOT NULL,
    [Address]    NVARCHAR(MAX)    NOT NULL,
    CONSTRAINT [PK_sales_Project_Id] PRIMARY KEY ([Id] ASC)
)  