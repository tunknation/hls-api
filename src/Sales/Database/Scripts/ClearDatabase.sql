﻿-- noinspection SqlWithoutWhereForFile

DELETE
FROM [sales].[InboxMessages]

DELETE
FROM [sales].[InternalCommands]

DELETE
FROM [sales].[OutboxMessages]

DELETE
FROM [sales].[Messages]

DBCC CHECKIDENT ('sales.Messages', RESEED, 0);

DELETE
FROM [sales].[Streams]

DBCC CHECKIDENT ('sales.Streams', RESEED, 0);

DELETE
FROM [sales].[SubscriptionCheckpoints]

DELETE
FROM [sales].[Customer]

DELETE
FROM [sales].[Order]

DELETE
FROM [sales].[Product]

DELETE
FROM [sales].[Project]