﻿using System;
using HLS.Core.EventBus;

namespace HLS.ContainerManagement.IntegrationEvents {
    public class NewProjectStartedIntegrationEvent : IntegrationEvent {
        public NewProjectStartedIntegrationEvent(
            Guid id,
            DateTime occurredOn,
            Guid projectId,
            Guid customerId,
            string city,
            string postalCode,
            string streetAddress) : base(id, occurredOn) {
            ProjectId = projectId;
            CustomerId = customerId;
            City = city;
            PostalCode = postalCode;
            StreetAddress = streetAddress;
        }

        public Guid ProjectId { get; }

        public Guid CustomerId { get; }

        public string City { get; }

        public string PostalCode { get; }

        public string StreetAddress { get; }
    }
}