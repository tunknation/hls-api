﻿-- noinspection SqlWithoutWhereForFile

DELETE
FROM [containermanagement].[InboxMessages]

DELETE
FROM [containermanagement].[InternalCommands]

DELETE
FROM [containermanagement].[OutboxMessages]

DELETE
FROM [containermanagement].[Messages]

DBCC CHECKIDENT ('containermanagement.Messages', RESEED, 0);

DELETE
FROM [containermanagement].Streams

DBCC CHECKIDENT ('containermanagement.Streams', RESEED, 0);

DELETE
FROM [containermanagement].[SubscriptionCheckpoints]

DELETE
FROM [containermanagement].[Project]

DELETE
FROM [containermanagement].[Container]

DELETE
FROM [containermanagement].[ContainerLease]