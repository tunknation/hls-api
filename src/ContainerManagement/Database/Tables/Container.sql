﻿CREATE TABLE [containermanagement].[Container]
(
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [Status]           VARCHAR(255)     NOT NULL,
    [ProjectId]        UNIQUEIDENTIFIER NOT NULL,
    [ContainerTypeId]  UNIQUEIDENTIFIER NOT NULL,
    [DescriptiveName]  VARCHAR(255)     NULL,
    [DeliveryDate]     DATETIME2        NOT NULL,
    [RetrievementDate] DATETIME2        NULL,
    CONSTRAINT [PK_containermanagement_Container_Id] PRIMARY KEY ([Id] ASC)
)