﻿CREATE TABLE [containermanagement].[LeaseDay]
(
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ContainerLeaseId] UNIQUEIDENTIFIER NOT NULL,
    [Date]             DATETIME2        NOT NULL,
    [Status]           VARCHAR(50)      NOT NULL,
    CONSTRAINT [PK_containermanagement_LeaseDay_Id] PRIMARY KEY ([Id] ASC)
)