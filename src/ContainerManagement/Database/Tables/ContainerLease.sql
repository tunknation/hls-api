﻿CREATE TABLE [containermanagement].[ContainerLease]
(
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [ProjectId]      UNIQUEIDENTIFIER NOT NULL,
    [ContainerId]    UNIQUEIDENTIFIER NOT NULL,
    [Status]         VARCHAR(50)      NOT NULL,
    [StartDate]      DATETIME2        NOT NULL,
    [ExpirationDate] DATETIME2        NOT NULL,
    [CompletionDate] DATETIME2        NULL,
    [Months]         NVARCHAR(MAX)    NOT NULL,
    CONSTRAINT [PK_containermanagement_ContainerLease_Id] PRIMARY KEY ([Id] ASC)
)