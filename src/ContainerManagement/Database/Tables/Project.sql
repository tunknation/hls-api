﻿CREATE TABLE [containermanagement].[Project]
(
    [Id]             UNIQUEIDENTIFIER NOT NULL,
    [Status]         VARCHAR(50)      NOT NULL,
    [CustomerId]     UNIQUEIDENTIFIER NOT NULL,
    [Address]        NVARCHAR(MAX)    NOT NULL,
    [StartDate]      DATETIME2        NOT NULL,
    [CompletionDate] DATETIME2        NULL,
    CONSTRAINT [PK_containermanagement_Project_Id] PRIMARY KEY ([Id] ASC)
)  