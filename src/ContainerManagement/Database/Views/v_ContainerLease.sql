﻿CREATE VIEW [containermanagement].[v_ContainerLease]
AS
SELECT [CL].[Id],
       [CL].[ProjectId],
       [CL].[ContainerId],
       [CL].[Status],
       [CL].[StartDate],
       [CL].[ExpirationDate],
       [CL].[CompletionDate],
       [CL].[Months]
FROM [containermanagement].[ContainerLease] AS [CL];
GO