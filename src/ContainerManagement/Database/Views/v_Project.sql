﻿CREATE VIEW [containermanagement].[v_Project]
AS
SELECT [P].[Id],
       [P].[Status],
       [P].[CustomerId],
       [P].[Address],
       [P].[StartDate],
       [P].[CompletionDate],
       ISNULL((
                  SELECT [C].[Id],
                         [C].[Status],
                         [C].[ContainerTypeId],
                         [C].[DescriptiveName],
                         [C].[DeliveryDate],
                         [C].[RetrievementDate]
                  FROM [containermanagement].[Container] AS [C]
                  WHERE [C].[ProjectId] = [P].[Id]
                  FOR JSON PATH
              ), '[]') AS [Containers]
FROM [containermanagement].[Project] AS [P]
GO