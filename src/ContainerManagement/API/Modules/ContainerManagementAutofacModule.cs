﻿using Autofac;
using HLS.ContainerManagement.Application.Contracts;
using HLS.ContainerManagement.Infrastructure;

namespace HLS.ContainerManagement.API.Modules {
    public class ContainerManagementAutofacModule : Module {
        protected override void Load(ContainerBuilder builder) {
            builder.RegisterType<ContainerManagementModule>()
                .As<IContainerManagementModule>()
                .InstancePerLifetimeScope();
        }
    }
}