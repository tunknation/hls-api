﻿namespace HLS.ContainerManagement.API.GraphQL.Projects.Payloads {
    public class AddDescriptiveContainerNamePayload {
        public bool Success { get; set; }
    }
}