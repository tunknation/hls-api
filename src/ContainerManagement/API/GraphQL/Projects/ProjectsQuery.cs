﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HLS.ContainerManagement.API.GraphQL.Projects.Types;
using HLS.ContainerManagement.Application.Contracts;
using HLS.ContainerManagement.Application.Projects.GetProject;
using HLS.ContainerManagement.Application.Projects.GetProjects;
using HotChocolate.Types;

namespace HLS.ContainerManagement.API.GraphQL.Projects {
    [ExtendObjectType(Name = "Query")]
    public class ProjectsQuery {
        private readonly IContainerManagementModule _module;

        public ProjectsQuery(IContainerManagementModule module) {
            _module = module;
        }

        public async Task<List<Project>> GetProjects(int? page, int? perPage) {
            var query = new GetProjectsQuery(page, perPage);

            var projects = await _module.ExecuteQueryAsync(query);

            return (from project in projects select (Project)project).ToList();
        }

        public async Task<Project> GetProject(Guid projectId) {
            var query = new GetProjectQuery(projectId);

            var project = await _module.ExecuteQueryAsync(query);

            return (Project)project;
        }
    }
}