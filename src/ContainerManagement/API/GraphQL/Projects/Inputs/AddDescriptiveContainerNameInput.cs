﻿using System;

namespace HLS.ContainerManagement.API.GraphQL.Projects.Inputs {
    public class AddDescriptiveContainerNameInput {
        public Guid ProjectId { get; set; }

        public Guid ContainerId { get; set; }

        public string DescriptiveName { get; set; }
    }
}