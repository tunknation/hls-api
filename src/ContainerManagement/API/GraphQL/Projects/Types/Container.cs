﻿using System;
using HLS.ContainerManagement.Application.Projects.Dtos;

namespace HLS.ContainerManagement.API.GraphQL.Projects.Types {
    public class Container {
        public Guid Id { get; set; }

        public string Status { get; set; }

        public Guid ContainerTypeId { get; set; }

        public string DescriptiveName { get; set; }

        public DateTime DeliveryDate { get; set; }

        public DateTime? RetrievementDate { get; set; }

        public static explicit operator Container(ContainerDto dto) {
            return new Container {
                Id = dto.Id,
                Status = dto.Status,
                ContainerTypeId = dto.ContainerTypeId,
                DescriptiveName = dto.DescriptiveName,
                DeliveryDate = dto.DeliveryDate,
                RetrievementDate = dto.RetrievementDate
            };
        }
    }
}