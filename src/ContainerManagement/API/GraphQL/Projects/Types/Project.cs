﻿using System;
using System.Collections.Generic;
using System.Linq;
using HLS.ContainerManagement.Application.Projects.Dtos;
using HLS.ContainerManagement.Domain.Projects;

namespace HLS.ContainerManagement.API.GraphQL.Projects.Types {
    public class Project {
        public Guid Id { get; set; }

        public string Status { get; set; }

        public Guid CustomerId { get; set; }

        public ProjectAddress Address { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? CompletionDate { get; set; }

        public List<Container> Containers { get; set; }

        public static explicit operator Project(ProjectDto dto) {
            return new Project {
                Id = dto.Id,
                Status = dto.Status,
                CustomerId = dto.CustomerId,
                Address = dto.Address,
                StartDate = dto.StartDate,
                CompletionDate = dto.CompletionDate,
                Containers = (from container in dto.Containers select (Container)container).ToList()
            };
        }
    }
}