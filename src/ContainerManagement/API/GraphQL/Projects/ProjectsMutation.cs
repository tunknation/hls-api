﻿using System.Threading.Tasks;
using HLS.ContainerManagement.API.GraphQL.Projects.Inputs;
using HLS.ContainerManagement.API.GraphQL.Projects.Payloads;
using HLS.ContainerManagement.Application.Contracts;
using HLS.ContainerManagement.Application.Projects.AddDescriptiveContainerName;
using HotChocolate.Types;

namespace HLS.ContainerManagement.API.GraphQL.Projects {
    [ExtendObjectType(Name = "Mutation")]
    public class ProjectsMutation {
        private readonly IContainerManagementModule _module;

        public ProjectsMutation(IContainerManagementModule module) {
            _module = module;
        }

        public async Task<AddDescriptiveContainerNamePayload> AddDescriptiveContainerName(
            AddDescriptiveContainerNameInput input) {
            var cmd = new AddDescriptiveContainerNameCommand(
                input.ProjectId,
                input.ContainerId,
                input.DescriptiveName);

            await _module.ExecuteCommandAsync(cmd);

            return new AddDescriptiveContainerNamePayload {
                Success = true
            };
        }
    }
}