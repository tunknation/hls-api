﻿using HotChocolate.Types;

namespace HLS.ContainerManagement.API.GraphQL {
    [ExtendObjectType(Name = "Query")]
    public class HelloWorldQuery {
        public string GetHelloWorld() {
            return "Hello World!";
        }
    }
}