﻿using System;

namespace HLS.ContainerManagement.API.GraphQL.ContainerLeases.Types {
    public class ContainerLeaseDay {
        public DateTime Date { get; set; }

        public string Status { get; set; }

        public static explicit operator ContainerLeaseDay(Domain.ContainerLeases.ContainerLeaseDay vo) {
            return new ContainerLeaseDay {
                Date = vo.Date,
                Status = vo.Status
            };
        }
    }
}