﻿using System;
using System.Collections.Generic;
using System.Linq;
using HLS.ContainerManagement.Application.ContainerLeases.Dtos;

namespace HLS.ContainerManagement.API.GraphQL.ContainerLeases.Types {
    public class ContainerLease {
        public Guid Id { get; set; }

        public Guid ProjectId { get; set; }

        public Guid ContainerId { get; set; }

        public string Status { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime ExpirationDate { get; set; }

        public DateTime? CompletionDate { get; set; }

        public List<ContainerLeaseMonth> Months { get; set; }

        public static explicit operator ContainerLease(ContainerLeaseDto dto) {
            return new ContainerLease {
                Id = dto.Id,
                ProjectId = dto.ProjectId,
                ContainerId = dto.ContainerId,
                Status = dto.Status,
                StartDate = dto.StartDate,
                ExpirationDate = dto.ExpirationDate,
                CompletionDate = dto.CompletionDate,
                Months = (from month in dto.Months select (ContainerLeaseMonth)month).ToList()
            };
        }
    }
}