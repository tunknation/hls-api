﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HLS.ContainerManagement.API.GraphQL.ContainerLeases.Types {
    public class ContainerLeaseMonth {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public List<ContainerLeaseDay> Days { get; set; }

        public static explicit operator ContainerLeaseMonth(Domain.ContainerLeases.ContainerLeaseMonth vo) {
            return new ContainerLeaseMonth {
                StartDate = vo.StartDate,
                EndDate = vo.EndDate,
                Days = (from day in vo.Days select (ContainerLeaseDay)day).ToList()
            };
        }
    }
}