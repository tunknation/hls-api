﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HLS.ContainerManagement.API.GraphQL.ContainerLeases.Types;
using HLS.ContainerManagement.Application.ContainerLeases.GetContainerLease;
using HLS.ContainerManagement.Application.ContainerLeases.GetContainerLeases;
using HLS.ContainerManagement.Application.Contracts;
using HotChocolate.Types;

namespace HLS.ContainerManagement.API.GraphQL.ContainerLeases {
    [ExtendObjectType(Name = "Query")]
    public class ContainerLeasesQuery {
        private readonly IContainerManagementModule _module;

        public ContainerLeasesQuery(IContainerManagementModule module) {
            _module = module;
        }

        public async Task<ContainerLease> GetContainerLease(Guid containerLeaseId) {
            var query = new GetContainerLeaseQuery(containerLeaseId);

            var containerLease = await _module.ExecuteQueryAsync(query);

            return (ContainerLease)containerLease;
        }

        public async Task<List<ContainerLease>> GetContainerLeases(int? page, int? perPage) {
            var query = new GetContainerLeasesQuery(page, perPage);

            var containerLeases = await _module.ExecuteQueryAsync(query);

            return (from containerLease in containerLeases select (ContainerLease)containerLease).ToList();
        }
    }
}