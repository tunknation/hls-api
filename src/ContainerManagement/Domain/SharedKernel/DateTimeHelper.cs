﻿using System;

namespace HLS.ContainerManagement.Domain.SharedKernel {
    public static class DateTimeHelper {
        public static DateTime AddMonths(DateTime date, int months) {
            var next = date.AddMonths(months);

            if (date.Day != DateTime.DaysInMonth(date.Year, date.Month)) {
                return next;
            }

            var addDays = DateTime.DaysInMonth(next.Year, next.Month) - next.Day;
            return next.AddDays(addDays);
        }

        public static int GetMonthsBetween(DateTime from, DateTime to) {
            if (from > to) {
                throw new InvalidOperationException();
            }

            var monthDiff = Math.Abs(to.Year * 12 + (to.Month - 1) - (from.Year * 12 + (from.Month - 1)));

            if (from.AddMonths(monthDiff) > to || to.Day < from.Day) {
                return monthDiff - 1;
            }

            return monthDiff;
        }
    }
}