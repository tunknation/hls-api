﻿using System;
using System.Collections.Generic;

namespace HLS.ContainerManagement.Domain.SharedKernel {
    public static class DateObservancesHelper {
        private static readonly TimeSpan OneDay = new(24, 0, 0);
        private static readonly Dictionary<int, List<DateTime>> Cache = new();

        private static DateTime _calculateEaster(int year) {
            var a = year % 19;
            var b = year / 100;
            var c = year % 100;
            var d = b / 4;
            var e = b % 4;
            var f = (b + 8) / 25;
            var g = (b - f + 1) / 3;
            var h = (19 * a + b - d - g + 15) % 30;
            var i = c / 4;
            var k = c % 4;
            var l = (32 + 2 * e + 2 * i - h - k) % 7;
            var m = (a + 11 * h + 22 * l) / 451;
            var n0 = h + l + 7 * m + 114;
            var n = n0 / 31;
            var p = n0 % 31 + 1;

            return new DateTime(year, n, p);
        }

        private static List<DateTime> _calculatePublicHolidays(int year) {
            if (Cache.TryGetValue(year, out var publicHolidays)) {
                return publicHolidays;
            }

            var easter = _calculateEaster(year);
            var newYearsDay = new DateTime(year, 1, 1);
            var palmSunday = easter.Subtract(OneDay * 7);
            var maundyThursday = easter.Subtract(OneDay * 3);
            var goodFriday = easter.Subtract(OneDay * 2);
            var easterMonday = easter.Add(OneDay);
            var labourDay = new DateTime(year, 5, 1);
            var ascensionDay = easter.Add(OneDay * 39);
            var constitutionDay = new DateTime(year, 5, 17);
            var whitSunday = easter.Add(OneDay * 49);
            var whitMonday = easter.Add(OneDay * 50);
            var christmasDay = new DateTime(year, 12, 25);
            var christmasDay2 = new DateTime(year, 12, 26);

            publicHolidays = new List<DateTime> {
                newYearsDay.Date,
                palmSunday.Date,
                maundyThursday.Date,
                goodFriday.Date,
                easter.Date,
                easterMonday.Date,
                labourDay.Date,
                ascensionDay.Date,
                constitutionDay.Date,
                whitSunday.Date,
                whitMonday.Date,
                christmasDay.Date,
                christmasDay2.Date
            };

            Cache.Add(year, publicHolidays);

            return publicHolidays;
        }

        public static bool IsRedDay(DateTime date) {
            return date.DayOfWeek is DayOfWeek.Sunday || _calculatePublicHolidays(date.Year).Contains(date.Date);
        }

        public static bool IsGreyDay(DateTime date) {
            return date.DayOfWeek is DayOfWeek.Saturday;
        }

        public static bool IsBlackDay(DateTime date) {
            return !IsRedDay(date) && !IsGreyDay(date);
        }
    }
}