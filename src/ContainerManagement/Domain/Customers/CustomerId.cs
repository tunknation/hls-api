﻿using System;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Customers {
    public class CustomerId : TypedIdValueBase {
        public CustomerId(Guid value) : base(value) { }
    }
}