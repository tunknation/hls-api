﻿using System;
using HLS.ContainerManagement.Domain.SharedKernel;

namespace HLS.ContainerManagement.Domain.ContainerLeases {
    public static class ContainerLeaseExpirationDateCalculator {
        public static DateTime CalculateExpirationDate(DateTime start) {
            var months = DateTimeHelper.GetMonthsBetween(start, SystemClock.Today) + 1;

            return DateTimeHelper.AddMonths(start, months).AddDays(-1).Date;
        }
    }
}