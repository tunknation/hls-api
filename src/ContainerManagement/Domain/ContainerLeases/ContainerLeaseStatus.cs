﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Domain.ContainerLeases {
    public class ContainerLeaseStatus : ValueObject {
        [JsonConstructor]
        private ContainerLeaseStatus(string code) {
            Code = code;
        }

        public static ContainerLeaseStatus Active => new(nameof(Active));

        public static ContainerLeaseStatus Expired => new(nameof(Expired));

        public static ContainerLeaseStatus Completed => new(nameof(Completed));

        public string Code { get; }

        public static ContainerLeaseStatus Of(string code) {
            return new ContainerLeaseStatus(code);
        }

        public static implicit operator string(ContainerLeaseStatus status) {
            return status.Code;
        }
    }
}