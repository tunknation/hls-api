﻿using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Events {
    public class ContainerLeaseExpiredDomainEvent : DomainEventBase {
        public ContainerLeaseExpiredDomainEvent(
            ContainerLeaseId containerLeaseId,
            ContainerLeaseStatus status) {
            ContainerLeaseId = containerLeaseId;
            Status = status;
        }

        public ContainerLeaseId ContainerLeaseId { get; }

        public ContainerLeaseStatus Status { get; }
    }
}