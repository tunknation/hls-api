﻿using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Events {
    public class ContainerLeasedDomainEvent : DomainEventBase {
        public ContainerLeasedDomainEvent(
            ContainerLeaseId containerLeaseId,
            ContainerLeaseMonth month) {
            ContainerLeaseId = containerLeaseId;
            Month = month;
        }

        public ContainerLeaseId ContainerLeaseId { get; }

        public ContainerLeaseMonth Month { get; }
    }
}