﻿using System;
using System.Collections.Generic;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Events {
    public class ContainerLeaseCompletedDomainEvent : DomainEventBase {
        public ContainerLeaseCompletedDomainEvent(
            ContainerLeaseId containerLeaseId,
            ContainerLeaseStatus status,
            DateTime expirationDate,
            DateTime completionDate,
            List<ContainerLeaseMonth> months) {
            ContainerLeaseId = containerLeaseId;
            Status = status;
            ExpirationDate = expirationDate;
            CompletionDate = completionDate;
            Months = months;
        }

        public ContainerLeaseId ContainerLeaseId { get; }

        public ContainerLeaseStatus Status { get; }

        public DateTime ExpirationDate { get; }

        public DateTime CompletionDate { get; }

        public List<ContainerLeaseMonth> Months { get; }
    }
}