﻿using System;
using System.Collections.Generic;
using HLS.ContainerManagement.Domain.Projects;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Events {
    public class NewContainerLeaseStartedDomainEvent : DomainEventBase {
        public NewContainerLeaseStartedDomainEvent(
            ContainerLeaseId containerLeaseId,
            ProjectId projectId,
            ContainerId containerId,
            ContainerLeaseStatus status,
            DateTime startDate,
            DateTime expirationDate,
            List<ContainerLeaseMonth> months) {
            ContainerLeaseId = containerLeaseId;
            ProjectId = projectId;
            ContainerId = containerId;
            Status = status;
            StartDate = startDate;
            ExpirationDate = expirationDate;
            Months = months;
        }

        public ContainerLeaseId ContainerLeaseId { get; }

        public ProjectId ProjectId { get; }

        public ContainerId ContainerId { get; }

        public ContainerLeaseStatus Status { get; }

        public DateTime StartDate { get; }

        public DateTime ExpirationDate { get; }

        public List<ContainerLeaseMonth> Months { get; }
    }
}