﻿using System;
using System.Collections.Generic;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Events {
    public class ContainerLeaseRenewedDomainEvent : DomainEventBase {
        public ContainerLeaseRenewedDomainEvent(
            ContainerLeaseId containerLeaseId,
            ContainerLeaseStatus status,
            DateTime expirationDate,
            List<ContainerLeaseMonth> months) {
            ContainerLeaseId = containerLeaseId;
            Status = status;
            ExpirationDate = expirationDate;
            Months = months;
        }

        public ContainerLeaseId ContainerLeaseId { get; }

        public ContainerLeaseStatus Status { get; }

        public DateTime ExpirationDate { get; }

        public List<ContainerLeaseMonth> Months { get; }
    }
}