﻿using System;
using System.Collections.Generic;
using HLS.ContainerManagement.Domain.ContainerLeases.Rules;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Domain.ContainerLeases {
    public class ContainerLeaseDay : ValueObject {
        [JsonConstructor]
        private ContainerLeaseDay(DateTime date, ContainerLeaseDayStatus status) {
            Date = date.Date;
            Status = status;
        }

        public DateTime Date { get; }

        public ContainerLeaseDayStatus Status { get; }

        public static ContainerLeaseDay Of(DateTime date, ContainerLeaseDayStatus status) {
            CheckRule(new ContainerLeaseDayCannotBeGreyRule(date));
            CheckRule(new ContainerLeaseDayCannotBeRedRule(date));

            return new ContainerLeaseDay(date, status);
        }

        public static ContainerLeaseDay Leased(DateTime date) {
            return Of(date, ContainerLeaseDayStatus.Leased);
        }

        public static ContainerLeaseDay NotLeased(DateTime date) {
            return Of(date, ContainerLeaseDayStatus.NotLeased);
        }

        public static List<ContainerLeaseDay> Days(DateTime startDate, DateTime endDate) {
            var days = new List<ContainerLeaseDay>();

            var pointer = startDate.Date;
            while ((endDate.Date - pointer).TotalDays >= 0) {
                if (DateObservancesHelper.IsBlackDay(pointer)) {
                    days.Add(NotLeased(pointer.Date));
                }

                pointer = pointer.AddDays(1);
            }

            return days;
        }

        public ContainerLeaseDay Lease() {
            CheckRule(new CannotLeaseDayThatAlreadyHasBeenLeasedRule(Status));

            return Leased(Date);
        }
    }
}