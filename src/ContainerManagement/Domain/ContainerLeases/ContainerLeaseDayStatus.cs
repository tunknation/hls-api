﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Domain.ContainerLeases {
    public class ContainerLeaseDayStatus : ValueObject {
        [JsonConstructor]
        private ContainerLeaseDayStatus(string code) {
            Code = code;
        }

        public static ContainerLeaseDayStatus NotLeased => new(nameof(NotLeased));

        public static ContainerLeaseDayStatus Leased => new(nameof(Leased));

        public string Code { get; }

        public static ContainerLeaseDayStatus Of(string code) {
            return new ContainerLeaseDayStatus(code);
        }

        public static implicit operator string(ContainerLeaseDayStatus status) {
            return status.Code;
        }
    }
}