﻿using System;
using HLS.ContainerManagement.Domain.SeedWork;

namespace HLS.ContainerManagement.Domain.ContainerLeases {
    public class ContainerLeaseId : AggregateId<ContainerLease> {
        public ContainerLeaseId(Guid value) : base(value) { }
    }
}