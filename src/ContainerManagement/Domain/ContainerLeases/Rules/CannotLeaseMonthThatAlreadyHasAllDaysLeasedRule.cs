﻿using System.Collections.Generic;
using System.Linq;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotLeaseMonthThatAlreadyHasAllDaysLeasedRule : IBusinessRule {
        private readonly List<ContainerLeaseDay> _days;

        public CannotLeaseMonthThatAlreadyHasAllDaysLeasedRule(List<ContainerLeaseDay> days) {
            _days = days;
        }

        public bool IsBroken() {
            return _days.All(day => day.Status == ContainerLeaseDayStatus.Leased);
        }

        public string Message => "Cannot lease a month that already has all days leased";
    }
}