﻿using System;
using System.Collections.Generic;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotRevertLeasedDayThatDoesNotExistInMonthRule : IBusinessRule {
        private readonly DateTime _date;
        private readonly List<ContainerLeaseDay> _days;

        public CannotRevertLeasedDayThatDoesNotExistInMonthRule(List<ContainerLeaseDay> days, DateTime date) {
            _days = days;
            _date = date;
        }

        public bool IsBroken() {
            return !_days.Exists(day => day.Date == _date);
        }

        public string Message => "Cannot revert a leased day that does not exist in month";
    }
}