﻿using System.Collections.Generic;
using System.Linq;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotLeaseDayThatDoesNotExistInContainerLeaseRule : IBusinessRule {
        private readonly ContainerLeaseDay _day;
        private readonly List<ContainerLeaseDay> _days;

        public CannotLeaseDayThatDoesNotExistInContainerLeaseRule(
            List<ContainerLeaseMonth> months,
            ContainerLeaseDay day) {
            _days = months.SelectMany(month => month.Days).ToList();
            _day = day;
        }

        public bool IsBroken() {
            return !_days.Contains(_day);
        }

        public string Message => "Cannot lease a day that does not exist in container lease";
    }
}