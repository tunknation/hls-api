﻿using System;
using System.Collections.Generic;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotRevertLeasedDayThatAlreadyIsNotLeasedRule : IBusinessRule {
        private readonly List<ContainerLeaseDay> _days;
        private readonly ContainerLeaseDay _day;

        public CannotRevertLeasedDayThatAlreadyIsNotLeasedRule(List<ContainerLeaseDay> days, DateTime date) {
            _days = days;
            _day = ContainerLeaseDay.NotLeased(date);
        }

        public bool IsBroken() {
            return _days.Exists(day => day == _day);
        }

        public string Message => "Cannot lease a day that already has been leased";
    }
}