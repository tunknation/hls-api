﻿using System;
using System.Collections.Generic;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotRevertLeasedDayThatDoesNotExistInMonthIntervalRule : IBusinessRule {
        private readonly DateTime _date;
        private readonly List<ContainerLeaseMonth> _months;

        public CannotRevertLeasedDayThatDoesNotExistInMonthIntervalRule(
            List<ContainerLeaseMonth> months,
            DateTime date) {
            _months = months;
            _date = date;
        }

        public bool IsBroken() {
            return !_months.Exists(month => month.StartDate >= _date && _date <= month.EndDate);
        }

        public string Message => "Cannot revert a leased day that does not exist in month interval";
    }
}