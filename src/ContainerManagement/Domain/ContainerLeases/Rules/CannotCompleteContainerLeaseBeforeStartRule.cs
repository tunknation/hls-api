﻿using System;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotCompleteContainerLeaseBeforeStartRule : IBusinessRule {
        private readonly DateTime _startDate;
        private readonly DateTime _completionDate;

        public CannotCompleteContainerLeaseBeforeStartRule(DateTime startDate, DateTime completionDate) {
            _startDate = startDate;
            _completionDate = completionDate;
        }

        public bool IsBroken() {
            return _completionDate < _startDate;
        }

        public string Message => "Cannot complete a container lease before the start";
    }
}