﻿using System;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class ContainerLeaseDayCannotBeRedRule : IBusinessRule {
        private readonly DateTime _date;

        public ContainerLeaseDayCannotBeRedRule(DateTime date) {
            _date = date;
        }

        public bool IsBroken() {
            return DateObservancesHelper.IsRedDay(_date);
        }

        public string Message => "Container lease day cannot be a red day";
    }
}