﻿using System;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class ContainerLeaseDayCannotBeGreyRule : IBusinessRule {
        private readonly DateTime _date;

        public ContainerLeaseDayCannotBeGreyRule(DateTime date) {
            _date = date;
        }

        public bool IsBroken() {
            return DateObservancesHelper.IsGreyDay(_date);
        }

        public string Message => "Container lease day cannot be a grey day";
    }
}