﻿using System;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotStartContainerLeaseInTheFutureRule : IBusinessRule {
        private readonly DateTime _startDate;

        public CannotStartContainerLeaseInTheFutureRule(DateTime startDate) {
            _startDate = startDate;
        }

        public bool IsBroken() {
            return SystemClock.Now < _startDate;
        }

        public string Message => "Cannot start a container lease in the future";
    }
}