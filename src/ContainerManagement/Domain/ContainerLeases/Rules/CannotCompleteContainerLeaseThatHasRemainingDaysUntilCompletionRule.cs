﻿using System;
using System.Collections.Generic;
using System.Linq;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotCompleteContainerLeaseThatHasRemainingDaysUntilCompletionRule : IBusinessRule {
        private readonly List<ContainerLeaseDay> _days;
        private readonly DateTime _completionDate;

        public CannotCompleteContainerLeaseThatHasRemainingDaysUntilCompletionRule(
            List<ContainerLeaseMonth> months,
            DateTime completionDate) {
            _days = months.SelectMany(month => month.Days).ToList();
            _completionDate = completionDate;
        }

        public bool IsBroken() {
            return (from day in _days
                where day.Date <= _completionDate && day.Status == ContainerLeaseDayStatus.NotLeased
                select day).Any();
        }

        public string Message => "Cannot complete a container lease that has remaining days until completion";
    }
}