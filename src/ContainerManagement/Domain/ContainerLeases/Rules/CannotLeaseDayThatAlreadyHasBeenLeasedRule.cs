﻿using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotLeaseDayThatAlreadyHasBeenLeasedRule : IBusinessRule {
        private readonly ContainerLeaseDayStatus _status;

        public CannotLeaseDayThatAlreadyHasBeenLeasedRule(ContainerLeaseDayStatus status) {
            _status = status;
        }

        public bool IsBroken() {
            return _status == ContainerLeaseDayStatus.Leased;
        }

        public string Message => "Cannot lease a day that already has been leased";
    }
}