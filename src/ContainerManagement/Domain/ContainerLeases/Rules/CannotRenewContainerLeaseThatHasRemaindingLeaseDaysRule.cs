﻿using System.Collections.Generic;
using System.Linq;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotRenewContainerLeaseThatHasRemainingLeaseDaysRule : IBusinessRule {
        private readonly List<ContainerLeaseDay> _days;

        public CannotRenewContainerLeaseThatHasRemainingLeaseDaysRule(List<ContainerLeaseMonth> months) {
            _days = months.SelectMany(month => month.Days).ToList();
        }

        public bool IsBroken() {
            return !_days.TrueForAll(day => day == ContainerLeaseDay.Leased(day.Date));
        }

        public string Message => "Cannot renew container lease that has remaining lease days";
    }
}