﻿using System;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotCompleteContainerLeaseInTheFutureRule : IBusinessRule {
        private readonly DateTime _completionDate;

        public CannotCompleteContainerLeaseInTheFutureRule(DateTime completionDate) {
            _completionDate = completionDate;
        }

        public bool IsBroken() {
            return SystemClock.Now < _completionDate;
        }

        public string Message => "Cannot complete a container lease in the future";
    }
}