﻿using System.Collections.Generic;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotExtendMonthThatDoesNotExistInContainerLeaseRule : IBusinessRule {
        private readonly ContainerLeaseMonth _month;
        private readonly List<ContainerLeaseMonth> _months;

        public CannotExtendMonthThatDoesNotExistInContainerLeaseRule(
            List<ContainerLeaseMonth> months,
            ContainerLeaseMonth month) {
            _months = months;
            _month = month;
        }

        public bool IsBroken() {
            return !_months.Exists(month => month.StartDate == _month.StartDate);
        }

        public string Message => "Cannot extend a container lease that does not exist";
    }
}