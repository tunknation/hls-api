﻿using System.Collections.Generic;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases.Rules {
    public class CannotLeaseDayThatDoesNotExistInMonthRule : IBusinessRule {
        private readonly ContainerLeaseDay _day;
        private readonly List<ContainerLeaseDay> _days;

        public CannotLeaseDayThatDoesNotExistInMonthRule(
            List<ContainerLeaseDay> days,
            ContainerLeaseDay day) {
            _days = days;
            _day = day;
        }

        public bool IsBroken() {
            return !_days.Contains(_day);
        }

        public string Message => "Cannot lease a day that does not exist in month";
    }
}