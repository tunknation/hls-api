﻿using System;
using System.Collections.Generic;
using System.Linq;
using HLS.ContainerManagement.Domain.ContainerLeases.Events;
using HLS.ContainerManagement.Domain.ContainerLeases.Rules;
using HLS.ContainerManagement.Domain.Projects;
using HLS.ContainerManagement.Domain.SeedWork;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerLeases {
    public class ContainerLease : AggregateRoot<ContainerLeaseId> {
        private ProjectId _projectId;

        private ContainerId _containerId;

        private ContainerLeaseStatus _status;

        private DateTime _startDate;

        private DateTime _expirationDate;

        private DateTime? _completionDate;

        private List<ContainerLeaseMonth> _months;

        public static ContainerLease StartNewContainerLease(
            ProjectId projectId,
            ContainerId containerId,
            DateTime startDate) {
            CheckRule(new CannotStartContainerLeaseInTheFutureRule(startDate));

            var containerLease = new ContainerLease();

            var id = new ContainerLeaseId(Guid.NewGuid());
            var status = ContainerLeaseStatus.Active;

            var expirationDate =
                ContainerLeaseExpirationDateCalculator.CalculateExpirationDate(startDate);
            var months = ContainerLeaseMonth.Months(startDate, expirationDate);

            var @event = new NewContainerLeaseStartedDomainEvent(
                id,
                projectId,
                containerId,
                status,
                startDate,
                expirationDate,
                months);

            containerLease.Apply(@event);
            containerLease.AddDomainEvent(@event);

            return containerLease;
        }

        public void LeaseContainer(ContainerLeaseDay day) {
            CheckRule(new CannotLeaseDayThatDoesNotExistInContainerLeaseRule(_months, day));

            var month = _getMonthByDay(day).Lease(day);

            var @event = new ContainerLeasedDomainEvent(Id, month);

            Apply(@event);
            AddDomainEvent(@event);
        }

        public void ExtendContainerLeaseForFullMonth(ContainerLeaseMonth month) {
            CheckRule(new CannotExtendMonthThatDoesNotExistInContainerLeaseRule(_months, month));

            month = month.LeaseAll();

            var @event = new ContainerLeaseExtendedForFullMonthDomainEvent(Id, month);

            Apply(@event);
            AddDomainEvent(@event);
        }

        public void ExpireContainerLease() {
            var status = ContainerLeaseStatus.Expired;

            var @event = new ContainerLeaseExpiredDomainEvent(Id, status);

            Apply(@event);
            AddDomainEvent(@event);
        }

        public void RenewContainerLease() {
            CheckRule(new CannotRenewContainerLeaseThatHasRemainingLeaseDaysRule(_months));

            var status = ContainerLeaseStatus.Active;

            var expirationDate =
                ContainerLeaseExpirationDateCalculator.CalculateExpirationDate(_expirationDate);
            var months = ContainerLeaseMonth.Months(_expirationDate, expirationDate);

            var @event = new ContainerLeaseRenewedDomainEvent(
                Id,
                status,
                expirationDate,
                months);

            Apply(@event);
            AddDomainEvent(@event);
        }

        public void CompleteContainerLease(DateTime completionDate) {
            CheckRule(new CannotCompleteContainerLeaseInTheFutureRule(completionDate));
            CheckRule(new CannotCompleteContainerLeaseBeforeStartRule(_startDate, completionDate));
            CheckRule(new CannotCompleteContainerLeaseThatHasRemainingDaysUntilCompletionRule(_months, completionDate));

            var status = ContainerLeaseStatus.Completed;
            var expirationDate = completionDate.Date;
            var months = ContainerLeaseMonth.Months(_startDate, completionDate);

            for (var i = 0; i < months.Count; i++) {
                var month = months[i];

                foreach (var day in month.Days) {
                    if ((completionDate.Date - day.Date).TotalDays >= 0) {
                        month = month.Lease(day);
                    }
                }

                months[i] = month;
            }

            var @event = new ContainerLeaseCompletedDomainEvent(
                Id,
                status,
                expirationDate,
                completionDate,
                months);

            Apply(@event);
            AddDomainEvent(@event);
        }

        private ContainerLeaseMonth _getMonthByDay(ContainerLeaseDay day) {
            return (
                from month in _months
                where month.Days.Contains(day)
                select month).Single();
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(NewContainerLeaseStartedDomainEvent @event) {
            Id = @event.ContainerLeaseId;
            _projectId = @event.ProjectId;
            _containerId = @event.ContainerId;
            _status = @event.Status;
            _startDate = @event.StartDate;
            _expirationDate = @event.ExpirationDate;
            _months = @event.Months;
        }

        private void _when(ContainerLeasedDomainEvent @event) {
            var idx = _months.FindIndex(month => month.StartDate == @event.Month.StartDate);

            if (idx == -1) {
                return;
            }

            _months[idx] = @event.Month;
        }

        private void _when(ContainerLeaseExtendedForFullMonthDomainEvent @event) {
            var idx = _months.FindIndex(month => month.StartDate == @event.Month.StartDate);

            if (idx == -1) {
                return;
            }

            _months[idx] = @event.Month;
        }

        private void _when(ContainerLeaseExpiredDomainEvent @event) {
            _status = @event.Status;
        }

        private void _when(ContainerLeaseRenewedDomainEvent @event) {
            _status = @event.Status;
            _expirationDate = @event.ExpirationDate;

            foreach (var month in @event.Months) {
                _months.Add(month);
            }
        }

        private void _when(ContainerLeaseCompletedDomainEvent @event) {
            _status = @event.Status;
            _expirationDate = @event.ExpirationDate;
            _completionDate = @event.CompletionDate;
            _months = @event.Months;
        }
    }
}