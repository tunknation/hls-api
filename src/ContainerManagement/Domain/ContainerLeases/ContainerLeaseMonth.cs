﻿using System;
using System.Collections.Generic;
using System.Linq;
using HLS.ContainerManagement.Domain.ContainerLeases.Rules;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Domain.ContainerLeases {
    public class ContainerLeaseMonth : ValueObject {
        [JsonConstructor]
        private ContainerLeaseMonth(DateTime startDate, DateTime endDate, List<ContainerLeaseDay> days) {
            StartDate = startDate.Date;
            EndDate = endDate.Date;
            Days = days;
        }

        public DateTime StartDate { get; }

        public DateTime EndDate { get; }

        public IReadOnlyList<ContainerLeaseDay> Days { get; }

        public static ContainerLeaseMonth Of(DateTime startDate) {
            var endDate = DateTimeHelper.AddMonths(startDate, 1).AddDays(-1);
            var days = ContainerLeaseDay.Days(startDate, endDate);

            return new ContainerLeaseMonth(startDate, endDate, days);
        }

        public static List<ContainerLeaseMonth> Months(DateTime startDate, DateTime endDate) {
            var months = new List<ContainerLeaseMonth>();

            var pointer = startDate.Date;
            while ((endDate.Date - pointer).TotalDays >= 0) {
                months.Add(Of(pointer.Date));
                pointer = DateTimeHelper.AddMonths(pointer, 1);
            }

            return months;
        }

        public ContainerLeaseMonth Lease(ContainerLeaseDay day) {
            CheckRule(new CannotLeaseDayThatDoesNotExistInMonthRule(Days.ToList(), day));

            var days = Days.ToList();
            var idx = days.FindIndex(d => d == day);

            if (idx == -1) {
                return this;
            }

            days[idx] = day.Lease();

            return new ContainerLeaseMonth(StartDate, EndDate, days);
        }

        public ContainerLeaseMonth LeaseAll() {
            CheckRule(new CannotLeaseMonthThatAlreadyHasAllDaysLeasedRule(Days.ToList()));

            var month = this;

            foreach (var day in month.Days) {
                if (day.Status == ContainerLeaseDayStatus.Leased) {
                    continue;
                }

                month = month.Lease(day);
            }

            return month;
        }
    }
}