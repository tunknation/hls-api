﻿using System;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.ContainerTypes {
    public class ContainerTypeId : TypedIdValueBase {
        public ContainerTypeId(Guid value) : base(value) { }
    }
}