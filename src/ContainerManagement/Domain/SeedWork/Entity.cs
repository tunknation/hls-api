﻿using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.SeedWork {
    public abstract class Entity<T> where T : TypedIdValueBase {
        public T Id { get; protected set; }
    }
}