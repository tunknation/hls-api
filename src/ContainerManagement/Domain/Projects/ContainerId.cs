﻿using System;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects {
    public class ContainerId : TypedIdValueBase {
        public ContainerId(Guid value) : base(value) { }
    }
}