﻿using System;
using HLS.ContainerManagement.Domain.ContainerTypes;
using HLS.ContainerManagement.Domain.SeedWork;

namespace HLS.ContainerManagement.Domain.Projects {
    public class Container : Entity<ContainerId> {
        public ContainerStatus Status { get; private set; }

        public ContainerTypeId ContainerTypeId { get; private set; }

        public string DescriptiveName { get; private set; }

        public DateTime DeliveryDate { get; private set; }

        public DateTime? RetrievementDate { get; private set; }

        public static Container DeliverContainer(
            ContainerId id,
            ContainerStatus status,
            ContainerTypeId containerTypeId,
            DateTime deliveryDate) {
            return new Container {
                Id = id,
                Status = status,
                ContainerTypeId = containerTypeId,
                DeliveryDate = deliveryDate
            };
        }

        public void RetrieveContainer(ContainerStatus status, DateTime retrievementDate) {
            Status = status;
            RetrievementDate = retrievementDate;
        }

        public void AddDescriptiveContainerName(string descriptiveName) {
            DescriptiveName = descriptiveName;
        }
    }
}