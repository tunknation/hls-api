﻿using System;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects.Rules {
    public class CannotCompleteProjectInTheFutureRule : IBusinessRule {
        private readonly DateTime _completionDate;

        public CannotCompleteProjectInTheFutureRule(DateTime? completionDate) {
            _completionDate = completionDate ?? SystemClock.Now;
        }

        public bool IsBroken() {
            return SystemClock.Now < _completionDate;
        }

        public string Message => "Cannot complete a project in the future";
    }
}