﻿using System;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects.Rules {
    public class CannotDeliverContainerInTheFutureRule : IBusinessRule {
        private readonly DateTime _deliveryDate;

        public CannotDeliverContainerInTheFutureRule(DateTime? deliveryDate) {
            _deliveryDate = deliveryDate ?? SystemClock.Now;
        }

        public bool IsBroken() {
            return SystemClock.Now < _deliveryDate;
        }

        public string Message => "Cannot deliver a container in the future";
    }
}