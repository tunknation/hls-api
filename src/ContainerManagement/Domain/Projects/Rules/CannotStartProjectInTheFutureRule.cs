﻿using System;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects.Rules {
    public class CannotStartProjectInTheFutureRule : IBusinessRule {
        private readonly DateTime _startDate;

        public CannotStartProjectInTheFutureRule(DateTime? startDate) {
            _startDate = startDate ?? SystemClock.Now;
        }

        public bool IsBroken() {
            return SystemClock.Now < _startDate;
        }

        public string Message => "Cannot start a project in the future";
    }
}