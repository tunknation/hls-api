﻿using System.Collections.Generic;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects.Rules {
    public class CannotRetrieveContainerThatHasAlreadyBeenRetrievedRule : IBusinessRule {
        private readonly List<Container> _containers;
        private readonly ContainerId _containerId;

        public CannotRetrieveContainerThatHasAlreadyBeenRetrievedRule(
            List<Container> containers,
            ContainerId containerId) {
            _containers = containers;
            _containerId = containerId;
        }

        public bool IsBroken() {
            return _containers.Exists(container =>
                container.Id == _containerId && container.Status == ContainerStatus.Retrieved);
        }

        public string Message => "Cannot retrieve a container that has already been retrieved";
    }
}