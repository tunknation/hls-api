﻿using System;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects.Rules {
    public class CannotRetrieveContainerInTheFutureRule : IBusinessRule {
        private readonly DateTime _retrievementDate;

        public CannotRetrieveContainerInTheFutureRule(DateTime? retrievementDate) {
            _retrievementDate = retrievementDate ?? SystemClock.Now;
        }

        public bool IsBroken() {
            return SystemClock.Now < _retrievementDate;
        }

        public string Message => "Cannot retrieve a container in the future";
    }
}