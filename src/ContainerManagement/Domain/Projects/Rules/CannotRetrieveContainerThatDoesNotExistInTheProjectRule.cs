﻿using System.Collections.Generic;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects.Rules {
    public class CannotRetrieveContainerThatDoesNotExistInTheProjectRule : IBusinessRule {
        private readonly List<Container> _containers;
        private readonly ContainerId _containerId;

        public CannotRetrieveContainerThatDoesNotExistInTheProjectRule(
            List<Container> containers,
            ContainerId containerId) {
            _containers = containers;
            _containerId = containerId;
        }

        public bool IsBroken() {
            return !_containers.Exists(container => container.Id == _containerId);
        }

        public string Message => "Cannot retrieve a container that does not exist in the project";
    }
}