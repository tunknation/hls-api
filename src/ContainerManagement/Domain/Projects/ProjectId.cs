﻿using System;
using HLS.ContainerManagement.Domain.SeedWork;

namespace HLS.ContainerManagement.Domain.Projects {
    public class ProjectId : AggregateId<Project> {
        public ProjectId(Guid value) : base(value) { }
    }
}