﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Domain.Projects {
    public class ContainerStatus : ValueObject {
        [JsonConstructor]
        private ContainerStatus(string code) {
            Code = code;
        }

        public static ContainerStatus Delivered => new(nameof(Delivered));

        public static ContainerStatus Retrieved => new(nameof(Retrieved));

        public string Code { get; }

        public static ContainerStatus Of(string code) {
            return new ContainerStatus(code);
        }

        public static implicit operator string(ContainerStatus status) {
            return status.Code;
        }
    }
}