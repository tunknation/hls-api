﻿using System;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects.Events {
    public class ProjectCompletedDomainEvent : DomainEventBase {
        public ProjectCompletedDomainEvent(ProjectId projectId, ProjectStatus status, DateTime completionDate) {
            ProjectId = projectId;
            Status = status;
            CompletionDate = completionDate;
        }

        public ProjectId ProjectId { get; }

        public ProjectStatus Status { get; }

        public DateTime CompletionDate { get; }
    }
}