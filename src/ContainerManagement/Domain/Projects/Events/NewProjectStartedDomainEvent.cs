﻿using System;
using HLS.ContainerManagement.Domain.Customers;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects.Events {
    public class NewProjectStartedDomainEvent : DomainEventBase {
        public NewProjectStartedDomainEvent(
            ProjectId projectId,
            ProjectStatus status,
            CustomerId customerId,
            ProjectAddress address,
            DateTime startDate) {
            ProjectId = projectId;
            Status = status;
            CustomerId = customerId;
            Address = address;
            StartDate = startDate;
        }

        public ProjectId ProjectId { get; }

        public ProjectStatus Status { get; }

        public CustomerId CustomerId { get; }

        public ProjectAddress Address { get; }

        public DateTime StartDate { get; }
    }
}