﻿using System;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects.Events {
    public class ContainerRetrievedFromProjectDomainEvent : DomainEventBase {
        public ContainerRetrievedFromProjectDomainEvent(
            ContainerId containerId,
            ContainerStatus containerStatus,
            DateTime retrievementDate) {
            ContainerId = containerId;
            ContainerStatus = containerStatus;
            RetrievementDate = retrievementDate;
        }

        public ContainerId ContainerId { get; }

        public ContainerStatus ContainerStatus { get; }

        public DateTime RetrievementDate { get; }
    }
}