﻿using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects.Events {
    public class DescriptiveContainerNameAddedDomainEvent : DomainEventBase {
        public DescriptiveContainerNameAddedDomainEvent(
            ContainerId containerId,
            string descriptiveContainerName) {
            ContainerId = containerId;
            DescriptiveContainerName = descriptiveContainerName;
        }

        public ContainerId ContainerId { get; }

        public string DescriptiveContainerName { get; }
    }
}