﻿using System;
using HLS.ContainerManagement.Domain.ContainerTypes;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects.Events {
    public class ContainerDeliveredToProjectDomainEvent : DomainEventBase {
        public ContainerDeliveredToProjectDomainEvent(
            ProjectId projectId,
            ContainerId containerId,
            ContainerStatus containerStatus,
            ContainerTypeId containerTypeId,
            DateTime deliveryDate) {
            ProjectId = projectId;
            ContainerId = containerId;
            ContainerTypeId = containerTypeId;
            DeliveryDate = deliveryDate;
            ContainerStatus = containerStatus;
        }

        public ProjectId ProjectId { get; }

        public ContainerId ContainerId { get; }

        public ContainerStatus ContainerStatus { get; }

        public ContainerTypeId ContainerTypeId { get; }

        public DateTime DeliveryDate { get; }
    }
}