﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Domain.Projects {
    public class ProjectStatus : ValueObject {
        [JsonConstructor]
        private ProjectStatus(string code) {
            Code = code;
        }

        public static ProjectStatus Active => new(nameof(Active));

        public static ProjectStatus Completed => new(nameof(Completed));

        public string Code { get; }

        public static ProjectStatus Of(string code) {
            return new ProjectStatus(code);
        }

        public static implicit operator string(ProjectStatus status) {
            return status.Code;
        }
    }
}