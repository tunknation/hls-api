﻿using System;
using System.Collections.Generic;
using HLS.ContainerManagement.Domain.ContainerTypes;
using HLS.ContainerManagement.Domain.Customers;
using HLS.ContainerManagement.Domain.Projects.Events;
using HLS.ContainerManagement.Domain.Projects.Rules;
using HLS.ContainerManagement.Domain.SeedWork;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Domain.Projects {
    public class Project : AggregateRoot<ProjectId> {
        private ProjectStatus _status;

        private CustomerId _customerId;

        private ProjectAddress _address;

        private DateTime _startDate;

        private DateTime _completionDate;

        private List<Container> _containers;

        public static Project StartNewProject(
            CustomerId customerId,
            ContainerTypeId containerTypeId,
            ProjectAddress address,
            DateTime? startDate) {
            CheckRule(new CannotStartProjectInTheFutureRule(startDate));

            var project = new Project();

            var id = new ProjectId(Guid.NewGuid());
            var status = ProjectStatus.Active;
            var date = startDate?.Date ?? SystemClock.Today;

            var @event = new NewProjectStartedDomainEvent(
                id,
                status,
                customerId,
                address,
                date);

            project.Apply(@event);
            project.AddDomainEvent(@event);

            project.DeliverContainerToProject(containerTypeId, date);

            return project;
        }

        public ProjectSnapshot GetSnapshot() {
            return new ProjectSnapshot(
                Id,
                _customerId,
                _address);
        }

        public ContainerId DeliverContainerToProject(ContainerTypeId containerTypeId, DateTime? deliveryDate) {
            CheckRule(new CannotDeliverContainerInTheFutureRule(deliveryDate));

            var containerId = new ContainerId(Guid.NewGuid());
            var containerStatus = ContainerStatus.Delivered;
            var date = deliveryDate?.Date ?? SystemClock.Today;

            var @event = new ContainerDeliveredToProjectDomainEvent(
                Id,
                containerId,
                containerStatus,
                containerTypeId,
                date);

            Apply(@event);
            AddDomainEvent(@event);

            return containerId;
        }

        public void AddDescriptiveContainerName(ContainerId containerId, string descriptiveName) {
            var @event = new DescriptiveContainerNameAddedDomainEvent(containerId, descriptiveName);

            Apply(@event);
            AddDomainEvent(@event);
        }

        public void RetrieveContainerFromProject(ContainerId containerId, DateTime? retrievementDate) {
            CheckRule(new CannotRetrieveContainerThatDoesNotExistInTheProjectRule(_containers, containerId));
            CheckRule(new CannotRetrieveContainerThatHasAlreadyBeenRetrievedRule(_containers, containerId));
            CheckRule(new CannotRetrieveContainerInTheFutureRule(retrievementDate));

            var containerStatus = ContainerStatus.Retrieved;
            var date = retrievementDate?.Date ?? SystemClock.Today;

            var @event = new ContainerRetrievedFromProjectDomainEvent(containerId, containerStatus, date);

            Apply(@event);
            AddDomainEvent(@event);

            if (!_containers.TrueForAll(container => container.Status == ContainerStatus.Retrieved)) {
                _completeProject(SystemClock.Today);
            }
        }

        private void _completeProject(DateTime? completionDate) {
            CheckRule(new CannotCompleteProjectInTheFutureRule(completionDate));

            var status = ProjectStatus.Completed;
            var date = completionDate?.Date ?? SystemClock.Today;

            var @event = new ProjectCompletedDomainEvent(Id, status, date);

            Apply(@event);
            AddDomainEvent(@event);
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(NewProjectStartedDomainEvent @event) {
            Id = @event.ProjectId;
            _status = @event.Status;
            _customerId = @event.CustomerId;
            _address = @event.Address;
            _startDate = @event.StartDate;
            _containers = new List<Container>();
        }

        private void _when(ContainerDeliveredToProjectDomainEvent @event) {
            var container = Container.DeliverContainer(
                @event.ContainerId,
                @event.ContainerStatus,
                @event.ContainerTypeId,
                @event.DeliveryDate);
            _containers.Add(container);
        }

        private void _when(DescriptiveContainerNameAddedDomainEvent @event) {
            var container = _containers.Find(container => container.Id == @event.ContainerId);
            container?.AddDescriptiveContainerName(@event.DescriptiveContainerName);
        }

        private void _when(ContainerRetrievedFromProjectDomainEvent @event) {
            var container = _containers.Find(container => container.Id == @event.ContainerId);
            container?.RetrieveContainer(@event.ContainerStatus, @event.RetrievementDate);
        }

        private void _when(ProjectCompletedDomainEvent @event) {
            _status = @event.Status;
            _completionDate = @event.CompletionDate;
        }
    }
}