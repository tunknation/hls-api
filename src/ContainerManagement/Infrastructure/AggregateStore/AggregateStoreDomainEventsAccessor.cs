﻿using System.Collections.Generic;
using System.Linq;
using HLS.ContainerManagement.Domain.SeedWork;
using HLS.Core.Domain;
using HLS.Core.Infrastructure.DomainEvents;

namespace HLS.ContainerManagement.Infrastructure.AggregateStore {
    public class AggregateStoreDomainEventsAccessor : IDomainEventsAccessor {
        private readonly IAggregateStore _aggregateStore;

        public AggregateStoreDomainEventsAccessor(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public IReadOnlyCollection<IDomainEvent> GetAllDomainEvents() {
            return _aggregateStore
                .GetChanges()
                .ToList()
                .AsReadOnly();
        }

        public void ClearAllDomainEvents() {
            _aggregateStore.ClearChanges();
        }
    }
}