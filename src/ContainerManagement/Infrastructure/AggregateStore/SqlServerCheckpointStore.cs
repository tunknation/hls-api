﻿using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;

namespace HLS.ContainerManagement.Infrastructure.AggregateStore {
    public class SqlServerCheckpointStore : ICheckpointStore {
        private readonly ISqlConnectionFactory _sqlConnectionFactory;

        public SqlServerCheckpointStore(ISqlConnectionFactory sqlConnectionFactory) {
            _sqlConnectionFactory = sqlConnectionFactory;
        }

        public long? GetCheckpoint(SubscriptionCode subscriptionCode) {
            using var connection = _sqlConnectionFactory.GetOpenConnection();

            var checkpoint = connection.QuerySingleOrDefault<long?>(
                @"SELECT
                     [SubscriptionCheckpoint].[Position]
                     FROM [containermanagement].[SubscriptionCheckpoints] AS [SubscriptionCheckpoint]
                     WHERE [Code] = @Code",
                new {
                    Code = subscriptionCode
                });

            return checkpoint;
        }

        public async Task StoreCheckpoint(SubscriptionCode subscriptionCode, long checkpoint) {
            var actualCheckpoint = GetCheckpoint(subscriptionCode);

            using var connection = _sqlConnectionFactory.GetOpenConnection();
            if (actualCheckpoint == null) {
                await connection.ExecuteScalarAsync(
                    @"INSERT INTO [containermanagement].[SubscriptionCheckpoints] 
                         VALUES (@Code, @Position)",
                    new {
                        Code = subscriptionCode,
                        Position = checkpoint
                    });
            } else {
                await connection.ExecuteScalarAsync(
                    @"UPDATE [containermanagement].[SubscriptionCheckpoints]
                         SET Position = @Position
                         WHERE Code = @Code",
                    new {
                        Code = subscriptionCode,
                        Position = checkpoint
                    });
            }
        }
    }
}