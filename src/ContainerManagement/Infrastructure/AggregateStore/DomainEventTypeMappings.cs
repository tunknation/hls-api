﻿using System;
using System.Collections.Generic;
using HLS.ContainerManagement.Domain.ContainerLeases.Events;
using HLS.ContainerManagement.Domain.Projects.Events;

namespace HLS.ContainerManagement.Infrastructure.AggregateStore {
    internal static class DomainEventTypeMappings {
        static DomainEventTypeMappings() {
            Dictionary = new Dictionary<string, Type> {
                { nameof(NewProjectStartedDomainEvent), typeof(NewProjectStartedDomainEvent) },
                { nameof(ContainerDeliveredToProjectDomainEvent), typeof(ContainerDeliveredToProjectDomainEvent) },
                { nameof(ContainerRetrievedFromProjectDomainEvent), typeof(ContainerRetrievedFromProjectDomainEvent) },
                { nameof(ProjectCompletedDomainEvent), typeof(ProjectCompletedDomainEvent) },
                { nameof(NewContainerLeaseStartedDomainEvent), typeof(NewContainerLeaseStartedDomainEvent) },
                { nameof(ContainerLeasedDomainEvent), typeof(ContainerLeasedDomainEvent) },
                { nameof(ContainerLeaseExpiredDomainEvent), typeof(ContainerLeaseExpiredDomainEvent) },
                { nameof(ContainerLeaseRenewedDomainEvent), typeof(ContainerLeaseRenewedDomainEvent) },
                { nameof(DescriptiveContainerNameAddedDomainEvent), typeof(DescriptiveContainerNameAddedDomainEvent) },
                { nameof(ContainerLeaseCompletedDomainEvent), typeof(ContainerLeaseCompletedDomainEvent) }, {
                    nameof(ContainerLeaseExtendedForFullMonthDomainEvent),
                    typeof(ContainerLeaseExtendedForFullMonthDomainEvent)
                }
            };
        }

        internal static IDictionary<string, Type> Dictionary { get; }
    }
}