﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Core.Infrastructure.Outbox;

namespace HLS.ContainerManagement.Infrastructure.AggregateStore {
    public class SqlOutboxAccessor : IOutbox {
        private readonly List<OutboxMessage> _messages;
        private readonly ISqlConnectionFactory _sqlConnectionFactory;

        public SqlOutboxAccessor(ISqlConnectionFactory sqlConnectionFactory) {
            _sqlConnectionFactory = sqlConnectionFactory;
            _messages = new List<OutboxMessage>();
        }

        public void Add(OutboxMessage message) {
            _messages.Add(message);
        }

        public async Task Save() {
            if (!_messages.Any()) {
                return;
            }

            using var connection = _sqlConnectionFactory.CreateNewConnection();

            const string sql = @"INSERT INTO [containermanagement].[OutboxMessages]
                                    ([Id], [OccurredOn], [Type], [Data]) 
                                 VALUES (@Id, @OccurredOn, @Type, @Data)";

            foreach (var message in _messages) {
                await connection.ExecuteScalarAsync(sql, new {
                    message.Id,
                    message.OccurredOn,
                    message.Type,
                    message.Data
                });
            }

            _messages.Clear();
        }
    }
}