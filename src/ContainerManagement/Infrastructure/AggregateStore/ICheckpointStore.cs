﻿using System.Threading.Tasks;

namespace HLS.ContainerManagement.Infrastructure.AggregateStore {
    public interface ICheckpointStore {
        long? GetCheckpoint(SubscriptionCode subscriptionCode);

        Task StoreCheckpoint(SubscriptionCode subscriptionCode, long checkpoint);
    }
}