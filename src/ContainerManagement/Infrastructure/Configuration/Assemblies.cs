﻿using System.Reflection;
using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Infrastructure.Configuration {
    internal static class Assemblies {
        public static readonly Assembly Application = typeof(InternalCommandBase).Assembly;
    }
}