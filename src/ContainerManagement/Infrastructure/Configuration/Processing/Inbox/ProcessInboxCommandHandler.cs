﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.Core.Application.Data;
using MediatR;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Processing.Inbox {
    internal class ProcessInboxCommandHandler : ICommandHandler<ProcessInboxCommand> {
        private readonly IMediator _mediator;
        private readonly ISqlConnectionFactory _sqlConnectionFactory;

        public ProcessInboxCommandHandler(IMediator mediator, ISqlConnectionFactory sqlConnectionFactory) {
            _mediator = mediator;
            _sqlConnectionFactory = sqlConnectionFactory;
        }

        public async Task<Unit> Handle(ProcessInboxCommand command, CancellationToken cancellationToken) {
            var connection = _sqlConnectionFactory.GetOpenConnection();

            const string sql = @"SELECT [InboxMessage].[Id],
                                        [InboxMessage].[Type],
                                        [InboxMessage].[Data]
                                 FROM [containermanagement].[InboxMessages] AS [InboxMessage]
                                 WHERE [InboxMessage].[ProcessedDate] IS NULL
                                 ORDER BY [InboxMessage].[OccurredOn]";

            var messages = await connection.QueryAsync<InboxMessageDto>(sql);
            var messagesList = messages.AsList();

            const string sqlUpdateProcessedDate = @"UPDATE [containermanagement].[InboxMessages]
                                                    SET [ProcessedDate] = @Date
                                                    WHERE [Id] = @Id";

            foreach (var message in messagesList) {
                var messageAssembly = AppDomain.CurrentDomain.GetAssemblies()
                    .SingleOrDefault(assembly => message.Type.Contains(assembly.GetName().Name!));

                var type = messageAssembly!.GetType(message.Type);
                var request = JsonConvert.DeserializeObject(message.Data, type!);

                try {
                    await _mediator.Publish((INotification) request, cancellationToken);
                } catch (Exception e) {
                    Console.WriteLine(e);
                    throw;
                }

                await connection.ExecuteScalarAsync(sqlUpdateProcessedDate,
                    new {
                        Date = DateTime.UtcNow,
                        message.Id
                    });
            }

            return Unit.Value;
        }
    }
}