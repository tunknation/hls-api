﻿using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Processing.Inbox {
    public class ProcessInboxCommand : CommandBase, IRecurringCommand { }
}