﻿using System;
using HLS.ContainerManagement.Application.ContainerLeases.ExpireContainerLease;
using HLS.ContainerManagement.Application.ContainerLeases.LeaseContainer;
using HLS.ContainerManagement.Application.Projects.DeliverContainerToProject;
using HLS.ContainerManagement.Application.Projects.RetrieveContainerFromProject;
using HLS.ContainerManagement.Application.Projects.StartNewProject;
using HLS.Core.Infrastructure;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Processing {
    internal static class DomainNotificationTypeMappings {
        static DomainNotificationTypeMappings() {
            Dictionary = new BiDictionary<string, Type>();

            Dictionary.Add(
                nameof(ContainerDeliveredToProjectNotification),
                typeof(ContainerDeliveredToProjectNotification));
            Dictionary.Add(
                nameof(ContainerRetrievedFromProjectNotification),
                typeof(ContainerRetrievedFromProjectNotification));
            Dictionary.Add(
                nameof(ContainerLeaseExpiredNotification),
                typeof(ContainerLeaseExpiredNotification));
            Dictionary.Add(
                nameof(NewProjectStartedNotification),
                typeof(NewProjectStartedNotification));
            Dictionary.Add(
                nameof(ContainerLeasedNotification),
                typeof(ContainerLeasedNotification));
        }

        internal static BiDictionary<string, Type> Dictionary { get; }
    }
}