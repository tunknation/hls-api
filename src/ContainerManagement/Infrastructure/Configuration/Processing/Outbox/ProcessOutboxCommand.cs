﻿using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Processing.Outbox {
    public class ProcessOutboxCommand : CommandBase, IRecurringCommand { }
}