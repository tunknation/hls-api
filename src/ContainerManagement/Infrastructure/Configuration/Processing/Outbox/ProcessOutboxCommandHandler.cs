﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.Core.Application.Data;
using HLS.Core.Application.Events;
using HLS.Core.Infrastructure.DomainEvents;
using MediatR;
using Newtonsoft.Json;
using Serilog.Context;
using Serilog.Core;
using Serilog.Events;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Processing.Outbox {
    internal class ProcessOutboxCommandHandler : ICommandHandler<ProcessOutboxCommand> {
        private readonly IDomainNotificationsMapper _domainNotificationsMapper;
        private readonly IMediator _mediator;
        private readonly ISqlConnectionFactory _sqlConnectionFactory;

        public ProcessOutboxCommandHandler(
            IMediator mediator,
            ISqlConnectionFactory sqlConnectionFactory,
            IDomainNotificationsMapper domainNotificationsMapper) {
            _mediator = mediator;
            _sqlConnectionFactory = sqlConnectionFactory;
            _domainNotificationsMapper = domainNotificationsMapper;
        }

        public async Task<Unit> Handle(ProcessOutboxCommand command, CancellationToken cancellationToken) {
            var connection = _sqlConnectionFactory.GetOpenConnection();

            const string sql = @"SELECT [OutboxMessage].[Id],
                                        [OutboxMessage].[Type],
                                        [OutboxMessage].[Data]
                                 FROM [containermanagement].[OutboxMessages] AS [OutboxMessage]
                                 WHERE [OutboxMessage].[ProcessedDate] IS NULL
                                 ORDER BY [OutboxMessage].[OccurredOn]";

            var messages = await connection.QueryAsync<OutboxMessageDto>(sql);
            var messagesList = messages.AsList();

            if (messagesList.Count <= 0) {
                return Unit.Value;
            }

            const string sqlUpdateProcessedDate = @"UPDATE [containermanagement].[OutboxMessages]
                                                    SET [ProcessedDate] = @Date
                                                    WHERE [Id] = @Id";

            foreach (var message in messagesList) {
                var type = _domainNotificationsMapper.GetType(message.Type);
                var @event = JsonConvert.DeserializeObject(message.Data, type) as IDomainEventNotification;

                using (LogContext.Push(new OutboxMessageContextEnricher(@event))) {
                    await _mediator.Publish(@event, cancellationToken);

                    await connection.ExecuteAsync(sqlUpdateProcessedDate,
                        new {
                            Date = DateTime.UtcNow,
                            message.Id
                        });
                }
            }

            return Unit.Value;
        }

        private class OutboxMessageContextEnricher : ILogEventEnricher {
            private readonly IDomainEventNotification _notification;

            public OutboxMessageContextEnricher(IDomainEventNotification notification) {
                _notification = notification;
            }

            public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory) {
                logEvent.AddOrUpdateProperty(new LogEventProperty("Context",
                    new ScalarValue($"OutboxMessage:{_notification.Id.ToString()}")));
            }
        }
    }
}