﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using MediatR;
using Serilog;
using Serilog.Context;
using Serilog.Core;
using Serilog.Events;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Processing {
    internal class LoggingCommandHandlerDecorator<T> : ICommandHandler<T> where T : ICommand {
        private readonly ICommandHandler<T> _decorated;
        private readonly ILogger _logger;

        public LoggingCommandHandlerDecorator(
            ILogger logger,
            ICommandHandler<T> decorated) {
            _logger = logger;
            _decorated = decorated;
        }

        public async Task<Unit> Handle(T command, CancellationToken cancellationToken) {
            if (command is IRecurringCommand) {
                return await _decorated.Handle(command, cancellationToken);
            }

            using (
                LogContext.Push(
                    new CommandLogEnricher(command))) {
                try {
                    _logger.Information(
                        "Executing command {Command}",
                        command.GetType().Name);

                    var result = await _decorated.Handle(command, cancellationToken);

                    _logger.Information("Command {Command} processed successful", command.GetType().Name);

                    return result;
                } catch (Exception exception) {
                    _logger.Error(exception, "Command {Command} processing failed", command.GetType().Name);
                    throw;
                }
            }
        }

        private class CommandLogEnricher : ILogEventEnricher {
            private readonly ICommand _command;

            public CommandLogEnricher(ICommand command) {
                _command = command;
            }

            public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory) {
                logEvent.AddOrUpdateProperty(new LogEventProperty("Context",
                    new ScalarValue($"Command:{_command.Id.ToString()}")));
            }
        }
    }
}