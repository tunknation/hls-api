﻿using System;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.Core.Application.Data;
using HLS.Core.Infrastructure.Serialization;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Processing.InternalCommands {
    public class CommandsScheduler : ICommandsScheduler {
        private readonly ISqlConnectionFactory _sqlConnectionFactory;

        public CommandsScheduler(ISqlConnectionFactory sqlConnectionFactory) {
            _sqlConnectionFactory = sqlConnectionFactory;
        }

        public async Task EnqueueAsync(ICommand command) {
            var connection = _sqlConnectionFactory.GetOpenConnection();

            const string sqlInsert =
                "INSERT INTO [containermanagement].[InternalCommands] ([Id], [EnqueueDate], [Type], [Data]) VALUES " +
                "(@Id, @EnqueueDate, @Type, @Data)";

            await connection.ExecuteAsync(sqlInsert, new {
                command.Id,
                EnqueueDate = DateTime.UtcNow,
                Type = command.GetType().FullName,
                Data = JsonConvert.SerializeObject(command, new JsonSerializerSettings {
                    ContractResolver = new AllPropertiesContractResolver()
                })
            });
        }

        public async Task EnqueueAsync<T>(ICommand<T> command) {
            var connection = _sqlConnectionFactory.GetOpenConnection();

            const string sqlInsert =
                "INSERT INTO [containermanagement].[InternalCommands] ([Id], [EnqueueDate], [Type], [Data]) VALUES " +
                "(@Id, @EnqueueDate, @Type, @Data)";

            await connection.ExecuteAsync(sqlInsert, new {
                command.Id,
                EnqueueDate = DateTime.UtcNow,
                Type = command.GetType().FullName,
                Data = JsonConvert.SerializeObject(command, new JsonSerializerSettings {
                    ContractResolver = new AllPropertiesContractResolver()
                })
            });
        }
    }
}