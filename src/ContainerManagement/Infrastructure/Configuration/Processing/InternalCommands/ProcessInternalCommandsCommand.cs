﻿using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Processing.InternalCommands {
    internal class ProcessInternalCommandsCommand : CommandBase, IRecurringCommand { }
}