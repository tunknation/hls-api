﻿using System.Threading.Tasks;
using HLS.ContainerManagement.Infrastructure.Configuration.Processing;
using HLS.ContainerManagement.Infrastructure.Configuration.Processing.Outbox;
using Quartz;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Quartz.Jobs {
    [DisallowConcurrentExecution]
    public class ProcessOutboxJob : IJob {
        public async Task Execute(IJobExecutionContext context) {
            await CommandsExecutor.Execute(new ProcessOutboxCommand());
        }
    }
}