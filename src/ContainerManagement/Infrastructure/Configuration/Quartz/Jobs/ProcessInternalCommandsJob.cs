﻿using System.Threading.Tasks;
using HLS.ContainerManagement.Infrastructure.Configuration.Processing;
using HLS.ContainerManagement.Infrastructure.Configuration.Processing.InternalCommands;
using Quartz;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Quartz.Jobs {
    [DisallowConcurrentExecution]
    public class ProcessInternalCommandsJob : IJob {
        public async Task Execute(IJobExecutionContext context) {
            await CommandsExecutor.Execute(new ProcessInternalCommandsCommand());
        }
    }
}