﻿using System.Threading.Tasks;
using HLS.ContainerManagement.Application.ContainerLeases.LeaseContainers;
using HLS.ContainerManagement.Infrastructure.Configuration.Processing;
using Quartz;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Quartz.Jobs {
    [DisallowConcurrentExecution]
    public class LeaseContainersJob : IJob {
        public async Task Execute(IJobExecutionContext context) {
            await CommandsExecutor.Execute(new LeaseContainersCommand());
        }
    }
}