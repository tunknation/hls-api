﻿using System.Threading.Tasks;
using HLS.ContainerManagement.Application.ContainerLeases.ExpireContainerLeases;
using HLS.ContainerManagement.Infrastructure.Configuration.Processing;
using Quartz;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Quartz.Jobs {
    [DisallowConcurrentExecution]
    public class ExpireContainerLeasesJob : IJob {
        public async Task Execute(IJobExecutionContext context) {
            await CommandsExecutor.Execute(new ExpireContainerLeasesCommand());
        }
    }
}