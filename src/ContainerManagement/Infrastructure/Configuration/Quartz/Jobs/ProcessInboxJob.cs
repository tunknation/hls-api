﻿using System.Threading.Tasks;
using HLS.ContainerManagement.Infrastructure.Configuration.Processing;
using HLS.ContainerManagement.Infrastructure.Configuration.Processing.Inbox;
using Quartz;

namespace HLS.ContainerManagement.Infrastructure.Configuration.Quartz.Jobs {
    [DisallowConcurrentExecution]
    public class ProcessInboxJob : IJob {
        public async Task Execute(IJobExecutionContext context) {
            await CommandsExecutor.Execute(new ProcessInboxCommand());
        }
    }
}