﻿using Autofac;
using HLS.Core.EventBus;
using HLS.Sales.IntegrationEvents;
using Serilog;

namespace HLS.ContainerManagement.Infrastructure.Configuration.EventsBus {
    public static class EventsBusStartup {
        public static void Initialize(ILogger logger) {
            SubscribeToIntegrationEvents(logger);
        }

        private static void SubscribeToIntegrationEvents(ILogger logger) {
            var eventBus = ContainerManagementCompositionRoot.BeginLifetimeScope().Resolve<IEventsBus>();

            SubscribeToIntegrationEvent<ContainerDeliveryOrderFulfilledIntegrationEvent>(eventBus, logger);
            SubscribeToIntegrationEvent<ContainerRetrievalOrderFulfilledIntegrationEvent>(eventBus, logger);
        }

        private static void SubscribeToIntegrationEvent<T>(IEventsBus eventBus, ILogger logger)
            where T : IntegrationEvent {
            logger.Information("Subscribe to {@IntegrationEvent}", typeof(T).FullName);
            eventBus.Subscribe(
                new IntegrationEventGenericHandler<T>());
        }
    }
}