﻿using System.Threading.Tasks;
using Autofac;
using Dapper;
using HLS.Core.Application.Data;
using HLS.Core.EventBus;
using HLS.Core.Infrastructure.Serialization;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Infrastructure.Configuration.EventsBus {
    internal class IntegrationEventGenericHandler<T> : IIntegrationEventHandler<T> where T : IntegrationEvent {
        public async Task Handle(T @event) {
            using var scope = ContainerManagementCompositionRoot.BeginLifetimeScope();
            using var connection = scope.Resolve<ISqlConnectionFactory>().GetOpenConnection();

            var type = @event.GetType().FullName;
            var data = JsonConvert.SerializeObject(@event, new JsonSerializerSettings {
                ContractResolver = new AllPropertiesContractResolver()
            });

            const string sql = @"INSERT INTO [containermanagement].[InboxMessages] (Id, OccurredOn, Type, Data)
                                 VALUES (@Id, @OccurredOn, @Type, @Data)";

            await connection.ExecuteScalarAsync(sql, new {
                @event.Id,
                @event.OccurredOn,
                type,
                data
            });
        }
    }
}