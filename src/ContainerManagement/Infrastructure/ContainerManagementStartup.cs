﻿using Autofac;
using Dapper;
using HLS.ContainerManagement.Application.ContainerLeases.TypeHandlers;
using HLS.ContainerManagement.Application.ContainerTypes.TypeHandlers;
using HLS.ContainerManagement.Application.Customers.TypeHandlers;
using HLS.ContainerManagement.Application.Projects.TypeHandlers;
using HLS.ContainerManagement.Infrastructure.AggregateStore;
using HLS.ContainerManagement.Infrastructure.Configuration.DataAccess;
using HLS.ContainerManagement.Infrastructure.Configuration.EventsBus;
using HLS.ContainerManagement.Infrastructure.Configuration.Logging;
using HLS.ContainerManagement.Infrastructure.Configuration.Mediation;
using HLS.ContainerManagement.Infrastructure.Configuration.Processing;
using HLS.ContainerManagement.Infrastructure.Configuration.Processing.Outbox;
using HLS.ContainerManagement.Infrastructure.Configuration.Quartz;
using HLS.Core.EventBus;
using Serilog;
using Serilog.Extensions.Logging;

namespace HLS.ContainerManagement.Infrastructure {
    public static class ContainerManagementStartup {
        private static IContainer _container;

        private static SubscriptionsManager _subscriptionsManager;

        public static void Initialize(
            string connectionString,
            ILogger logger,
            IEventsBus eventsBus,
            bool runQuartz = true) {
            var moduleLogger = logger.ForContext("Module", "ContainerManagement");

            ConfigureCompositionRoot(connectionString, moduleLogger, eventsBus, runQuartz);

            if (runQuartz) {
                QuartzStartup.Initialize(moduleLogger);
            }

            EventsBusStartup.Initialize(moduleLogger);
        }

        public static void Stop() {
            _subscriptionsManager.Stop();
            QuartzStartup.StopQuartz();
        }

        private static void ConfigureCompositionRoot(
            string connectionString,
            ILogger logger,
            IEventsBus eventsBus,
            bool runQuartz = true) {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new LoggingModule(logger));

            var loggerFactory = new SerilogLoggerFactory(logger);
            containerBuilder.RegisterModule(new DataAccessModule(connectionString, loggerFactory));

            containerBuilder.RegisterModule(new ProcessingModule());
            containerBuilder.RegisterModule(new EventsBusModule(eventsBus));
            containerBuilder.RegisterModule(new MediatorModule());
            containerBuilder.RegisterModule(new OutboxModule());


            if (runQuartz) {
                containerBuilder.RegisterModule(new QuartzModule());
            }

            _container = containerBuilder.Build();

            ContainerManagementCompositionRoot.SetContainer(_container);

            AddSqlTypeHandlers();

            RunEventsProjectors();
        }

        private static void AddSqlTypeHandlers() {
            SqlMapper.AddTypeHandler(new ContainerLeaseDayHandler());
            SqlMapper.AddTypeHandler(new ContainerLeaseDaysListHandler());
            SqlMapper.AddTypeHandler(new ContainerLeaseDayStatusHandler());
            SqlMapper.AddTypeHandler(new ContainerLeaseIdHandler());
            SqlMapper.AddTypeHandler(new ContainerLeaseMonthHandler());
            SqlMapper.AddTypeHandler(new ContainerLeaseMonthsListHandler());
            SqlMapper.AddTypeHandler(new ContainerLeaseStatusHandler());
            SqlMapper.AddTypeHandler(new ContainerTypeIdHandler());
            SqlMapper.AddTypeHandler(new CustomerIdHandler());
            SqlMapper.AddTypeHandler(new ContainerIdHandler());
            SqlMapper.AddTypeHandler(new ContainerStatusHandler());
            SqlMapper.AddTypeHandler(new ProjectAddressHandler());
            SqlMapper.AddTypeHandler(new ProjectIdHandler());
            SqlMapper.AddTypeHandler(new ProjectStatusHandler());
        }

        private static void RunEventsProjectors() {
            _subscriptionsManager = _container.Resolve<SubscriptionsManager>();

            _subscriptionsManager.Start();
        }
    }
}