﻿using System.Threading.Tasks;
using Autofac;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.Configuration.Queries;
using HLS.ContainerManagement.Application.Contracts;
using HLS.ContainerManagement.Infrastructure.Configuration.Processing;
using MediatR;

namespace HLS.ContainerManagement.Infrastructure {
    public class ContainerManagementModule : IContainerManagementModule {
        public async Task<TResult> ExecuteCommandAsync<TResult>(ICommand<TResult> command) {
            return await CommandsExecutor.Execute(command);
        }

        public async Task ExecuteCommandAsync(ICommand command) {
            await CommandsExecutor.Execute(command);
        }

        public async Task<TResult> ExecuteQueryAsync<TResult>(IQuery<TResult> query) {
            using var scope = ContainerManagementCompositionRoot.BeginLifetimeScope();
            var mediator = scope.Resolve<IMediator>();

            return await mediator.Send(query);
        }
    }
}