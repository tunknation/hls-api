﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Domain.ContainerLeases;
using HLS.ContainerManagement.Domain.Projects;
using HLS.ContainerManagement.Domain.SeedWork;
using MediatR;

namespace HLS.ContainerManagement.Application.ContainerLeases.StartNewContainerLease {
    internal class StartNewContainerLeaseCommandHandler : ICommandHandler<StartNewContainerLeaseCommand> {
        private readonly IAggregateStore _aggregateStore;

        public StartNewContainerLeaseCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public Task<Unit> Handle(StartNewContainerLeaseCommand cmd, CancellationToken cancellationToken) {
            var containerLease = ContainerLease.StartNewContainerLease(
                new ProjectId(cmd.ProjectId),
                new ContainerId(cmd.ContainerId),
                cmd.StartDate);

            _aggregateStore.AppendChanges(containerLease);

            return Task.FromResult(Unit.Value);
        }
    }
}