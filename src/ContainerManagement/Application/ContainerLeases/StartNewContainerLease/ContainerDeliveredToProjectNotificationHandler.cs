﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.Projects.DeliverContainerToProject;
using MediatR;

namespace HLS.ContainerManagement.Application.ContainerLeases.StartNewContainerLease {
    internal class
        ContainerDeliveredToProjectNotificationHandler : INotificationHandler<ContainerDeliveredToProjectNotification> {
        private readonly ICommandsScheduler _commandsScheduler;

        public ContainerDeliveredToProjectNotificationHandler(ICommandsScheduler commandsScheduler) {
            _commandsScheduler = commandsScheduler;
        }

        public async Task Handle(ContainerDeliveredToProjectNotification notification,
            CancellationToken cancellationToken) {
            var cmd = new StartNewContainerLeaseCommand(
                notification.DomainEvent.ProjectId.Value,
                notification.DomainEvent.ContainerId.Value,
                notification.DomainEvent.DeliveryDate);

            await _commandsScheduler.EnqueueAsync(cmd);
        }
    }
}