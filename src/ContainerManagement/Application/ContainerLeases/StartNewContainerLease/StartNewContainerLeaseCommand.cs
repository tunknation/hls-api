﻿using System;
using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Application.ContainerLeases.StartNewContainerLease {
    public class StartNewContainerLeaseCommand : InternalCommandBase {
        public StartNewContainerLeaseCommand(Guid projectId, Guid containerId, DateTime startDate) {
            ProjectId = projectId;
            ContainerId = containerId;
            StartDate = startDate;
        }

        public Guid ProjectId { get; }

        public Guid ContainerId { get; }

        public DateTime StartDate { get; }
    }
}