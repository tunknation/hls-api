﻿using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Domain.ContainerLeases;

namespace HLS.ContainerManagement.Application.ContainerLeases.LeaseContainer {
    public class LeaseContainerCommand : InternalCommandBase {
        public LeaseContainerCommand(ContainerLeaseId containerLeaseId, ContainerLeaseDay day) {
            ContainerLeaseId = containerLeaseId;
            Day = day;
        }

        public ContainerLeaseId ContainerLeaseId { get; }

        public ContainerLeaseDay Day { get; }
    }
}