﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.ContainerLeases.Exceptions;
using HLS.ContainerManagement.Domain.ContainerLeases;
using HLS.ContainerManagement.Domain.SeedWork;
using MediatR;

namespace HLS.ContainerManagement.Application.ContainerLeases.LeaseContainer {
    internal class LeaseContainerCommandHandler : ICommandHandler<LeaseContainerCommand> {
        private readonly IAggregateStore _aggregateStore;

        public LeaseContainerCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(LeaseContainerCommand cmd, CancellationToken cancellationToken) {
            var containerLease = await _aggregateStore.Load(new ContainerLeaseId(cmd.ContainerLeaseId));

            if (containerLease is null) {
                throw new ContainerLeaseNotFoundException();
            }

            containerLease.LeaseContainer(cmd.Day);

            _aggregateStore.AppendChanges(containerLease);

            return Unit.Value;
        }
    }
}