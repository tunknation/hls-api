﻿using System;
using HLS.ContainerManagement.Domain.ContainerLeases.Events;
using HLS.Core.Application.Events;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.ContainerLeases.LeaseContainer {
    public class ContainerLeasedNotification : DomainNotificationBase<ContainerLeasedDomainEvent> {
        [JsonConstructor]
        public ContainerLeasedNotification(ContainerLeasedDomainEvent domainEvent, Guid id)
            : base(domainEvent, id) { }
    }
}