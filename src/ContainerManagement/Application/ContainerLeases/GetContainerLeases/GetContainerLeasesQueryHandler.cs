﻿using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Queries;
using HLS.ContainerManagement.Application.ContainerLeases.Dtos;
using HLS.ContainerManagement.Application.Pagination;
using HLS.Core.Application.Data;

namespace HLS.ContainerManagement.Application.ContainerLeases.GetContainerLeases {
    internal class
        GetContainerLeasesQueryHandler : IQueryHandler<GetContainerLeasesQuery, IEnumerable<ContainerLeaseDto>> {
        private readonly IDbConnection _connection;

        public GetContainerLeasesQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<IEnumerable<ContainerLeaseDto>> Handle(
            GetContainerLeasesQuery query,
            CancellationToken cancellationToken) {
            var parameters = new DynamicParameters();
            var pageData = PagedQueryHelper.GetPageData(query);

            parameters.Add(nameof(PagedQueryHelper.Offset), pageData.Offset);
            parameters.Add(nameof(PagedQueryHelper.Next), pageData.Next);

            var sql = @"
                SELECT [CL].[Id],
                       [CL].[ProjectId],
                       [CL].[ContainerId],
                       [CL].[Status],
                       [CL].[StartDate],
                       [CL].[ExpirationDate],
                       [CL].[CompletionDate],
                       [CL].[Months]
                FROM [containermanagement].[v_ContainerLease] AS [CL]
                ORDER BY [CL].[Id]";

            sql = PagedQueryHelper.AppendPageStatement(sql);

            return await _connection.QueryAsync<ContainerLeaseDto>(sql, parameters);
        }
    }
}