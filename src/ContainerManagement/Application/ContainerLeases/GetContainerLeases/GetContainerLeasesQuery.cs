﻿using System.Collections.Generic;
using HLS.ContainerManagement.Application.Configuration.Queries;
using HLS.ContainerManagement.Application.ContainerLeases.Dtos;
using HLS.ContainerManagement.Application.Pagination;

namespace HLS.ContainerManagement.Application.ContainerLeases.GetContainerLeases {
    public class GetContainerLeasesQuery : QueryBase<IEnumerable<ContainerLeaseDto>>, IPagedQuery {
        public GetContainerLeasesQuery(int? page, int? perPage) {
            Page = page;
            PerPage = perPage;
        }

        public int? Page { get; }

        public int? PerPage { get; }
    }
}