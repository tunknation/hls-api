﻿using System.Data;
using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.ContainerLeases.ExpireContainerLease;
using HLS.Core.Application.Data;
using MediatR;

namespace HLS.ContainerManagement.Application.ContainerLeases.RenewContainerLease {
    internal class ContainerLeaseExpiredNotificationHandler : INotificationHandler<ContainerLeaseExpiredNotification> {
        private readonly ICommandsScheduler _commandsScheduler;

        private readonly IDbConnection _connection;

        public ContainerLeaseExpiredNotificationHandler(
            ICommandsScheduler commandsScheduler,
            ISqlConnectionFactory sqlConnectionFactory) {
            _commandsScheduler = commandsScheduler;
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Handle(
            ContainerLeaseExpiredNotification notification,
            CancellationToken cancellationToken) {
            var cmd = new RenewContainerLeaseCommand(notification.DomainEvent.ContainerLeaseId.Value);

            await _commandsScheduler.EnqueueAsync(cmd);
        }
    }
}