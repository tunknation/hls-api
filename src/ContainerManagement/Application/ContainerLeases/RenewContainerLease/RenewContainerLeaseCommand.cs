﻿using System;
using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Application.ContainerLeases.RenewContainerLease {
    public class RenewContainerLeaseCommand : InternalCommandBase {
        public RenewContainerLeaseCommand(Guid containerLeaseId) {
            ContainerLeaseId = containerLeaseId;
        }

        public Guid ContainerLeaseId { get; }
    }
}