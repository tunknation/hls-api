﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.Projects.RetrieveContainerFromProject;
using HLS.Core.Application.Data;
using MediatR;

namespace HLS.ContainerManagement.Application.ContainerLeases.CompleteContainerLease {
    internal class
        ContainerRetrievedFromProjectNotificationHandler : INotificationHandler<
            ContainerRetrievedFromProjectNotification> {
        private readonly ICommandsScheduler _commandsScheduler;

        private readonly IDbConnection _connection;

        public ContainerRetrievedFromProjectNotificationHandler(
            ICommandsScheduler commandsScheduler,
            ISqlConnectionFactory sqlConnectionFactory) {
            _commandsScheduler = commandsScheduler;
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Handle(
            ContainerRetrievedFromProjectNotification notification,
            CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [CL].[Id]
                FROM [containermanagement].[ContainerLease] AS [CL]
                WHERE [CL].[ContainerId] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", notification.DomainEvent.ContainerId);

            var containerLeaseId = await _connection.QuerySingleOrDefaultAsync<Guid>(sql, parameters);

            if (containerLeaseId == Guid.Empty) {
                return;
            }

            var cmd = new CompleteContainerLeaseCommand(
                containerLeaseId,
                notification.DomainEvent.RetrievementDate);

            await _commandsScheduler.EnqueueAsync(cmd);
        }
    }
}