﻿using System;
using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Application.ContainerLeases.CompleteContainerLease {
    public class CompleteContainerLeaseCommand : InternalCommandBase {
        public CompleteContainerLeaseCommand(Guid containerLeaseId, DateTime completionDate) {
            ContainerLeaseId = containerLeaseId;
            CompletionDate = completionDate;
        }

        public Guid ContainerLeaseId { get; }

        public DateTime CompletionDate { get; }
    }
}