﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.ContainerLeases.Exceptions;
using HLS.ContainerManagement.Domain.ContainerLeases;
using HLS.ContainerManagement.Domain.SeedWork;
using MediatR;

namespace HLS.ContainerManagement.Application.ContainerLeases.CompleteContainerLease {
    public class CompleteContainerLeaseCommandHandler : ICommandHandler<CompleteContainerLeaseCommand> {
        private readonly IAggregateStore _aggregateStore;

        public CompleteContainerLeaseCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(CompleteContainerLeaseCommand cmd, CancellationToken cancellationToken) {
            var containerLease = await _aggregateStore.Load(new ContainerLeaseId(cmd.ContainerLeaseId));

            if (containerLease is null) {
                throw new ContainerLeaseNotFoundException();
            }

            containerLease.CompleteContainerLease(cmd.CompletionDate);

            _aggregateStore.AppendChanges(containerLease);

            return Unit.Value;
        }
    }
}