﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Projections;
using HLS.ContainerManagement.Domain.ContainerLeases.Events;
using HLS.Core.Application.Data;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Application.ContainerLeases.Projections {
    internal class ContainerLeaseProjector : IProjector {
        private readonly IDbConnection _connection;

        public ContainerLeaseProjector(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Project(IDomainEvent @event) {
            await _when((dynamic)@event);
        }

        private static Task _when(IDomainEvent @event) {
            return Task.CompletedTask;
        }

        private async Task _when(NewContainerLeaseStartedDomainEvent @event) {
            const string sql = @"
                INSERT INTO [containermanagement].[ContainerLease]
                    ([Id], [ProjectId], [ContainerId], [Status], [StartDate], [ExpirationDate], [Months])
                VALUES (@Id, @ProjectId, @ContainerId, @Status, @StartDate, @ExpirationDate, @Months);";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ContainerLeaseId);
            parameters.Add("@ProjectId", @event.ProjectId);
            parameters.Add("@ContainerId", @event.ContainerId);
            parameters.Add("@Status", @event.Status);
            parameters.Add("@StartDate", @event.StartDate);
            parameters.Add("@ExpirationDate", @event.ExpirationDate);
            parameters.Add("@Months", @event.Months);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerLeasedDomainEvent @event) {
            const string sql = @"
                WITH [CTE] AS (
                    SELECT [KEY]
                    FROM [containermanagement].[ContainerLease]
                    CROSS APPLY OPENJSON([Months]) [M]
                    WHERE JSON_VALUE([M].[value], '$.StartDate') = CAST(JSON_VALUE(@Month, '$.StartDate') AS DATETIME2)
                )
                UPDATE [containermanagement].[ContainerLease]
                SET [Months] = JSON_MODIFY([Months], '$[' + [CTE].[key] + ']', JSON_QUERY(@Month, '$'))
                FROM [CTE]
                WHERE [Id] = @Id;";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ContainerLeaseId);
            parameters.Add("@Month", @event.Month);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerLeaseExtendedForFullMonthDomainEvent @event) {
            const string sql = @"
                WITH [CTE] AS (
                    SELECT [KEY]
                    FROM [containermanagement].[ContainerLease]
                    CROSS APPLY OPENJSON([Months]) [M]
                    WHERE JSON_VALUE([M].[value], '$.StartDate') = CAST(JSON_VALUE(@Month, '$.StartDate') AS DATETIME2)
                )
                UPDATE [containermanagement].[ContainerLease]
                SET [Months] = JSON_MODIFY([Months], '$[' + [CTE].[key] + ']', JSON_QUERY(@Month, '$'))
                FROM [CTE]
                WHERE [Id] = @Id;";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ContainerLeaseId);
            parameters.Add("@Month", @event.Month);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerLeaseExpiredDomainEvent @event) {
            const string sql = @"
                UPDATE [containermanagement].[ContainerLease]
                SET [Status] = @Status
                WHERE [Id] = @Id;";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ContainerLeaseId);
            parameters.Add("@Status", @event.Status);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerLeaseRenewedDomainEvent @event) {
            const string sql = @"
                WITH [CTE] AS (
                    SELECT [M].[StartDate],
                           [M].[EndDate],
                           [M].[Days]
                    FROM [containermanagement].[ContainerLease]
                    CROSS APPLY OPENJSON([Months])
                    WITH (
                        StartDate DATETIME2 '$.StartDate',
                        EndDate DATETIME2 '$.EndDate',
                        Days NVARCHAR(MAX) '$.Days' AS JSON
                    ) AS [M]
                    UNION ALL
                    SElect *
                    FROM OPENJSON(@Months)
                    WITH (
                        StartDate DATETIME2 '$.StartDate',
                        EndDate DATETIME2 '$.EndDate',
                        Days NVARCHAR(MAX) '$.Days' AS JSON
                    )
                )
                UPDATE [containermanagement].[ContainerLease]
                SET [Status] = @Status,
                    [Months] = (
                        SELECT [StartDate],
                               [EndDate],
                               JSON_QUERY([Days], '$') AS Days
                        FROM [CTE]
                        FOR JSON PATH, INCLUDE_NULL_VALUES
                    )
                WHERE [Id] = @Id;";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ContainerLeaseId);
            parameters.Add("@Status", @event.Status);
            parameters.Add("@Months", @event.Months);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerLeaseCompletedDomainEvent @event) {
            const string sql = @"
                UPDATE [containermanagement].[ContainerLease]
                SET [Status] = @Status,
                    [ExpirationDate] = @ExpirationDate,
                    [CompletionDate] = @CompletionDate,
                    [Months] = @Months
                WHERE [Id] = @Id;";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ContainerLeaseId);
            parameters.Add("@Status", @event.Status);
            parameters.Add("@ExpirationDate", @event.ExpirationDate);
            parameters.Add("@CompletionDate", @event.CompletionDate);
            parameters.Add("@Months", @event.Months);

            await _connection.ExecuteAsync(sql, parameters);
        }
    }
}