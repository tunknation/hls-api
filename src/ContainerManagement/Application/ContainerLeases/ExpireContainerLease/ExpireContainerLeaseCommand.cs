﻿using System;
using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Application.ContainerLeases.ExpireContainerLease {
    public class ExpireContainerLeaseCommand : InternalCommandBase {
        public ExpireContainerLeaseCommand(Guid containerLeaseId) {
            ContainerLeaseId = containerLeaseId;
        }

        public Guid ContainerLeaseId { get; }
    }
}