﻿using System;
using HLS.ContainerManagement.Domain.ContainerLeases.Events;
using HLS.Core.Application.Events;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.ContainerLeases.ExpireContainerLease {
    public class ContainerLeaseExpiredNotification : DomainNotificationBase<ContainerLeaseExpiredDomainEvent> {
        [JsonConstructor]
        public ContainerLeaseExpiredNotification(ContainerLeaseExpiredDomainEvent domainEvent, Guid id)
            : base(domainEvent, id) { }
    }
}