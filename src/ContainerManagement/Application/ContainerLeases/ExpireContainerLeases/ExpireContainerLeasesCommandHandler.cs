﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.ContainerLeases.ExpireContainerLease;
using HLS.ContainerManagement.Domain.ContainerLeases;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Application.Data;
using MediatR;

namespace HLS.ContainerManagement.Application.ContainerLeases.ExpireContainerLeases {
    internal class ExpireContainerLeasesCommandHandler : ICommandHandler<ExpireContainerLeasesCommand> {
        private readonly ICommandsScheduler _commandsScheduler;

        private readonly IDbConnection _connection;

        public ExpireContainerLeasesCommandHandler(
            ISqlConnectionFactory sqlConnectionFactory,
            ICommandsScheduler commandsScheduler) {
            _connection = sqlConnectionFactory.GetOpenConnection();
            _commandsScheduler = commandsScheduler;
        }

        public async Task<Unit> Handle(ExpireContainerLeasesCommand cmd, CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [CL].[Id]
                FROM [containermanagement].[ContainerLease] AS [CL]
                WHERE [CL].[Status] = @Status AND
                      [CL].[ExpirationDate] < @Date";

            var parameters = new DynamicParameters();
            parameters.Add("@Status", ContainerLeaseStatus.Active);
            parameters.Add("@Date", SystemClock.Today);

            var containerLeaseIds = await _connection.QueryAsync<Guid>(sql, parameters);

            foreach (var containerLeaseId in containerLeaseIds) {
                await _commandsScheduler.EnqueueAsync(
                    new ExpireContainerLeaseCommand(containerLeaseId));
            }

            return Unit.Value;
        }
    }
}