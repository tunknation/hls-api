﻿using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Application.ContainerLeases.ExpireContainerLeases {
    public class ExpireContainerLeasesCommand : CommandBase, IRecurringCommand { }
}