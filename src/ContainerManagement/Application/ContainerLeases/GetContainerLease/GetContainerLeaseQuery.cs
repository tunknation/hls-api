﻿using System;
using HLS.ContainerManagement.Application.Configuration.Queries;
using HLS.ContainerManagement.Application.ContainerLeases.Dtos;

namespace HLS.ContainerManagement.Application.ContainerLeases.GetContainerLease {
    public class GetContainerLeaseQuery : QueryBase<ContainerLeaseDto> {
        public GetContainerLeaseQuery(Guid containerLeaseId) {
            ContainerLeaseId = containerLeaseId;
        }

        public Guid ContainerLeaseId { get; }
    }
}