﻿using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Queries;
using HLS.ContainerManagement.Application.ContainerLeases.Dtos;
using HLS.Core.Application.Data;

namespace HLS.ContainerManagement.Application.ContainerLeases.GetContainerLease {
    internal class GetContainerLeaseQueryHandler : IQueryHandler<GetContainerLeaseQuery, ContainerLeaseDto> {
        private readonly IDbConnection _connection;

        public GetContainerLeaseQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<ContainerLeaseDto> Handle(
            GetContainerLeaseQuery query,
            CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [CL].[Id],
                       [CL].[ProjectId],
                       [CL].[ContainerId],
                       [CL].[Status],
                       [CL].[StartDate],
                       [CL].[ExpirationDate],
                       [CL].[CompletionDate],
                       [CL].[Months]
                FROM [containermanagement].[v_ContainerLease] AS [CL]
                WHERE [CL].[Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", query.ContainerLeaseId);

            return await _connection.QuerySingleOrDefaultAsync<ContainerLeaseDto>(sql, parameters);
        }
    }
}