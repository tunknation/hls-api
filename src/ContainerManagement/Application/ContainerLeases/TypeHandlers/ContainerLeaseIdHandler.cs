﻿using System;
using System.Data;
using Dapper;
using HLS.ContainerManagement.Domain.ContainerLeases;

namespace HLS.ContainerManagement.Application.ContainerLeases.TypeHandlers {
    public class ContainerLeaseIdHandler : SqlMapper.TypeHandler<ContainerLeaseId> {
        public override void SetValue(IDbDataParameter parameter, ContainerLeaseId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override ContainerLeaseId Parse(object value) {
            return new ContainerLeaseId(Guid.Parse(value.ToString()!));
        }
    }
}