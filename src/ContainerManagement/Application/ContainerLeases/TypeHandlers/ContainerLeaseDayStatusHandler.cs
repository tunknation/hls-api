﻿using System.Data;
using Dapper;
using HLS.ContainerManagement.Domain.ContainerLeases;

namespace HLS.ContainerManagement.Application.ContainerLeases.TypeHandlers {
    public class ContainerLeaseDayStatusHandler : SqlMapper.TypeHandler<ContainerLeaseDayStatus> {
        public override void SetValue(IDbDataParameter parameter, ContainerLeaseDayStatus status) {
            parameter.Value = status.Code;
            parameter.DbType = DbType.String;
        }

        public override ContainerLeaseDayStatus Parse(object value) {
            return ContainerLeaseDayStatus.Of(value.ToString());
        }
    }
}