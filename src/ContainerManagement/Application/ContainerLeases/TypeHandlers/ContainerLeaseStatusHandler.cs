﻿using System.Data;
using Dapper;
using HLS.ContainerManagement.Domain.ContainerLeases;

namespace HLS.ContainerManagement.Application.ContainerLeases.TypeHandlers {
    public class ContainerLeaseStatusHandler : SqlMapper.TypeHandler<ContainerLeaseStatus> {
        public override void SetValue(IDbDataParameter parameter, ContainerLeaseStatus status) {
            parameter.Value = status.Code;
            parameter.DbType = DbType.String;
        }

        public override ContainerLeaseStatus Parse(object value) {
            return ContainerLeaseStatus.Of(value.ToString());
        }
    }
}