﻿using System.Data;
using Dapper;
using HLS.ContainerManagement.Domain.ContainerLeases;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.ContainerLeases.TypeHandlers {
    public class ContainerLeaseDayHandler : SqlMapper.TypeHandler<ContainerLeaseDay> {
        public override void SetValue(IDbDataParameter parameter, ContainerLeaseDay day) {
            parameter.Value = JsonConvert.SerializeObject(day);
            parameter.DbType = DbType.String;
            parameter.Size = int.MaxValue;
        }

        public override ContainerLeaseDay Parse(object value) {
            return JsonConvert.DeserializeObject<ContainerLeaseDay>(value.ToString()!);
        }
    }
}