﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using HLS.ContainerManagement.Domain.ContainerLeases;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.ContainerLeases.TypeHandlers {
    public class ContainerLeaseDaysListHandler : SqlMapper.TypeHandler<List<ContainerLeaseDay>> {
        public override void SetValue(IDbDataParameter parameter, List<ContainerLeaseDay> daysList) {
            parameter.Value = JsonConvert.SerializeObject(daysList);
            parameter.DbType = DbType.String;
            parameter.Size = int.MaxValue;
        }

        public override List<ContainerLeaseDay> Parse(object value) {
            return JsonConvert.DeserializeObject<List<ContainerLeaseDay>>(value.ToString()!);
        }
    }
}