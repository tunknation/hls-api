﻿using System.Data;
using Dapper;
using HLS.ContainerManagement.Domain.ContainerLeases;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.ContainerLeases.TypeHandlers {
    public class ContainerLeaseMonthHandler : SqlMapper.TypeHandler<ContainerLeaseMonth> {
        public override void SetValue(IDbDataParameter parameter, ContainerLeaseMonth month) {
            parameter.Value = JsonConvert.SerializeObject(month);
            parameter.DbType = DbType.String;
            parameter.Size = int.MaxValue;
        }

        public override ContainerLeaseMonth Parse(object value) {
            return JsonConvert.DeserializeObject<ContainerLeaseMonth>(value.ToString()!);
        }
    }
}