﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using HLS.ContainerManagement.Domain.ContainerLeases;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.ContainerLeases.TypeHandlers {
    public class ContainerLeaseMonthsListHandler : SqlMapper.TypeHandler<List<ContainerLeaseMonth>> {
        public override void SetValue(IDbDataParameter parameter, List<ContainerLeaseMonth> daysList) {
            parameter.Value = JsonConvert.SerializeObject(daysList);
            parameter.DbType = DbType.String;
            parameter.Size = int.MaxValue;
        }

        public override List<ContainerLeaseMonth> Parse(object value) {
            return JsonConvert.DeserializeObject<List<ContainerLeaseMonth>>(value.ToString()!);
        }
    }
}