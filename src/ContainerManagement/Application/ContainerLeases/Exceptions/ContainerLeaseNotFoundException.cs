﻿using System;

namespace HLS.ContainerManagement.Application.ContainerLeases.Exceptions {
    public class ContainerLeaseNotFoundException : Exception {
        private const string ErrorMessage = "Container lease not found";

        public ContainerLeaseNotFoundException() : base(ErrorMessage) { }
    }
}