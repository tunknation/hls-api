﻿using System;
using System.Collections.Generic;
using HLS.ContainerManagement.Domain.ContainerLeases;
using HLS.ContainerManagement.Domain.Projects;

namespace HLS.ContainerManagement.Application.ContainerLeases.Dtos {
    public class ContainerLeaseDto {
        public ContainerLeaseId Id { get; set; }

        public ProjectId ProjectId { get; set; }

        public ContainerId ContainerId { get; set; }

        public ContainerLeaseStatus Status { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime ExpirationDate { get; set; }

        public DateTime? CompletionDate { get; set; }

        public List<ContainerLeaseMonth> Months { get; set; }
    }
}