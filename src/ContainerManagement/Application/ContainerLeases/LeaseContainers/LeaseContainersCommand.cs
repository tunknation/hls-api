﻿using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Application.ContainerLeases.LeaseContainers {
    public class LeaseContainersCommand : CommandBase, IRecurringCommand { }
}