﻿using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.ContainerLeases.LeaseContainer;
using HLS.ContainerManagement.Domain.ContainerLeases;
using HLS.ContainerManagement.Domain.SharedKernel;
using HLS.Core.Application.Data;
using MediatR;

namespace HLS.ContainerManagement.Application.ContainerLeases.LeaseContainers {
    internal class LeaseContainersCommandHandler : ICommandHandler<LeaseContainersCommand> {
        private readonly ICommandsScheduler _commandsScheduler;

        private readonly IDbConnection _connection;

        public LeaseContainersCommandHandler(
            ISqlConnectionFactory sqlConnectionFactory,
            ICommandsScheduler commandsScheduler) {
            _connection = sqlConnectionFactory.GetOpenConnection();
            _commandsScheduler = commandsScheduler;
        }

        public async Task<Unit> Handle(LeaseContainersCommand cmd, CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [CL].[Id],
                       MIN([D].[value]) AS Day
                FROM [containermanagement].[ContainerLease] AS [CL]
                CROSS APPLY OPENJSON([Months])
                WITH (
                    Days NVARCHAR(MAX) '$.Days' AS JSON
                ) AS [M]
                CROSS APPLY OPENJSON([M].[Days]) AS [D]
                WHERE [Status] = @Active AND
                      JSON_VALUE([D].[value], '$.Date') <= @Today AND
                      JSON_VALUE([D].[value], '$.Status.Code') = @NotLeased
                GROUP BY [CL].[Id]";

            var parameters = new DynamicParameters();
            parameters.Add("@Active", ContainerLeaseStatus.Active);
            parameters.Add("@Today", SystemClock.Today);
            parameters.Add("@NotLeased", ContainerLeaseDayStatus.NotLeased);

            var dtos =
                await _connection.QueryAsync<ResponseDto>(sql, parameters);

            foreach (var dto in dtos) {
                await _commandsScheduler.EnqueueAsync(
                    new LeaseContainerCommand(dto.Id, dto.Day));
            }

            return Unit.Value;
        }
    }

    internal class ResponseDto {
        public ContainerLeaseId Id { get; set; }

        public ContainerLeaseDay Day { get; set; }
    }
}