﻿using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.ContainerLeases.LeaseContainer;
using HLS.ContainerManagement.Domain.ContainerLeases;
using HLS.Core.Application.Data;
using MediatR;

namespace HLS.ContainerManagement.Application.ContainerLeases.ExtendContainerLeaseForFullMonth {
    internal class ContainerLeasedNotificationHandler : INotificationHandler<ContainerLeasedNotification> {
        private readonly ICommandsScheduler _commandsScheduler;

        private readonly IDbConnection _connection;

        public ContainerLeasedNotificationHandler(
            ICommandsScheduler commandsScheduler,
            ISqlConnectionFactory sqlConnectionFactory) {
            _commandsScheduler = commandsScheduler;
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Handle(ContainerLeasedNotification notification, CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [CL].[Id],
                       [M].[value] AS Month
                FROM [containermanagement].[ContainerLease] AS [CL]
                CROSS APPLY OPENJSON([Months]) AS [M]
                CROSS APPLY OPENJSON(JSON_QUERY([M].[value], '$.Days')) AS [D]
                WHERE [Cl].[Id] = @Id
                GROUP BY [Cl].[Id],
                         [M].[value]
                HAVING COUNT(CASE WHEN JSON_VALUE([D].[value], '$.Status.Code') = @Leased THEN 1 END) = @MaxIndividuallyLeasedDays";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", notification.DomainEvent.ContainerLeaseId);
            parameters.Add("@Leased", ContainerLeaseDayStatus.Leased);
            parameters.Add("@MaxIndividuallyLeasedDays", 7);

            var dto = await _connection.QuerySingleOrDefaultAsync<ResponseDto>(sql, parameters);

            if (dto is null) {
                return;
            }

            var cmd = new ExtendContainerLeaseForFullMonthCommand(dto.Id, dto.Month);
            await _commandsScheduler.EnqueueAsync(cmd);
        }
    }

    internal class ResponseDto {
        public ContainerLeaseId Id { get; set; }

        public ContainerLeaseMonth Month { get; set; }
    }
}