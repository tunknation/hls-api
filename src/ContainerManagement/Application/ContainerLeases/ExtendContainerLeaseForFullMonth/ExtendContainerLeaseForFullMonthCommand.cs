﻿using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Domain.ContainerLeases;

namespace HLS.ContainerManagement.Application.ContainerLeases.ExtendContainerLeaseForFullMonth {
    public class ExtendContainerLeaseForFullMonthCommand : InternalCommandBase {
        public ExtendContainerLeaseForFullMonthCommand(ContainerLeaseId containerLeaseId, ContainerLeaseMonth month) {
            ContainerLeaseId = containerLeaseId;
            Month = month;
        }

        public ContainerLeaseId ContainerLeaseId { get; }

        public ContainerLeaseMonth Month { get; }
    }
}