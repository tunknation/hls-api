﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.ContainerLeases.Exceptions;
using HLS.ContainerManagement.Domain.SeedWork;
using MediatR;

namespace HLS.ContainerManagement.Application.ContainerLeases.ExtendContainerLeaseForFullMonth {
    internal class
        ExtendContainerLeaseForFullMonthCommandHandler : ICommandHandler<ExtendContainerLeaseForFullMonthCommand> {
        private readonly IAggregateStore _aggregateStore;

        public ExtendContainerLeaseForFullMonthCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(ExtendContainerLeaseForFullMonthCommand cmd,
            CancellationToken cancellationToken) {
            var containerLease = await _aggregateStore.Load(cmd.ContainerLeaseId);

            if (containerLease is null) {
                throw new ContainerLeaseNotFoundException();
            }

            containerLease.ExtendContainerLeaseForFullMonth(cmd.Month);

            _aggregateStore.AppendChanges(containerLease);

            return Unit.Value;
        }
    }
}