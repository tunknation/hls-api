﻿using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.Configuration.Queries;

namespace HLS.ContainerManagement.Application.Contracts {
    public interface IContainerManagementModule {
        Task<TResult> ExecuteCommandAsync<TResult>(ICommand<TResult> command);

        Task ExecuteCommandAsync(ICommand command);

        Task<TResult> ExecuteQueryAsync<TResult>(IQuery<TResult> query);
    }
}