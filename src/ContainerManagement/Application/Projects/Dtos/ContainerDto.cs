﻿using System;
using HLS.ContainerManagement.Domain.ContainerTypes;
using HLS.ContainerManagement.Domain.Projects;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.Projects.Dtos {
    public class ContainerDto {
        [JsonConstructor]
        public ContainerDto(
            Guid id,
            string status,
            Guid containerTypeId,
            string descriptiveName,
            DateTime deliveryDate,
            DateTime? retrievementDate) {
            Id = new ContainerId(id);
            Status = ContainerStatus.Of(status);
            ContainerTypeId = new ContainerTypeId(containerTypeId);
            DescriptiveName = descriptiveName;
            DeliveryDate = deliveryDate;
            RetrievementDate = retrievementDate;
        }

        public ContainerId Id { get; }

        public ContainerStatus Status { get; }

        public ContainerTypeId ContainerTypeId { get; }

        public string DescriptiveName { get; }

        public DateTime DeliveryDate { get; }

        public DateTime? RetrievementDate { get; }
    }
}