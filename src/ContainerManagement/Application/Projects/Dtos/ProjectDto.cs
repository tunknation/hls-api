﻿using System;
using System.Collections.Generic;
using HLS.ContainerManagement.Domain.Customers;
using HLS.ContainerManagement.Domain.Projects;

namespace HLS.ContainerManagement.Application.Projects.Dtos {
    public class ProjectDto {
        public ProjectId Id { get; set; }

        public ProjectStatus Status { get; set; }

        public CustomerId CustomerId { get; set; }

        public ProjectAddress Address { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? CompletionDate { get; set; }

        public List<ContainerDto> Containers { get; set; }
    }
}