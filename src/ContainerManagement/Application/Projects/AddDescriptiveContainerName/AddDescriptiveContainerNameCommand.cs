﻿using System;
using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Application.Projects.AddDescriptiveContainerName {
    public class AddDescriptiveContainerNameCommand : CommandBase {
        public AddDescriptiveContainerNameCommand(
            Guid projectId,
            Guid containerId,
            string descriptiveName) {
            ProjectId = projectId;
            ContainerId = containerId;
            DescriptiveName = descriptiveName;
        }

        public Guid ProjectId { get; }

        public Guid ContainerId { get; }

        public string DescriptiveName { get; }
    }
}