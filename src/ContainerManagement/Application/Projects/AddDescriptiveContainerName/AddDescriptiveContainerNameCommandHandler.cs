﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.Projects.Exceptions;
using HLS.ContainerManagement.Domain.Projects;
using HLS.ContainerManagement.Domain.SeedWork;
using MediatR;

namespace HLS.ContainerManagement.Application.Projects.AddDescriptiveContainerName {
    public class AddDescriptiveContainerNameCommandHandler : ICommandHandler<AddDescriptiveContainerNameCommand> {
        private readonly IAggregateStore _aggregateStore;

        public AddDescriptiveContainerNameCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(AddDescriptiveContainerNameCommand cmd, CancellationToken cancellationToken) {
            var project = await _aggregateStore.Load(new ProjectId(cmd.ProjectId));

            if (project is null) {
                throw new ProjectNotFoundException();
            }

            project.AddDescriptiveContainerName(new ContainerId(cmd.ContainerId), cmd.DescriptiveName);

            _aggregateStore.AppendChanges(project);

            return Unit.Value;
        }
    }
}