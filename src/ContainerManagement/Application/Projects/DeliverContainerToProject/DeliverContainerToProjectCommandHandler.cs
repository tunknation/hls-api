﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.Projects.Exceptions;
using HLS.ContainerManagement.Domain.ContainerTypes;
using HLS.ContainerManagement.Domain.Projects;
using HLS.ContainerManagement.Domain.SeedWork;
using MediatR;

namespace HLS.ContainerManagement.Application.Projects.DeliverContainerToProject {
    internal class DeliverContainerToProjectCommandHandler : ICommandHandler<DeliverContainerToProjectCommand> {
        private readonly IAggregateStore _aggregateStore;

        public DeliverContainerToProjectCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(DeliverContainerToProjectCommand cmd, CancellationToken cancellationToken) {
            var project = await _aggregateStore.Load(new ProjectId(cmd.ProjectId));

            if (project is null) {
                throw new ProjectNotFoundException();
            }

            project.DeliverContainerToProject(new ContainerTypeId(cmd.ContainerTypeId), cmd.DeliveryDate);

            _aggregateStore.AppendChanges(project);

            return Unit.Value;
        }
    }
}