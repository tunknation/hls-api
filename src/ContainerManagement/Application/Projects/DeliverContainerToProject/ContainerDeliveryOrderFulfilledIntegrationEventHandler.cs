﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Domain.Projects;
using HLS.Core.Application.Data;
using HLS.Sales.IntegrationEvents;
using MediatR;

namespace HLS.ContainerManagement.Application.Projects.DeliverContainerToProject {
    internal class
        ContainerDeliveryOrderFulfilledIntegrationEventHandler : INotificationHandler<
            ContainerDeliveryOrderFulfilledIntegrationEvent> {
        private readonly IDbConnection _connection;

        private readonly ICommandsScheduler _commandsScheduler;

        public ContainerDeliveryOrderFulfilledIntegrationEventHandler(
            ISqlConnectionFactory sqlConnectionFactory,
            ICommandsScheduler commandsScheduler) {
            _connection = sqlConnectionFactory.GetOpenConnection();
            _commandsScheduler = commandsScheduler;
        }

        public async Task Handle(
            ContainerDeliveryOrderFulfilledIntegrationEvent @event,
            CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [P].[Id]
                FROM [containermanagement].[Project] AS [P]
                WHERE [P].[Status] = @Status AND
                      JSON_VALUE([P].[Address], '$.City') = @City AND
                      JSON_VALUE([P].[Address], '$.PostalCode') = @PostalCode AND
                      JSON_VALUE([P].[Address], '$.StreetAddress') = @StreetAddress";

            var parameters = new DynamicParameters();
            parameters.Add("@Status", ProjectStatus.Active);
            parameters.Add("@City", @event.City);
            parameters.Add("@PostalCode", @event.PostalCode);
            parameters.Add("@StreetAddress", @event.StreetAddress);

            var projectId = await _connection.QuerySingleOrDefaultAsync<Guid>(sql, parameters);

            if (projectId == Guid.Empty) {
                return;
            }

            var cmd = new DeliverContainerToProjectCommand(
                projectId,
                @event.ContainerTypeId,
                @event.FulfillmentDate);

            await _commandsScheduler.EnqueueAsync(cmd);
        }
    }
}