﻿using System;
using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Application.Projects.DeliverContainerToProject {
    public class DeliverContainerToProjectCommand : InternalCommandBase {
        public DeliverContainerToProjectCommand(Guid projectId, Guid containerTypeId, DateTime? deliveryDate) {
            ProjectId = projectId;
            ContainerTypeId = containerTypeId;
            DeliveryDate = deliveryDate;
        }

        public Guid ProjectId { get; }

        public Guid ContainerTypeId { get; }

        public DateTime? DeliveryDate { get; }
    }
}