﻿using System;
using HLS.ContainerManagement.Domain.Projects.Events;
using HLS.Core.Application.Events;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.Projects.DeliverContainerToProject {
    public class
        ContainerDeliveredToProjectNotification : DomainNotificationBase<ContainerDeliveredToProjectDomainEvent> {
        [JsonConstructor]
        public ContainerDeliveredToProjectNotification(ContainerDeliveredToProjectDomainEvent domainEvent, Guid id)
            : base(domainEvent, id) { }
    }
}