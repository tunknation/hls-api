﻿using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Queries;
using HLS.ContainerManagement.Application.Pagination;
using HLS.ContainerManagement.Application.Projects.Dtos;
using HLS.Core.Application.Data;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.Projects.GetProjects {
    internal class GetProjectsQueryHandler : IQueryHandler<GetProjectsQuery, IEnumerable<ProjectDto>> {
        private readonly IDbConnection _connection;

        public GetProjectsQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<IEnumerable<ProjectDto>> Handle(GetProjectsQuery query, CancellationToken cancellationToken) {
            var parameters = new DynamicParameters();
            var pageData = PagedQueryHelper.GetPageData(query);

            parameters.Add(nameof(PagedQueryHelper.Offset), pageData.Offset);
            parameters.Add(nameof(PagedQueryHelper.Next), pageData.Next);

            var sql = @"
                SELECT [P].[Id],
                       [P].[Status],
                       [P].[CustomerId],
                       [P].[Address],
                       [P].[StartDate],
                       [P].[CompletionDate],
                       [P].[Containers]
                FROM [containermanagement].[v_Project] AS [P]
                ORDER BY [P].[Id]";

            sql = PagedQueryHelper.AppendPageStatement(sql);

            return await _connection.QueryAsync<ProjectDto, string, ProjectDto>(sql,
                (project, containers) => {
                    project.Containers = JsonConvert.DeserializeObject<List<ContainerDto>>(containers);

                    return project;
                },
                parameters,
                splitOn: "Id,Containers");
        }
    }
}