﻿using System.Collections.Generic;
using HLS.ContainerManagement.Application.Configuration.Queries;
using HLS.ContainerManagement.Application.Pagination;
using HLS.ContainerManagement.Application.Projects.Dtos;

namespace HLS.ContainerManagement.Application.Projects.GetProjects {
    public class GetProjectsQuery : QueryBase<IEnumerable<ProjectDto>>, IPagedQuery {
        public GetProjectsQuery(int? page, int? perPage) {
            Page = page;
            PerPage = perPage;
        }

        public int? Page { get; }

        public int? PerPage { get; }
    }
}