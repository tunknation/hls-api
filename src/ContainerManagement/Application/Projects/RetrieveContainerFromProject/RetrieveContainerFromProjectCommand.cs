﻿using System;
using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Application.Projects.RetrieveContainerFromProject {
    public class RetrieveContainerFromProjectCommand : CommandBase {
        public RetrieveContainerFromProjectCommand(Guid projectId, Guid containerId, DateTime? retrievementDate) {
            ProjectId = projectId;
            ContainerId = containerId;
            RetrievementDate = retrievementDate;
        }

        public Guid ProjectId { get; }

        public Guid ContainerId { get; }

        public DateTime? RetrievementDate { get; }
    }
}