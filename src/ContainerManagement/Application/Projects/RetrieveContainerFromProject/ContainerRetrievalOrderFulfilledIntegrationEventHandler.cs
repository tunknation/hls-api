﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.Sales.IntegrationEvents;
using MediatR;

namespace HLS.ContainerManagement.Application.Projects.RetrieveContainerFromProject {
    internal class
        ContainerRetrievalOrderFulfilledIntegrationEventHandler : INotificationHandler<
            ContainerRetrievalOrderFulfilledIntegrationEvent> {
        private readonly ICommandsScheduler _commandsScheduler;

        public ContainerRetrievalOrderFulfilledIntegrationEventHandler(ICommandsScheduler commandsScheduler) {
            _commandsScheduler = commandsScheduler;
        }


        public async Task Handle(
            ContainerRetrievalOrderFulfilledIntegrationEvent @event,
            CancellationToken cancellationToken) {
            var cmd = new RetrieveContainerFromProjectCommand(@event.ProjectId, @event.ContainerId,
                @event.FulfillmentDate);

            await _commandsScheduler.EnqueueAsync(cmd);
        }
    }
}