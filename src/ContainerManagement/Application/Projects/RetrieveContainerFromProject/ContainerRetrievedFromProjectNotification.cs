﻿using System;
using HLS.ContainerManagement.Domain.Projects.Events;
using HLS.Core.Application.Events;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.Projects.RetrieveContainerFromProject {
    public class
        ContainerRetrievedFromProjectNotification : DomainNotificationBase<ContainerRetrievedFromProjectDomainEvent> {
        [JsonConstructor]
        public ContainerRetrievedFromProjectNotification(ContainerRetrievedFromProjectDomainEvent domainEvent, Guid id)
            : base(domainEvent, id) { }
    }
}