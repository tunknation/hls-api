﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Application.Projects.Exceptions;
using HLS.ContainerManagement.Domain.Projects;
using HLS.ContainerManagement.Domain.SeedWork;
using MediatR;

namespace HLS.ContainerManagement.Application.Projects.RetrieveContainerFromProject {
    internal class RetrieveContainerFromProjectCommandHandler : ICommandHandler<RetrieveContainerFromProjectCommand> {
        private readonly IAggregateStore _aggregateStore;

        public RetrieveContainerFromProjectCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(RetrieveContainerFromProjectCommand cmd, CancellationToken cancellationToken) {
            var project = await _aggregateStore.Load(new ProjectId(cmd.ProjectId));

            if (project is null) {
                throw new ProjectNotFoundException();
            }

            project.RetrieveContainerFromProject(new ContainerId(cmd.ContainerId), cmd.RetrievementDate);

            _aggregateStore.AppendChanges(project);

            return Unit.Value;
        }
    }
}