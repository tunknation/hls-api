﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Projections;
using HLS.ContainerManagement.Domain.Projects.Events;
using HLS.Core.Application.Data;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Application.Projects.Projections {
    internal class ProjectProjector : IProjector {
        private readonly IDbConnection _connection;

        public ProjectProjector(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Project(IDomainEvent @event) {
            await _when((dynamic)@event);
        }

        private static Task _when(IDomainEvent @event) {
            return Task.CompletedTask;
        }

        private async Task _when(NewProjectStartedDomainEvent @event) {
            const string sql = @"                
                INSERT INTO [containermanagement].[Project] 
                    ([Id], [Status], [CustomerId], [Address], [StartDate])
                VALUES (@Id, @Status, @CustomerId, @Address, @StartDate)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ProjectId);
            parameters.Add("@Status", @event.Status);
            parameters.Add("@CustomerId", @event.CustomerId);
            parameters.Add("@Address", @event.Address);
            parameters.Add("@StartDate", @event.StartDate);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerDeliveredToProjectDomainEvent @event) {
            const string sql = @"
                INSERT INTO [containermanagement].[Container]
                    ([Id], [Status], [ProjectId], [ContainerTypeId], [DeliveryDate])
                VALUES (@Id, @Status, @ProjectId, @ContainerTypeId, @DeliveryDate)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ContainerId);
            parameters.Add("@Status", @event.ContainerStatus);
            parameters.Add("@ProjectId", @event.ProjectId);
            parameters.Add("@ContainerTypeId", @event.ContainerTypeId);
            parameters.Add("@DeliveryDate", @event.DeliveryDate);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(DescriptiveContainerNameAddedDomainEvent @event) {
            const string sql = @"
                UPDATE [containermanagement].[Container]
                SET [DescriptiveName] = @DescriptiveContainerName
                WHERE [Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ContainerId);
            parameters.Add("@DescriptiveContainerName", @event.DescriptiveContainerName);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ContainerRetrievedFromProjectDomainEvent @event) {
            const string sql = @"
                UPDATE [containermanagement].[Container]
                SET [Status] = @Status,
                    [RetrievementDate] = @RetrievementDate
                WHERE [Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ContainerId);
            parameters.Add("@Status", @event.ContainerStatus);
            parameters.Add("@RetrievementDate", @event.RetrievementDate);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(ProjectCompletedDomainEvent @event) {
            const string sql = @"
                UPDATE [containermanagement].[Project]
                SET [Status] = @Status,
                    [CompletionDate] = @CompletionDate
                WHERE [Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ProjectId);
            parameters.Add("@Status", @event.Status);
            parameters.Add("@CompletionDate", @event.CompletionDate);

            await _connection.ExecuteAsync(sql, parameters);
        }
    }
}