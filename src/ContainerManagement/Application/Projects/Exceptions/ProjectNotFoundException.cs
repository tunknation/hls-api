﻿using System;

namespace HLS.ContainerManagement.Application.Projects.Exceptions {
    public class ProjectNotFoundException : Exception {
        private const string ErrorMessage = "Project not found";

        public ProjectNotFoundException() : base(ErrorMessage) { }
    }
}