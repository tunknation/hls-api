﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Projects.Exceptions;
using HLS.ContainerManagement.Domain.SeedWork;
using HLS.ContainerManagement.IntegrationEvents;
using HLS.Core.EventBus;
using MediatR;

namespace HLS.ContainerManagement.Application.Projects.StartNewProject {
    internal class NewProjectStartedNotificationHandler : INotificationHandler<NewProjectStartedNotification> {
        private readonly IAggregateStore _aggregateStore;

        private readonly IEventsBus _eventsBus;

        public NewProjectStartedNotificationHandler(IAggregateStore aggregateStore, IEventsBus eventsBus) {
            _aggregateStore = aggregateStore;
            _eventsBus = eventsBus;
        }

        public async Task Handle(NewProjectStartedNotification notification, CancellationToken cancellationToken) {
            var project = await _aggregateStore.Load(notification.DomainEvent.ProjectId);

            if (project is null) {
                throw new ProjectNotFoundException();
            }

            var projectSnapshot = project.GetSnapshot();

            var @event = new NewProjectStartedIntegrationEvent(
                notification.Id,
                notification.DomainEvent.OccurredOn,
                projectSnapshot.ProjectId.Value,
                projectSnapshot.CustomerId.Value,
                projectSnapshot.Address.City,
                projectSnapshot.Address.PostalCode,
                projectSnapshot.Address.StreetAddress);

            await _eventsBus.Publish(@event);
        }
    }
}