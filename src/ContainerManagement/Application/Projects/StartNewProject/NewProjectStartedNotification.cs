﻿using System;
using HLS.ContainerManagement.Domain.Projects.Events;
using HLS.Core.Application.Events;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.Projects.StartNewProject {
    public class NewProjectStartedNotification : DomainNotificationBase<NewProjectStartedDomainEvent> {
        [JsonConstructor]
        public NewProjectStartedNotification(NewProjectStartedDomainEvent domainEvent, Guid id) :
            base(domainEvent, id) { }
    }
}