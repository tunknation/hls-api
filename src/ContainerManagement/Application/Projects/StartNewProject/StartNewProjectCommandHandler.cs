﻿using System.Threading;
using System.Threading.Tasks;
using HLS.ContainerManagement.Application.Configuration.Commands;
using HLS.ContainerManagement.Domain.ContainerTypes;
using HLS.ContainerManagement.Domain.Customers;
using HLS.ContainerManagement.Domain.Projects;
using HLS.ContainerManagement.Domain.SeedWork;
using MediatR;

namespace HLS.ContainerManagement.Application.Projects.StartNewProject {
    internal class StartNewProjectCommandHandler : ICommandHandler<StartNewProjectCommand> {
        private readonly IAggregateStore _aggregateStore;

        public StartNewProjectCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public Task<Unit> Handle(StartNewProjectCommand cmd, CancellationToken cancellationToken) {
            var project = Project.StartNewProject(
                new CustomerId(cmd.CustomerId),
                new ContainerTypeId(cmd.ContainerTypeId),
                ProjectAddress.Of(cmd.City, cmd.PostalCode, cmd.StreetAddress),
                cmd.StartDate);

            _aggregateStore.AppendChanges(project);

            return Task.FromResult(Unit.Value);
        }
    }
}