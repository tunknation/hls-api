﻿using System;
using HLS.ContainerManagement.Application.Configuration.Commands;

namespace HLS.ContainerManagement.Application.Projects.StartNewProject {
    public class StartNewProjectCommand : InternalCommandBase {
        public StartNewProjectCommand(
            Guid customerId,
            Guid containerTypeId,
            string city,
            string postalCode,
            string streetAddress,
            DateTime? startDate) {
            CustomerId = customerId;
            ContainerTypeId = containerTypeId;
            City = city;
            PostalCode = postalCode;
            StreetAddress = streetAddress;
            StartDate = startDate;
        }

        public Guid CustomerId { get; }

        public Guid ContainerTypeId { get; }

        public string City { get; }

        public string PostalCode { get; }

        public string StreetAddress { get; }

        public DateTime? StartDate { get; }
    }
}