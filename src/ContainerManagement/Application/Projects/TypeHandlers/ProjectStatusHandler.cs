﻿using System.Data;
using Dapper;
using HLS.ContainerManagement.Domain.Projects;

namespace HLS.ContainerManagement.Application.Projects.TypeHandlers {
    public class ProjectStatusHandler : SqlMapper.TypeHandler<ProjectStatus> {
        public override void SetValue(IDbDataParameter parameter, ProjectStatus status) {
            parameter.Value = status.Code;
            parameter.DbType = DbType.String;
        }

        public override ProjectStatus Parse(object value) {
            return ProjectStatus.Of(value.ToString());
        }
    }
}