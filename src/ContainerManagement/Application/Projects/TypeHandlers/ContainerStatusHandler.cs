﻿using System.Data;
using Dapper;
using HLS.ContainerManagement.Domain.Projects;

namespace HLS.ContainerManagement.Application.Projects.TypeHandlers {
    public class ContainerStatusHandler : SqlMapper.TypeHandler<ContainerStatus> {
        public override void SetValue(IDbDataParameter parameter, ContainerStatus status) {
            parameter.Value = status.Code;
            parameter.DbType = DbType.String;
        }

        public override ContainerStatus Parse(object value) {
            return ContainerStatus.Of(value.ToString());
        }
    }
}