﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.ContainerManagement.Application.Configuration.Queries;
using HLS.ContainerManagement.Application.Projects.Dtos;
using HLS.Core.Application.Data;
using Newtonsoft.Json;

namespace HLS.ContainerManagement.Application.Projects.GetProject {
    internal class GetProjectQueryHandler : IQueryHandler<GetProjectQuery, ProjectDto> {
        private readonly IDbConnection _connection;

        public GetProjectQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<ProjectDto> Handle(GetProjectQuery query, CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [P].[Id],
                       [P].[Status],
                       [P].[CustomerId],
                       [P].[Address],
                       [P].[StartDate],
                       [P].[CompletionDate],
                       [P].[Containers]
                FROM [containermanagement].[v_Project] AS [P]
                WHERE [P].[Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", query.ProjectId);

            var result = await _connection.QueryAsync<ProjectDto, string, ProjectDto>(sql,
                (project, containers) => {
                    project.Containers = JsonConvert.DeserializeObject<List<ContainerDto>>(containers);

                    return project;
                },
                parameters,
                splitOn: "Id,Containers");

            return result.FirstOrDefault();
        }
    }
}