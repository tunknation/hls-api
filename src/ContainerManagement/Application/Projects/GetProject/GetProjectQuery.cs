﻿using System;
using HLS.ContainerManagement.Application.Configuration.Queries;
using HLS.ContainerManagement.Application.Projects.Dtos;

namespace HLS.ContainerManagement.Application.Projects.GetProject {
    public class GetProjectQuery : QueryBase<ProjectDto> {
        public GetProjectQuery(Guid projectId) {
            ProjectId = projectId;
        }

        public Guid ProjectId { get; }
    }
}