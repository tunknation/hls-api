﻿namespace HLS.ContainerManagement.Application.Pagination {
    public interface IPagedQuery {
        int? Page { get; }
        int? PerPage { get; }
    }
}