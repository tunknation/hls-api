﻿using System.Threading.Tasks;
using HLS.Core.Domain;

namespace HLS.ContainerManagement.Application.Configuration.Projections {
    public interface IProjector {
        Task Project(IDomainEvent @event);
    }
}