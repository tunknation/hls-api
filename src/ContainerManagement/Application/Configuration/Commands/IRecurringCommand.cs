﻿namespace HLS.ContainerManagement.Application.Configuration.Commands {
    public interface IRecurringCommand : ICommand { }
}