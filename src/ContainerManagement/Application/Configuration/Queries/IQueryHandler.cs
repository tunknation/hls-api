﻿using MediatR;

namespace HLS.ContainerManagement.Application.Configuration.Queries {
    public interface IQueryHandler<in TQuery, TResult> :
        IRequestHandler<TQuery, TResult>
        where TQuery : IQuery<TResult> { }
}