﻿using MediatR;

namespace HLS.ContainerManagement.Application.Configuration.Queries {
    public interface IQuery<out TResult> : IRequest<TResult> { }
}