﻿using System;
using System.Data;
using Dapper;
using HLS.ContainerManagement.Domain.ContainerTypes;

namespace HLS.ContainerManagement.Application.ContainerTypes.TypeHandlers {
    public class ContainerTypeIdHandler : SqlMapper.TypeHandler<ContainerTypeId> {
        public override void SetValue(IDbDataParameter parameter, ContainerTypeId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override ContainerTypeId Parse(object value) {
            return new ContainerTypeId(Guid.Parse(value.ToString()!));
        }
    }
}