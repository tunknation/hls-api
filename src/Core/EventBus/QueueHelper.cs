﻿using System;
using System.Text;

namespace HLS.Core.EventBus {
    public static class QueueHelper {
        public static string GenerateQueueName<T>(IIntegrationEventHandler<T> handler) where T : IntegrationEvent {
            var handlerType = handler.GetType();
            var builder = new StringBuilder();
            var name = handlerType.Name;
            var index = name.IndexOf("`", StringComparison.Ordinal);

            builder.AppendFormat("{0}-{1}.{2}", typeof(T), handlerType.Namespace, name[..index]);

            return builder.ToString();
        }
    }
}