﻿using System.Threading.Tasks;
using MassTransit;

namespace HLS.Core.EventBus {
    public class MassTransitClient : IEventsBus {
        private readonly IBus _bus;

        public MassTransitClient(IBus bus) {
            _bus = bus;
        }

        public void Dispose() { }

        public async Task Publish<T>(T @event) where T : IntegrationEvent {
            await _bus.Publish(@event);
        }

        public void Subscribe<T>(IIntegrationEventHandler<T> handler) where T : IntegrationEvent {
            _bus.ConnectReceiveEndpoint(
                QueueHelper.GenerateQueueName(handler),
                cfg => { cfg.Instance(new Consumer<T>(handler)); });
        }

        public void StartConsuming() { }
    }

    internal class Consumer<T> : IConsumer<T> where T : IntegrationEvent {
        private readonly IIntegrationEventHandler<T> _handler;

        public Consumer(IIntegrationEventHandler<T> handler) {
            _handler = handler;
        }

        public async Task Consume(ConsumeContext<T> context) {
            await _handler.Handle(context.Message);
        }
    }
}