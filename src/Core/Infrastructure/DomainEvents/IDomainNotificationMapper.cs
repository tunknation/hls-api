﻿using System;

namespace HLS.Core.Infrastructure.DomainEvents {
    public interface IDomainNotificationsMapper {
        string GetName(Type type);

        Type GetType(string name);
    }
}