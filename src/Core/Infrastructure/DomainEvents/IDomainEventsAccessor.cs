﻿using System.Collections.Generic;
using HLS.Core.Domain;

namespace HLS.Core.Infrastructure.DomainEvents {
    public interface IDomainEventsAccessor {
        IReadOnlyCollection<IDomainEvent> GetAllDomainEvents();

        void ClearAllDomainEvents();
    }
}