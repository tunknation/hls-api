﻿using System.Threading.Tasks;

namespace HLS.Core.Infrastructure.DomainEvents {
    public interface IDomainEventsDispatcher {
        Task DispatchEventsAsync();
    }
}