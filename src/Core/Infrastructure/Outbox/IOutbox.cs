﻿using System.Threading.Tasks;

namespace HLS.Core.Infrastructure.Outbox {
    public interface IOutbox {
        void Add(OutboxMessage message);

        Task Save();
    }
}