﻿using System;

namespace HLS.Core.Infrastructure.InternalCommands {
    public interface IInternalCommandsMapper {
        string GetName(Type type);

        Type GetType(string name);
    }
}