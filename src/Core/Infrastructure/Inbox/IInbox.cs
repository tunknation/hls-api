﻿using System.Threading.Tasks;

namespace HLS.Core.Infrastructure.Inbox {
    public interface IInbox {
        void Add(InboxMessage message);

        Task Save();
    }
}