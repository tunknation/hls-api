﻿using System;
using System.Collections.Generic;

namespace HLS.Core.Application.Exceptions {
    public class InvalidCommandException : Exception {
        public InvalidCommandException(List<string> errors) {
            Errors = errors;
        }

        public List<string> Errors { get; }
    }
}