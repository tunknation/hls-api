﻿using System.Data;

namespace HLS.Core.Application.Data {
    public interface ISqlConnectionFactory {
        IDbConnection GetOpenConnection();

        IDbConnection CreateNewConnection();

        string GetConnectionString();
    }
}