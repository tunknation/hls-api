﻿using System;
using HLS.Core.Domain;

namespace HLS.Core.Application.Events {
    public abstract class DomainNotificationBase<T> : IDomainEventNotification<T> where T : IDomainEvent {
        protected DomainNotificationBase(T domainEvent, Guid id) {
            Id = id;
            DomainEvent = domainEvent;
        }

        public Guid Id { get; }

        public T DomainEvent { get; }
    }
}