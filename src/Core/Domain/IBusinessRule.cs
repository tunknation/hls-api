﻿namespace HLS.Core.Domain {
    public interface IBusinessRule {
        string Message { get; }
        bool IsBroken();
    }
}