﻿using System;
using MediatR;

namespace HLS.Core.Domain {
    public interface IDomainEvent : INotification {
        Guid Id { get; }

        DateTime OccurredOn { get; }
    }
}