﻿using System;
using System.Collections.Generic;

namespace HLS.Core.Domain {
    public interface IEntity {
        Guid Id { get; protected set; }
        IReadOnlyCollection<IDomainEvent> GetDomainEvents();
        void AddDomainEvent(IDomainEvent @event);
        void ClearDomainEvents();
        void CheckRule(IBusinessRule rule);
    }
}