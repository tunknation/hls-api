﻿using System;

namespace HLS.API.Modules.Sales.Orders.Requests {
    public class PlaceContainerRetrievalOrderRequest {
        public Guid ProjectId { get; set; }
        
        public Guid ContainerId { get; set; }
    }
}