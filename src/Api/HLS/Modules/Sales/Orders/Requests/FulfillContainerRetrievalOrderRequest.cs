﻿using System;

namespace HLS.API.Modules.Sales.Orders.Requests {
    public class FulfillContainerRetrievalOrderRequest {
        public Guid OrderId { get; set; }
    }
}