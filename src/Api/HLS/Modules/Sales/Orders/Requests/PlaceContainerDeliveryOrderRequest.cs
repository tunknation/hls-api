﻿using System;

namespace HLS.API.Modules.Sales.Orders.Requests {
    public class PlaceContainerDeliveryOrderRequest {
        public Guid CustomerId { get; set; }
        
        public Guid ContainerTypeId { get; set; }
        
        public string City { get; set; }

        public string PostalCode { get; set; }

        public string StreetAddress { get; set; }
    }
}