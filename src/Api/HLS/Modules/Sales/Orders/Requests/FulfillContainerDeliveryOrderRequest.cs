﻿using System;

namespace HLS.API.Modules.Sales.Orders.Requests {
    public class FulfillContainerDeliveryOrderRequest {
        public Guid OrderId { get; set; }
    }
}