﻿using System.Threading.Tasks;
using HLS.API.Modules.Sales.Orders.Requests;
using HLS.Sales.Application.Contracts;
using HLS.Sales.Application.Orders.FulfillContainerDeliveryOrder;
using HLS.Sales.Application.Orders.FulfillContainerRetrievalOrder;
using HLS.Sales.Application.Orders.PlaceContainerDeliveryOrder;
using HLS.Sales.Application.Orders.PlaceContainerRetrievalOrder;
using Microsoft.AspNetCore.Mvc;

namespace HLS.API.Modules.Sales.Orders {
    [ApiController]
    [Route("api/sales/orders")]
    public class OrdersController : ControllerBase {
        private readonly ISalesModule _module;
        
        public OrdersController(ISalesModule module) {
            _module = module;
        }

        [HttpPost("place-container-delivery-order")]
        public async Task<IActionResult> PlaceContainerDeliveryOrder(
            [FromBody] PlaceContainerDeliveryOrderRequest request) {
            var cmd = new PlaceContainerDeliveryOrderCommand(
                request.CustomerId,
                request.ContainerTypeId,
                request.City,
                request.PostalCode,
                request.StreetAddress);

            var orderId = await _module.ExecuteCommandAsync(cmd);

            return Ok(orderId);
        }

        [HttpPost("fulfill-container-delivery-order")]
        public async Task<IActionResult> FulfillContainerDeliveryOrder(
            [FromBody] FulfillContainerDeliveryOrderRequest request) {
            var cmd = new FulfillContainerDeliveryOrderCommand(request.OrderId);

            await _module.ExecuteCommandAsync(cmd);

            return Ok();
        }

        [HttpPost("place-container-retrieval-order")]
        public async Task<IActionResult> PlaceContainerRetrievalOrder(
            [FromBody] PlaceContainerRetrievalOrderRequest request) {
            var cmd = new PlaceContainerRetrievalOrderCommand(request.ProjectId, request.ContainerId);

            var orderId = await _module.ExecuteCommandAsync(cmd);

            return Ok(orderId);
        }
        
        [HttpPost("fulfill-container-retrieval-order")]
        public async Task<IActionResult> FulfillContainerRetrievalOrder(
            [FromBody] FulfillContainerRetrievalOrderRequest request) {
            var cmd = new FulfillContainerRetrievalOrderCommand(request.OrderId);

            await _module.ExecuteCommandAsync(cmd);

            return Ok();
        }
    }
}