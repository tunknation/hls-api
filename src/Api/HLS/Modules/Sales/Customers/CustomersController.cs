﻿using System.Threading.Tasks;
using HLS.Sales.Application.Contracts;
using HLS.Sales.Application.Customers.RegisterNewBusinessCustomer;
using HLS.Sales.Application.Customers.RegisterNewPrivateCustomer;
using Microsoft.AspNetCore.Mvc;

namespace HLS.API.Modules.Sales.Customers {
    [ApiController]
    [Route("api/sales/customers")]
    public class CustomersController : ControllerBase {
        private readonly ISalesModule _module;
        
        public CustomersController(ISalesModule module) {
            _module = module;
        }

        [HttpPost("register-new-business-customer")]
        public async Task<IActionResult> RegisterNewBusinessCustomer() {
            var cmd = new RegisterNewBusinessCustomerCommand();

            var customerId = await _module.ExecuteCommandAsync(cmd);

            return Ok(customerId);
        }
        
        [HttpPost("register-new-private-customer")]
        public async Task<IActionResult> RegisterNewPrivateCustomer() {
            var cmd = new RegisterNewPrivateCustomerCommand();

            var customerId = await _module.ExecuteCommandAsync(cmd);

            return Ok(customerId);
        }
    }
}