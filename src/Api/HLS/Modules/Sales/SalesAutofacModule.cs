﻿using Autofac;
using HLS.Sales.Application.Contracts;
using HLS.Sales.Infrastructure;

namespace HLS.API.Modules.Sales {
    public class SalesAutofacModule : Module {
        protected override void Load(ContainerBuilder builder) {
            builder.RegisterType<SalesModule>()
                .As<ISalesModule>()
                .InstancePerLifetimeScope();
        }
    }
}