﻿using System.Threading.Tasks;
using HLS.API.Modules.Catalog.ContainerTypes.Requests;
using HLS.Catalog.Application.ContainerTypes.AddNewContainerType;
using HLS.Catalog.Application.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace HLS.API.Modules.Catalog.ContainerTypes {
    [ApiController]
    [Route("api/catalog/container-types")]
    public class ContainerTypesController : ControllerBase {
        private readonly ICatalogModule _module;

        public ContainerTypesController(ICatalogModule module) {
            _module = module;
        }
        
        [HttpPost("add-new-container-type")]
        public async Task<IActionResult> AddNewContainerType([FromBody] AddNewContainerTypeRequest request) { 
            var cmd = new AddNewContainerTypeCommand(request.Label);
        
            var containerTypeId = await _module.ExecuteCommandAsync(cmd);
        
            return Ok(containerTypeId); 
        }
    }
}