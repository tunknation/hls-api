﻿namespace HLS.API.Modules.Catalog.ContainerTypes.Requests {
    public class AddNewContainerTypeRequest {
        public string Label { get; set; }
    }
}