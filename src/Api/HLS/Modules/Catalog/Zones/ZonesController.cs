﻿using System.Threading.Tasks;
using HLS.API.Modules.Catalog.Zones.Requests;
using HLS.Catalog.Application.Contracts;
using HLS.Catalog.Application.Zones.AddNewZone;
using Microsoft.AspNetCore.Mvc;

namespace HLS.API.Modules.Catalog.Zones {
    [ApiController]
    [Route("api/catalog/zones")]
    public class ZonesController : ControllerBase {
        private readonly ICatalogModule _module;

        public ZonesController(ICatalogModule module) {
            _module = module;
        }
        
        [HttpPost("add-new-zone")]
        public async Task<IActionResult> AddNewZone([FromBody] AddNewZoneRequest request) { 
            var cmd = new AddNewZoneCommand(request.Label);
        
            var zoneId = await _module.ExecuteCommandAsync(cmd);
        
            return Ok(zoneId); 
        }
    }
}