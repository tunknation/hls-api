﻿namespace HLS.API.Modules.Catalog.Zones.Requests {
    public class AddNewZoneRequest {
        public string Label { get; set; }
    }
}