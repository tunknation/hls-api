﻿using Autofac;
using HLS.Catalog.Application.Contracts;
using HLS.Catalog.Infrastructure;

namespace HLS.API.Modules.Catalog {
    public class CatalogAutofacModule : Module {
        protected override void Load(ContainerBuilder builder) {
            builder.RegisterType<CatalogModule>()
                .As<ICatalogModule>()
                .InstancePerLifetimeScope();
        }
    }
}