﻿using System;
using System.Threading.Tasks;
using HLS.API.Modules.ContainerManagement.Projects.Requests;
using HLS.ContainerManagement.Application.Contracts;
using HLS.ContainerManagement.Application.Projects.AddDescriptiveContainerName;
using HLS.ContainerManagement.Application.Projects.DeliverContainerToProject;
using HLS.ContainerManagement.Application.Projects.GetProject;
using HLS.ContainerManagement.Application.Projects.GetProjects;
using HLS.ContainerManagement.Application.Projects.RetrieveContainerFromProject;
using Microsoft.AspNetCore.Mvc;

namespace HLS.API.Modules.ContainerManagement.Projects {
    [ApiController]
    [Route("api/containermanagement/projects")]
    public class ProjectsController : ControllerBase {
        private readonly IContainerManagementModule _module;

        public ProjectsController(IContainerManagementModule module) {
            _module = module;
        }

        [HttpGet]
        public async Task<IActionResult> GetProjects(int? page, int? perPage) {
            var query = new GetProjectsQuery(page, perPage);

            var result = await _module.ExecuteQueryAsync(query);

            return Ok(result);
        }

        [HttpGet("{projectId:guid}")]
        public async Task<IActionResult> GetProject(Guid projectId) {
            var query = new GetProjectQuery(projectId);

            var result = await _module.ExecuteQueryAsync(query);

            return Ok(result);
        }

        [HttpPost("{projectId:guid}/add-descriptive-container-name")]
        public async Task<IActionResult> AddDescriptiveContainerName(
            Guid projectId,
            [FromBody] AddDescriptiveContainerNameRequest request) {
            var cmd = new AddDescriptiveContainerNameCommand(
                projectId,
                request.ContainerId,
                request.DescriptiveName);

            await _module.ExecuteCommandAsync(cmd);

            return Ok();
        }
    }
}