﻿using System;

namespace HLS.API.Modules.ContainerManagement.Projects.Requests {
    public class AddDescriptiveContainerNameRequest {
        public Guid ContainerId { get; set; }

        public string DescriptiveName { get; set; }
    }
}