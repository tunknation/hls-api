﻿using Autofac;
using HLS.ContainerManagement.Application.Contracts;
using HLS.ContainerManagement.Infrastructure;

namespace HLS.API.Modules.ContainerManagement {
    public class ContainerManagementAutofacModule : Module {
        protected override void Load(ContainerBuilder builder) {
            builder.RegisterType<ContainerManagementModule>()
                .As<IContainerManagementModule>()
                .InstancePerLifetimeScope();
        }
    }
}