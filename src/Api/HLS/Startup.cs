using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Hellang.Middleware.ProblemDetails;
using HLS.API.Configuration.Extensions;
using HLS.API.Configuration.Validation;
using HLS.API.Modules.Catalog;
using HLS.API.Modules.ContainerManagement;
using HLS.API.Modules.Sales;
using HLS.Catalog.Infrastructure;
using HLS.ContainerManagement.Infrastructure;
using HLS.Core.Application.Exceptions;
using HLS.Core.Domain;
using HLS.Core.EventBus;
using HLS.Sales.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Formatting.Compact;
using MassTransit;

namespace HLS.API {
    public class Startup {
        private const string HlsConnectionString = "HlsConnectionString";
        private static ILogger _logger;
        private static ILogger _loggerForApi;
        private readonly IConfiguration _configuration;

        public Startup(IWebHostEnvironment env) {
            ConfigureLogger();

            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json")
                .AddUserSecrets<Startup>()
                .AddEnvironmentVariables("Hls_")
                .Build();
        }

        public void ConfigureServices(IServiceCollection services) {
            services.AddControllers();

            services.AddSwaggerDocumentation();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddProblemDetails(x => {
                x.Map<InvalidCommandException>(ex => new InvalidCommandProblemDetails(ex));
                x.Map<BusinessRuleValidationException>(ex => new BusinessRuleValidationExceptionProblemDetails(ex));
            });

            services.AddMassTransit(x => {
                x.UsingRabbitMq((context, cfg) => {
                    cfg.ConfigureEndpoints(context);
                });
            });
                
            services.AddMassTransitHostedService();
        }

        public void ConfigureContainer(ContainerBuilder containerBuilder) {
            containerBuilder.RegisterType<MassTransitClient>()
                .As<IEventsBus>()
                .SingleInstance();
            
            containerBuilder.RegisterModule(new ContainerManagementAutofacModule());
            containerBuilder.RegisterModule(new CatalogAutofacModule());
            containerBuilder.RegisterModule(new SalesAutofacModule());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider) {
            var container = app.ApplicationServices.GetAutofacRoot();

            app.UseCors(builder =>
                builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            InitializeModules(container);

            if (env.IsDevelopment()) {
                app.UseProblemDetails();
                app.UseSwaggerDocumentation();
            } else {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private static void ConfigureLogger() {
            _logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.Console(
                    outputTemplate:
                    "[{Timestamp:HH:mm:ss} {Level:u3}] [{Module}] [{Context}] {Message:lj}{NewLine}{Exception}")
                .WriteTo.File(new CompactJsonFormatter(), "logs/logs")
                .CreateLogger();

            _loggerForApi = _logger.ForContext("Module", "API");

            _loggerForApi.Information("Logger configured");
        }

        private void InitializeModules(IComponentContext container) {
            var eventBus = container.Resolve<IEventsBus>();
            
            ContainerManagementStartup.Initialize(
                _configuration[HlsConnectionString],
                _logger,
                eventBus);
            
            CatalogStartup.Initialize(
                _configuration[HlsConnectionString],
                _logger,
                eventBus);
            
            SalesStartup.Initialize(
                _configuration[HlsConnectionString],
                _logger,
                eventBus);
        }
    }
}