﻿using System.Collections.Generic;
using HLS.Core.Application.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HLS.API.Configuration.Validation {
    public class InvalidCommandProblemDetails : ProblemDetails {
        public InvalidCommandProblemDetails(InvalidCommandException exception) {
            Title = "Command validation error";
            Status = StatusCodes.Status400BadRequest;
            Errors = exception.Errors;
        }

        public List<string> Errors { get; }
    }
}