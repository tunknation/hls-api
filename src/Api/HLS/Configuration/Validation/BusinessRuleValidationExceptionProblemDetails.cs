﻿using HLS.Core.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HLS.API.Configuration.Validation {
    public class BusinessRuleValidationExceptionProblemDetails : ProblemDetails {
        public BusinessRuleValidationExceptionProblemDetails(BusinessRuleValidationException exception) {
            Title = "Business rule broken";
            Status = StatusCodes.Status409Conflict;
            Detail = exception.Message;
        }
    }
}