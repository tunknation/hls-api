﻿using HotChocolate.Types;

namespace HLS.Gateway.GraphQL {
    [ExtendObjectType(Name = "Query")]
    public class HelloWorldQuery {
        public string GetHelloWorld() {
            return "Hello World!";
        }
    }
}