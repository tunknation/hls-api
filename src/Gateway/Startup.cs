using System;
using System.Net.Http.Headers;
using HLS.Gateway.GraphQL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Formatting.Compact;
using StackExchange.Redis;

namespace HLS.Gateway {
    public class Startup {
        private const string Catalog = "Catalog";
        private const string ContainerManagement = "ContainerManagement";
        private const string Sales = "Sales";

        private const string CatalogUri = "CatalogUri";
        private const string ContainerManagementUri = "ContainerManagementUri";
        private const string SalesUri = "SalesUri";
        private const string RedisUri = "RedisUri";

        private static ILogger _logger;
        private static ILogger _loggerForGateway;
        private readonly IConfiguration _configuration;

        public Startup(IWebHostEnvironment env) {
            _configureLogger();

            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json")
                .AddUserSecrets<Startup>()
                .AddEnvironmentVariables("Gateway_")
                .Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddHttpContextAccessor();

            services.AddCors();

            services.AddHttpClient(
                Catalog,
                (sp, client) => {
                    var context = sp.GetRequiredService<IHttpContextAccessor>().HttpContext;

                    if (context is not null && context.Request.Headers.ContainsKey("Authorization")) {
                        client.DefaultRequestHeaders.Authorization =
                            AuthenticationHeaderValue.Parse(context.Request.Headers["Authorization"].ToString());
                    }

                    client.BaseAddress = new Uri(_configuration[CatalogUri]);
                });

            services.AddHttpClient(
                ContainerManagement,
                (sp, client) => {
                    var context = sp.GetRequiredService<IHttpContextAccessor>().HttpContext;

                    if (context is not null && context.Request.Headers.ContainsKey("Authorization")) {
                        client.DefaultRequestHeaders.Authorization =
                            AuthenticationHeaderValue.Parse(context.Request.Headers["Authorization"].ToString());
                    }

                    client.BaseAddress = new Uri(_configuration[ContainerManagementUri]);
                });

            services.AddHttpClient(
                Sales,
                (sp, client) => {
                    var context = sp.GetRequiredService<IHttpContextAccessor>().HttpContext;

                    if (context is not null && context.Request.Headers.ContainsKey("Authorization")) {
                        client.DefaultRequestHeaders.Authorization =
                            AuthenticationHeaderValue.Parse(context.Request.Headers["Authorization"].ToString());
                    }

                    client.BaseAddress = new Uri(_configuration[SalesUri]);
                });

            services.AddSingleton(ConnectionMultiplexer.Connect(_configuration[RedisUri]));

            services
                .AddGraphQLServer()
                .AddQueryType(d => d.Name("Query"))
                .AddMutationType(d => d.Name("Mutation"))
                .AddType<HelloWorldQuery>()
                .AddRemoteSchemasFromRedis(
                    "Hls",
                    sp => sp.GetRequiredService<ConnectionMultiplexer>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapGraphQL(); });
        }

        private static void _configureLogger() {
            _logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .WriteTo.Console(
                    outputTemplate:
                    "[{Timestamp:HH:mm:ss} {Level:u3}] [{Module}] [{Context}] {Message:lj}{NewLine}{Exception}")
                .WriteTo.File(new CompactJsonFormatter(), "logs/logs")
                .CreateLogger();

            _loggerForGateway = _logger.ForContext("Module", "HlsGateway");

            _loggerForGateway.Information("Logger configured");
        }
    }
}