﻿-- noinspection SqlWithoutWhereForFile

DELETE
FROM [catalog].[InboxMessages]

DELETE
FROM [catalog].[InternalCommands]

DELETE
FROM [catalog].[OutboxMessages]

DELETE
FROM [catalog].[Messages]

DBCC CHECKIDENT ('catalog.Messages', RESEED, 0);

DELETE
FROM [catalog].Streams

DBCC CHECKIDENT ('catalog.Streams', RESEED, 0);

DELETE
FROM [catalog].[SubscriptionCheckpoints]

DELETE
FROM [catalog].[ContainerType]

DELETE
FROM [catalog].[Zone]

DELETE
FROM [catalog].[Product]
