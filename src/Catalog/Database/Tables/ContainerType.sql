﻿CREATE TABLE [catalog].[ContainerType]
(
    [Id]    UNIQUEIDENTIFIER NOT NULL,
    [Label] VARCHAR(255)     NOT NULL,
    CONSTRAINT [PK_catalog_ContainerType_Id] PRIMARY KEY ([Id] ASC)
)