﻿CREATE TABLE [catalog].[Messages]
(
    StreamIdInternal INT                   NOT NULL,
    StreamVersion    INT                   NOT NULL,
    Position         BIGINT IDENTITY (0,1) NOT NULL,
    Id               UNIQUEIDENTIFIER      NOT NULL,
    Created          DATETIME              NOT NULL,
    [Type]           NVARCHAR(128)         NOT NULL,
    JsonData         NVARCHAR(max)         NOT NULL,
    JsonMetadata     NVARCHAR(max),
    CONSTRAINT PK_Events PRIMARY KEY NONCLUSTERED (Position),
    CONSTRAINT FK_Events_Streams FOREIGN KEY (StreamIdInternal) REFERENCES [catalog].[Streams] (IdInternal)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX IX_Messages_Position ON [catalog].[Messages] (Position);
GO

CREATE UNIQUE NONCLUSTERED INDEX IX_Messages_StreamIdInternal_Id ON [catalog].[Messages] (StreamIdInternal, Id);
GO

CREATE UNIQUE NONCLUSTERED INDEX IX_Messages_StreamIdInternal_Revision ON [catalog].[Messages] (StreamIdInternal, StreamVersion);
GO

CREATE NONCLUSTERED INDEX IX_Messages_StreamIdInternal_Created ON [catalog].[Messages] (StreamIdInternal, Created);
GO
