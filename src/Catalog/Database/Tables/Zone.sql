﻿CREATE TABLE [catalog].[Zone]
(
    [Id]    UNIQUEIDENTIFIER NOT NULL,
    [Label] VARCHAR(255)     NOT NULL,
    CONSTRAINT [PK_catalog_Zone_Id] PRIMARY KEY ([Id] ASC)
)