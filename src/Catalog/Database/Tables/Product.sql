﻿CREATE TABLE [catalog].[Product]
(
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [Type]            VARCHAR(50)      NOT NULL,
    [Sku]             VARCHAR(255)     NOT NULL,
    [ContainerTypeId] UNIQUEIDENTIFIER NULL,
    [ZoneId]          UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_catalog_Product_Id] PRIMARY KEY ([Id] ASC)
)