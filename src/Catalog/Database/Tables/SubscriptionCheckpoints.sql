﻿CREATE TABLE [catalog].[SubscriptionCheckpoints]
(
    [Code]     VARCHAR(50) NOT NULL,
    [Position] BIGINT      NOT NULL
)