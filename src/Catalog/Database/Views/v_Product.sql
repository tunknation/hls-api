﻿CREATE VIEW [catalog].[v_Product]
AS
SELECT [P].[Id],
       [P].[Type],
       [P].[Sku],
       [P].[ContainerTypeId],
       [P].[ZoneId]
FROM [catalog].[Product] AS [P]
GO