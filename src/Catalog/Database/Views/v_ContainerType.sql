﻿CREATE VIEW [catalog].[v_ContainerType]
AS
SELECT [CT].[Id],
       [CT].[Label]
FROM [catalog].[ContainerType] AS [CT]
GO