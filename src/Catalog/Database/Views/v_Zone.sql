﻿CREATE VIEW [catalog].[v_Zone]
AS
SELECT [Z].[Id],
       [Z].[Label]
FROM [catalog].[Zone] AS [Z]
GO