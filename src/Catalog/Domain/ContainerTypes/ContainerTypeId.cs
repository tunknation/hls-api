﻿using System;
using HLS.Catalog.Domain.SeedWork;

namespace HLS.Catalog.Domain.ContainerTypes {
    public class ContainerTypeId : AggregateId<ContainerType> {
        public ContainerTypeId(Guid value) : base(value) { }
    }
}