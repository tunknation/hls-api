﻿using HLS.Core.Domain;

namespace HLS.Catalog.Domain.ContainerTypes.Events {
    public class NewContainerTypeAddedDomainEvent : DomainEventBase {
        public NewContainerTypeAddedDomainEvent(ContainerTypeId containerTypeId, string label) {
            ContainerTypeId = containerTypeId;
            Label = label;
        }

        public ContainerTypeId ContainerTypeId { get; }

        public string Label { get; }
    }
}