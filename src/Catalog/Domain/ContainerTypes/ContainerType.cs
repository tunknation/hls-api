﻿using System;
using HLS.Catalog.Domain.ContainerTypes.Events;
using HLS.Catalog.Domain.SeedWork;
using HLS.Core.Domain;

namespace HLS.Catalog.Domain.ContainerTypes {
    public class ContainerType : AggregateRoot<ContainerTypeId> {
        private string _label;

        public static ContainerType AddNewContainerType(string label) {
            var containerType = new ContainerType();

            var id = new ContainerTypeId(Guid.NewGuid());

            var @event = new NewContainerTypeAddedDomainEvent(id, label);

            containerType.Apply(@event);
            containerType.AddDomainEvent(@event);

            return containerType;
        }

        public ContainerTypeSnapshot GetSnapshot() {
            return new ContainerTypeSnapshot(Id, _label);
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(NewContainerTypeAddedDomainEvent @event) {
            Id = @event.ContainerTypeId;
            _label = @event.Label;
        }
    }
}