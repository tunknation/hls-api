﻿namespace HLS.Catalog.Domain.ContainerTypes {
    public class ContainerTypeSnapshot {
        public ContainerTypeSnapshot(ContainerTypeId containerTypeId, string label) {
            ContainerTypeId = containerTypeId;
            Label = label;
        }

        public ContainerTypeId ContainerTypeId { get; }

        public string Label { get; }
    }
}