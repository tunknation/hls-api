﻿using System;
using HLS.Catalog.Domain.SeedWork;

namespace HLS.Catalog.Domain.Zones {
    public class ZoneId : AggregateId<Zone> {
        public ZoneId(Guid value) : base(value) { }
    }
}