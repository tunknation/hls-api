﻿using HLS.Core.Domain;

namespace HLS.Catalog.Domain.Zones.Events {
    public class NewZoneAddedDomainEvent : DomainEventBase {
        public NewZoneAddedDomainEvent(ZoneId zoneId, string label) {
            ZoneId = zoneId;
            Label = label;
        }

        public ZoneId ZoneId { get; }

        public string Label { get; }
    }
}