﻿namespace HLS.Catalog.Domain.Zones {
    public class ZoneSnapshot {
        public ZoneSnapshot(ZoneId zoneId, string label) {
            ZoneId = zoneId;
            Label = label;
        }

        public ZoneId ZoneId { get; }

        public string Label { get; }
    }
}