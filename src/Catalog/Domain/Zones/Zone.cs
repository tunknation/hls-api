﻿using System;
using HLS.Catalog.Domain.SeedWork;
using HLS.Catalog.Domain.Zones.Events;
using HLS.Core.Domain;

namespace HLS.Catalog.Domain.Zones {
    public class Zone : AggregateRoot<ZoneId> {
        private string _label;

        public static Zone AddNewZone(string label) {
            var zone = new Zone();

            var id = new ZoneId(Guid.NewGuid());

            var @event = new NewZoneAddedDomainEvent(id, label);

            zone.Apply(@event);
            zone.AddDomainEvent(@event);

            return zone;
        }

        public ZoneSnapshot GetSnapshot() {
            return new ZoneSnapshot(Id, _label);
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(NewZoneAddedDomainEvent @event) {
            Id = @event.ZoneId;
            _label = @event.Label;
        }
    }
}