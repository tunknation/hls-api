﻿using HLS.Core.Domain;

namespace HLS.Catalog.Domain.SeedWork {
    public abstract class Entity<T> where T : TypedIdValueBase {
        public T Id { get; protected set; }
    }
}