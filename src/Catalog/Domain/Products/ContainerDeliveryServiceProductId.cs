﻿using System;
using HLS.Catalog.Domain.SeedWork;

namespace HLS.Catalog.Domain.Products {
    public class ContainerDeliveryServiceProductId : AggregateId<ContainerDeliveryServiceProduct> {
        public ContainerDeliveryServiceProductId(Guid value) : base(value) { }
    }
}