﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.Catalog.Domain.Products {
    public class ProductSku : ValueObject {
        [JsonConstructor]
        private ProductSku(string code) {
            Code = code;
        }

        public static ProductSku ContainerDeliveryServiceSku(string containerTypeLabel, string zoneLabel) {
            return new ProductSku($"delivery-of-{containerTypeLabel}-to-{zoneLabel}");
        }

        public static ProductSku ContainerRetrievalServiceSku(string containerTypeLabel, string zoneLabel) {
            return new ProductSku($"retrieval-of-{containerTypeLabel}-from-{zoneLabel}");
        }

        public string Code { get; }

        public static ProductSku Of(string code) {
            return new ProductSku(code);
        }

        public static implicit operator string(ProductSku sku) {
            return sku.Code;
        }
    }
}