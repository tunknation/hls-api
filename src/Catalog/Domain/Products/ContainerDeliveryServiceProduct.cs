﻿using System;
using HLS.Catalog.Domain.ContainerTypes;
using HLS.Catalog.Domain.Products.Events;
using HLS.Catalog.Domain.SeedWork;
using HLS.Catalog.Domain.Zones;
using HLS.Core.Domain;

namespace HLS.Catalog.Domain.Products {
    public class ContainerDeliveryServiceProduct : AggregateRoot<ContainerDeliveryServiceProductId> {
        private ProductType _type;

        private ProductSku _sku;

        private ContainerTypeId _containerTypeId;

        private ZoneId _zoneId;

        public static ContainerDeliveryServiceProduct GenerateNewContainerDeliveryServiceProduct(
            ContainerTypeSnapshot containerTypeSnapshot,
            ZoneSnapshot zoneSnapshot) {
            var product = new ContainerDeliveryServiceProduct();

            var id = new ContainerDeliveryServiceProductId(Guid.NewGuid());
            var type = ProductType.ContainerDeliveryService;
            var sku = ProductSku.ContainerDeliveryServiceSku(containerTypeSnapshot.Label, zoneSnapshot.Label);

            var @event = new NewContainerDeliveryServiceProductGeneratedDomainEvent(
                id,
                type,
                sku,
                containerTypeSnapshot.ContainerTypeId,
                zoneSnapshot.ZoneId);

            product.Apply(@event);
            product.AddDomainEvent(@event);

            return product;
        }

        public ContainerDeliveryServiceProductSnapshot GetSnapshot() {
            return new ContainerDeliveryServiceProductSnapshot(
                Id,
                _sku,
                _containerTypeId,
                _zoneId);
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(NewContainerDeliveryServiceProductGeneratedDomainEvent @event) {
            Id = @event.ProductId;
            _type = @event.Type;
            _sku = @event.Sku;
            _containerTypeId = @event.ContainerTypeId;
            _zoneId = @event.ZoneId;
        }
    }
}