﻿using System;
using HLS.Catalog.Domain.SeedWork;

namespace HLS.Catalog.Domain.Products {
    public class ContainerRetrievalServiceProductId : AggregateId<ContainerRetrievalServiceProduct> {
        public ContainerRetrievalServiceProductId(Guid value) : base(value) { }
    }
}