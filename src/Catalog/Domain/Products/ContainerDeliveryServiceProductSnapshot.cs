﻿using HLS.Catalog.Domain.ContainerTypes;
using HLS.Catalog.Domain.Zones;

namespace HLS.Catalog.Domain.Products {
    public class ContainerDeliveryServiceProductSnapshot {
        public ContainerDeliveryServiceProductSnapshot(
            ContainerDeliveryServiceProductId productId,
            ProductSku sku,
            ContainerTypeId containerTypeId,
            ZoneId zoneId) {
            ProductId = productId;
            Sku = sku;
            ContainerTypeId = containerTypeId;
            ZoneId = zoneId;
        }

        public ContainerDeliveryServiceProductId ProductId { get; }

        public ProductSku Sku { get; }

        public ContainerTypeId ContainerTypeId { get; }

        public ZoneId ZoneId { get; }
    }
}