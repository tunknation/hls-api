﻿using HLS.Catalog.Domain.ContainerTypes;
using HLS.Catalog.Domain.Zones;
using HLS.Core.Domain;

namespace HLS.Catalog.Domain.Products.Events {
    public class NewContainerRetrievalServiceProductGeneratedDomainEvent : DomainEventBase {
        public NewContainerRetrievalServiceProductGeneratedDomainEvent(
            ContainerRetrievalServiceProductId productId,
            ProductType type,
            ProductSku sku,
            ContainerTypeId containerTypeId,
            ZoneId zoneId) {
            ProductId = productId;
            Type = type;
            Sku = sku;
            ContainerTypeId = containerTypeId;
            ZoneId = zoneId;
        }

        public ContainerRetrievalServiceProductId ProductId { get; }

        public ProductType Type { get; }

        public ProductSku Sku { get; }

        public ContainerTypeId ContainerTypeId { get; }

        public ZoneId ZoneId { get; }
    }
}