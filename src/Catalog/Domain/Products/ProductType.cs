﻿using HLS.Core.Domain;
using Newtonsoft.Json;

namespace HLS.Catalog.Domain.Products {
    public class ProductType : ValueObject {
        [JsonConstructor]
        private ProductType(string code) {
            Code = code;
        }

        public static ProductType ContainerDeliveryService => new(nameof(ContainerDeliveryService));

        public static ProductType ContainerRetrievalService => new(nameof(ContainerRetrievalService));

        public string Code { get; }

        public static ProductType Of(string code) {
            return new ProductType(code);
        }

        public static implicit operator string(ProductType type) {
            return type.Code;
        }
    }
}