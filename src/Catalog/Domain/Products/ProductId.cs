﻿using System;
using HLS.Core.Domain;

namespace HLS.Catalog.Domain.Products {
    public class ProductId : TypedIdValueBase {
        public ProductId(Guid value) : base(value) { }
    }
}