﻿using System;
using HLS.Catalog.Domain.ContainerTypes;
using HLS.Catalog.Domain.Products.Events;
using HLS.Catalog.Domain.SeedWork;
using HLS.Catalog.Domain.Zones;
using HLS.Core.Domain;

namespace HLS.Catalog.Domain.Products {
    public class ContainerRetrievalServiceProduct : AggregateRoot<ContainerRetrievalServiceProductId> {
        private ProductType _type;

        private ProductSku _sku;

        private ContainerTypeId _containerTypeId;

        private ZoneId _zoneId;

        public static ContainerRetrievalServiceProduct GenerateNewContainerRetrievalServiceProduct(
            ContainerTypeSnapshot containerTypeSnapshot,
            ZoneSnapshot zoneSnapshot) {
            var product = new ContainerRetrievalServiceProduct();

            var id = new ContainerRetrievalServiceProductId(Guid.NewGuid());
            var type = ProductType.ContainerRetrievalService;
            var sku = ProductSku.ContainerRetrievalServiceSku(containerTypeSnapshot.Label, zoneSnapshot.Label);

            var @event = new NewContainerRetrievalServiceProductGeneratedDomainEvent(
                id,
                type,
                sku,
                containerTypeSnapshot.ContainerTypeId,
                zoneSnapshot.ZoneId);

            product.Apply(@event);
            product.AddDomainEvent(@event);

            return product;
        }

        public ContainerRetrievalServiceProductSnapshot GetSnapshot() {
            return new ContainerRetrievalServiceProductSnapshot(
                Id,
                _sku,
                _containerTypeId,
                _zoneId);
        }

        protected override void Apply(IDomainEvent @event) {
            _when((dynamic)@event);
        }

        private void _when(NewContainerRetrievalServiceProductGeneratedDomainEvent @event) {
            Id = @event.ProductId;
            _type = @event.Type;
            _sku = @event.Sku;
            _containerTypeId = @event.ContainerTypeId;
            _zoneId = @event.ZoneId;
        }
    }
}