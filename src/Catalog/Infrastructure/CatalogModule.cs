﻿using System.Threading.Tasks;
using Autofac;
using HLS.Catalog.Application.Configuration.Commands;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.Contracts;
using HLS.Catalog.Infrastructure.Configuration.Processing;
using MediatR;

namespace HLS.Catalog.Infrastructure {
    public class CatalogModule : ICatalogModule {
        public async Task<TResult> ExecuteCommandAsync<TResult>(ICommand<TResult> command) {
            return await CommandsExecutor.Execute(command);
        }

        public async Task ExecuteCommandAsync(ICommand command) {
            await CommandsExecutor.Execute(command);
        }

        public async Task<TResult> ExecuteQueryAsync<TResult>(IQuery<TResult> query) {
            using var scope = CatalogCompositionRoot.BeginLifetimeScope();
            var mediator = scope.Resolve<IMediator>();

            return await mediator.Send(query);
        }
    }
}