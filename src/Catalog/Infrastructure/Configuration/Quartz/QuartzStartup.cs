﻿using System.Collections.Specialized;
using HLS.Catalog.Infrastructure.Configuration.Quartz.Jobs;
using Quartz;
using Quartz.Impl;
using Quartz.Logging;
using Serilog;

namespace HLS.Catalog.Infrastructure.Configuration.Quartz {
    internal static class QuartzStartup {
        private static IScheduler _scheduler;

        internal static void StopQuartz() {
            _scheduler.Shutdown();
        }

        internal static void Initialize(ILogger logger) {
            logger.Information("Quartz starting...");

            var schedulerConfiguration = new NameValueCollection {
                {
                    "quartz.scheduler.instanceName", "Catalog"
                }
            };

            ISchedulerFactory schedulerFactory = new StdSchedulerFactory(schedulerConfiguration);
            _scheduler = schedulerFactory.GetScheduler().GetAwaiter().GetResult();

            LogProvider.SetCurrentLogProvider(new SerilogLogProvider(logger));

            _scheduler.Start().GetAwaiter().GetResult();

            ScheduleProcessInternalCommandsJob(_scheduler);

            ScheduleProcessInboxJob(_scheduler);

            ScheduleProcessOutboxJob(_scheduler);

            logger.Information("Quartz started.");
        }

        private static void ScheduleProcessInternalCommandsJob(IScheduler scheduler) {
            var job = JobBuilder.Create<ProcessInternalCommandsJob>().Build();
            var trigger =
                TriggerBuilder
                    .Create()
                    .StartNow()
                    .WithCronSchedule("0/2 * * ? * *")
                    .Build();

            scheduler
                .ScheduleJob(job, trigger)
                .GetAwaiter()
                .GetResult();
        }

        private static void ScheduleProcessInboxJob(IScheduler scheduler) {
            var job = JobBuilder.Create<ProcessInboxJob>().Build();
            var trigger =
                TriggerBuilder
                    .Create()
                    .StartNow()
                    .WithCronSchedule("0/2 * * ? * *")
                    .Build();

            scheduler
                .ScheduleJob(job, trigger)
                .GetAwaiter()
                .GetResult();
        }

        private static void ScheduleProcessOutboxJob(IScheduler scheduler) {
            var job = JobBuilder.Create<ProcessOutboxJob>().Build();
            var trigger =
                TriggerBuilder
                    .Create()
                    .StartNow()
                    .WithCronSchedule("0/2 * * ? * *")
                    .Build();

            scheduler
                .ScheduleJob(job, trigger)
                .GetAwaiter()
                .GetResult();
        }
    }
}