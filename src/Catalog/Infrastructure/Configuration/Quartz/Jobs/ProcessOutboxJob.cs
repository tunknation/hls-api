﻿using System.Threading.Tasks;
using HLS.Catalog.Infrastructure.Configuration.Processing;
using HLS.Catalog.Infrastructure.Configuration.Processing.Outbox;
using Quartz;

namespace HLS.Catalog.Infrastructure.Configuration.Quartz.Jobs {
    [DisallowConcurrentExecution]
    public class ProcessOutboxJob : IJob {
        public async Task Execute(IJobExecutionContext context) {
            await CommandsExecutor.Execute(new ProcessOutboxCommand());
        }
    }
}