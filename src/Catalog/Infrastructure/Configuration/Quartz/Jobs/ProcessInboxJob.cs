﻿using System.Threading.Tasks;
using HLS.Catalog.Infrastructure.Configuration.Processing;
using HLS.Catalog.Infrastructure.Configuration.Processing.Inbox;
using Quartz;

namespace HLS.Catalog.Infrastructure.Configuration.Quartz.Jobs {
    [DisallowConcurrentExecution]
    public class ProcessInboxJob : IJob {
        public async Task Execute(IJobExecutionContext context) {
            await CommandsExecutor.Execute(new ProcessInboxCommand());
        }
    }
}