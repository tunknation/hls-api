﻿using Autofac;
using HLS.Catalog.Application.Configuration.Projections;
using HLS.Catalog.Domain.SeedWork;
using HLS.Catalog.Infrastructure.AggregateStore;
using HLS.Core.Application.Data;
using HLS.Core.Infrastructure.Data;
using Microsoft.Extensions.Logging;
using SqlStreamStore;

namespace HLS.Catalog.Infrastructure.Configuration.DataAccess {
    internal class DataAccessModule : Module {
        private readonly string _databaseConnectionString;
        private readonly ILoggerFactory _loggerFactory;

        internal DataAccessModule(string databaseConnectionString, ILoggerFactory loggerFactory) {
            _databaseConnectionString = databaseConnectionString;
            _loggerFactory = loggerFactory;
        }

        protected override void Load(ContainerBuilder builder) {
            builder.RegisterType<SqlConnectionFactory>()
                .As<ISqlConnectionFactory>()
                .WithParameter("connectionString", _databaseConnectionString)
                .InstancePerLifetimeScope();

            IStreamStore streamStore = new MsSqlStreamStore(new MsSqlStreamStoreSettings(_databaseConnectionString) {
                Schema = DatabaseSchema.Name
            });

            builder.RegisterInstance(streamStore);

            builder.RegisterType<SqlStreamAggregateStore>()
                .As<IAggregateStore>()
                .InstancePerLifetimeScope();

            builder.RegisterType<SqlServerCheckpointStore>()
                .As<ICheckpointStore>()
                .InstancePerLifetimeScope();

            var applicationAssembly = typeof(IProjector).Assembly;
            builder.RegisterAssemblyTypes(applicationAssembly)
                .Where(type => type.Name.EndsWith("Projector"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope()
                .FindConstructorsWith(new AllConstructorFinder());

            builder.RegisterType<SubscriptionsManager>()
                .As<SubscriptionsManager>()
                .SingleInstance();

            var infrastructureAssembly = ThisAssembly;
            builder.RegisterAssemblyTypes(infrastructureAssembly)
                .Where(type => type.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope()
                .FindConstructorsWith(new AllConstructorFinder());
        }
    }
}