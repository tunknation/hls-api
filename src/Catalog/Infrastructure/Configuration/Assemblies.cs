﻿using System.Reflection;
using HLS.Catalog.Application.Configuration.Commands;

namespace HLS.Catalog.Infrastructure.Configuration {
    internal static class Assemblies {
        public static readonly Assembly Application = typeof(InternalCommandBase).Assembly;
    }
}