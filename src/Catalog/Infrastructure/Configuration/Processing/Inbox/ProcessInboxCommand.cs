﻿using HLS.Catalog.Application.Configuration.Commands;

namespace HLS.Catalog.Infrastructure.Configuration.Processing.Inbox {
    public class ProcessInboxCommand : CommandBase, IRecurringCommand { }
}