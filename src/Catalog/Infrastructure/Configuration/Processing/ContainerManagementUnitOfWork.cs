﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Dapper;
using HLS.Catalog.Domain.SeedWork;
using HLS.Core.Application.Data;
using HLS.Core.Infrastructure.DomainEvents;
using HLS.Core.Infrastructure.Outbox;
using HLS.Core.Infrastructure.UnitOfWork;

namespace HLS.Catalog.Infrastructure.Configuration.Processing {
    public class CatalogUnitOfWork : IUnitOfWork {
        private readonly IAggregateStore _aggregateStore;
        private readonly IDomainEventsDispatcher _domainEventsDispatcher;
        private readonly IOutbox _outbox;
        private readonly ISqlConnectionFactory _sqlConnectionFactory;

        public CatalogUnitOfWork(
            IOutbox outbox,
            IAggregateStore aggregateStore,
            IDomainEventsDispatcher domainEventsDispatcher,
            ISqlConnectionFactory sqlConnectionFactory) {
            _outbox = outbox;
            _aggregateStore = aggregateStore;
            _domainEventsDispatcher = domainEventsDispatcher;
            _sqlConnectionFactory = sqlConnectionFactory;
        }

        public async Task<int> CommitAsync(
            CancellationToken cancellationToken = default,
            Guid? internalCommandId = null) {
            await _domainEventsDispatcher.DispatchEventsAsync();

            var options = new TransactionOptions {
                IsolationLevel = IsolationLevel.ReadCommitted
            };

            using var transaction = new TransactionScope(
                TransactionScopeOption.Required,
                options,
                TransactionScopeAsyncFlowOption.Enabled);

            await _aggregateStore.Save();

            await _outbox.Save();

            if (internalCommandId.HasValue) {
                using var connection = _sqlConnectionFactory.CreateNewConnection();
                await connection.ExecuteScalarAsync(
                    @"UPDATE catalog.InternalCommands
                         SET ProcessedDate = @Date
                         WHERE Id = @Id",
                    new {
                        Date = DateTime.UtcNow,
                        Id = internalCommandId.Value
                    });
            }

            transaction.Complete();

            return 0;
        }
    }
}