﻿using System;
using System.Threading;
using System.Threading.Tasks;
using HLS.Catalog.Application.Configuration.Commands;
using HLS.Core.Infrastructure.UnitOfWork;
using MediatR;

namespace HLS.Catalog.Infrastructure.Configuration.Processing {
    internal class UnitOfWorkCommandHandlerDecorator<T> : ICommandHandler<T> where T : ICommand {
        private readonly ICommandHandler<T> _decorated;

        private readonly IUnitOfWork _unitOfWork;

        public UnitOfWorkCommandHandlerDecorator(
            ICommandHandler<T> decorated,
            IUnitOfWork unitOfWork) {
            _decorated = decorated;
            _unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(T command, CancellationToken cancellationToken) {
            await _decorated.Handle(command, cancellationToken);

            Guid? internalCommandId = null;
            if (command is InternalCommandBase) {
                internalCommandId = command.Id;
            }

            await _unitOfWork.CommitAsync(cancellationToken, internalCommandId);

            return Unit.Value;
        }
    }
}