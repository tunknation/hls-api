﻿using System;
using HLS.Catalog.Application.ContainerTypes.AddNewContainerType;
using HLS.Catalog.Application.Products.GenerateNewContainerDeliveryServiceProduct;
using HLS.Catalog.Application.Products.GenerateNewContainerRetrievalServiceProduct;
using HLS.Catalog.Application.Zones.AddNewZone;
using HLS.Core.Infrastructure;

namespace HLS.Catalog.Infrastructure.Configuration.Processing {
    internal static class DomainNotificationTypeMappings {
        static DomainNotificationTypeMappings() {
            Dictionary = new BiDictionary<string, Type>();
            Dictionary.Add(nameof(NewContainerTypeAddedNotification), typeof(NewContainerTypeAddedNotification));
            Dictionary.Add(nameof(NewZoneAddedNotification), typeof(NewZoneAddedNotification));
            Dictionary.Add(
                nameof(NewContainerDeliveryServiceProductGeneratedNotification),
                typeof(NewContainerDeliveryServiceProductGeneratedNotification));
            Dictionary.Add(
                nameof(NewContainerRetrievalServiceProductGeneratedNotification),
                typeof(NewContainerRetrievalServiceProductGeneratedNotification));
        }

        internal static BiDictionary<string, Type> Dictionary { get; }
    }
}