﻿using HLS.Catalog.Application.Configuration.Commands;

namespace HLS.Catalog.Infrastructure.Configuration.Processing.Outbox {
    public class ProcessOutboxCommand : CommandBase, IRecurringCommand { }
}