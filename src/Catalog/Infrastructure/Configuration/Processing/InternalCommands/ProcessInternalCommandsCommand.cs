﻿using HLS.Catalog.Application.Configuration.Commands;

namespace HLS.Catalog.Infrastructure.Configuration.Processing.InternalCommands {
    internal class ProcessInternalCommandsCommand : CommandBase, IRecurringCommand { }
}