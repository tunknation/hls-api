﻿using System;
using System.Collections.Generic;
using HLS.Catalog.Domain.ContainerTypes.Events;
using HLS.Catalog.Domain.Products.Events;
using HLS.Catalog.Domain.Zones.Events;

namespace HLS.Catalog.Infrastructure.AggregateStore {
    internal static class DomainEventTypeMappings {
        static DomainEventTypeMappings() {
            Dictionary = new Dictionary<string, Type> {
                { "NewContainerTypeAddedDomainEvent", typeof(NewContainerTypeAddedDomainEvent) },
                { "NewZoneAddedDomainEvent", typeof(NewZoneAddedDomainEvent) }, {
                    "NewContainerDeliveryServiceProductGeneratedDomainEvent",
                    typeof(NewContainerDeliveryServiceProductGeneratedDomainEvent)
                }, {
                    "NewContainerRetrievalServiceProductGeneratedDomainEvent",
                    typeof(NewContainerRetrievalServiceProductGeneratedDomainEvent)
                }
            };
        }

        internal static IDictionary<string, Type> Dictionary { get; }
    }
}