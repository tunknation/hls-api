﻿using System.Threading.Tasks;

namespace HLS.Catalog.Infrastructure.AggregateStore {
    public interface ICheckpointStore {
        long? GetCheckpoint(SubscriptionCode subscriptionCode);

        Task StoreCheckpoint(SubscriptionCode subscriptionCode, long checkpoint);
    }
}