﻿using Autofac;
using Dapper;
using HLS.Catalog.Application.ContainerTypes.TypeHandlers;
using HLS.Catalog.Application.Products.TypeHandlers;
using HLS.Catalog.Application.Zones.TypeHandlers;
using HLS.Catalog.Infrastructure.AggregateStore;
using HLS.Catalog.Infrastructure.Configuration.DataAccess;
using HLS.Catalog.Infrastructure.Configuration.EventsBus;
using HLS.Catalog.Infrastructure.Configuration.Logging;
using HLS.Catalog.Infrastructure.Configuration.Mediation;
using HLS.Catalog.Infrastructure.Configuration.Processing;
using HLS.Catalog.Infrastructure.Configuration.Processing.Outbox;
using HLS.Catalog.Infrastructure.Configuration.Quartz;
using HLS.Core.EventBus;
using Serilog;
using Serilog.Extensions.Logging;

namespace HLS.Catalog.Infrastructure {
    public static class CatalogStartup {
        private static IContainer _container;

        private static SubscriptionsManager _subscriptionsManager;

        public static void Initialize(
            string connectionString,
            ILogger logger,
            IEventsBus eventsBus,
            bool runQuartz = true) {
            var moduleLogger = logger.ForContext("Module", "Catalog");

            ConfigureCompositionRoot(connectionString, moduleLogger, eventsBus, runQuartz);

            if (runQuartz) {
                QuartzStartup.Initialize(moduleLogger);
            }

            EventsBusStartup.Initialize(moduleLogger);
        }

        public static void Stop() {
            _subscriptionsManager.Stop();
            QuartzStartup.StopQuartz();
        }

        private static void ConfigureCompositionRoot(
            string connectionString,
            ILogger logger,
            IEventsBus eventsBus,
            bool runQuartz = true) {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new LoggingModule(logger));

            var loggerFactory = new SerilogLoggerFactory(logger);
            containerBuilder.RegisterModule(new DataAccessModule(connectionString, loggerFactory));

            containerBuilder.RegisterModule(new ProcessingModule());
            containerBuilder.RegisterModule(new EventsBusModule(eventsBus));
            containerBuilder.RegisterModule(new MediatorModule());
            containerBuilder.RegisterModule(new OutboxModule());

            if (runQuartz) {
                containerBuilder.RegisterModule(new QuartzModule());
            }

            _container = containerBuilder.Build();

            CatalogCompositionRoot.SetContainer(_container);

            AddSqlTypeHandlers();

            RunEventsProjectors();
        }

        private static void AddSqlTypeHandlers() {
            SqlMapper.AddTypeHandler(new ContainerTypeIdHandler());
            SqlMapper.AddTypeHandler(new ContainerDeliveryServiceProductIdHandler());
            SqlMapper.AddTypeHandler(new ContainerRetrievalServiceProductIdHandler());
            SqlMapper.AddTypeHandler(new ProductIdHandler());
            SqlMapper.AddTypeHandler(new ProductSkuHandler());
            SqlMapper.AddTypeHandler(new ProductTypeHandler());
            SqlMapper.AddTypeHandler(new ZoneIdHandler());
        }

        private static void RunEventsProjectors() {
            _subscriptionsManager = _container.Resolve<SubscriptionsManager>();

            _subscriptionsManager.Start();
        }
    }
}