﻿using System;
using System.Data;
using Dapper;
using HLS.Catalog.Domain.Products;

namespace HLS.Catalog.Application.Products.TypeHandlers {
    public class ProductIdHandler : SqlMapper.TypeHandler<ProductId> {
        public override void SetValue(IDbDataParameter parameter, ProductId id) {
            parameter.Value = id.Value;
            parameter.DbType = DbType.Guid;
        }

        public override ProductId Parse(object value) {
            return new ProductId(Guid.Parse(value.ToString()!));
        }
    }
}