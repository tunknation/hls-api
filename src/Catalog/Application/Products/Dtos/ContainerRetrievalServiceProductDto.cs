﻿using HLS.Catalog.Domain.ContainerTypes;
using HLS.Catalog.Domain.Products;
using HLS.Catalog.Domain.Zones;

namespace HLS.Catalog.Application.Products.Dtos {
    public class ContainerRetrievalServiceProductDto : ProductDto {
        public new ContainerRetrievalServiceProductId Id { get; set; }

        public ContainerTypeId ContainerTypeId { get; set; }

        public ZoneId ZoneId { get; set; }
    }
}