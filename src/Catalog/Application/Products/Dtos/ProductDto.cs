﻿using HLS.Catalog.Domain.Products;

namespace HLS.Catalog.Application.Products.Dtos {
    public abstract class ProductDto {
        public ProductId Id { get; set; }

        public ProductType Type { get; set; }

        public ProductSku Sku { get; set; }
    }
}