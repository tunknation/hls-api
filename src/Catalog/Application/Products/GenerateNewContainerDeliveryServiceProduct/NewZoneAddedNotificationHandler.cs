﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Catalog.Application.Configuration.Commands;
using HLS.Catalog.Application.Zones.AddNewZone;
using HLS.Core.Application.Data;
using MediatR;

namespace HLS.Catalog.Application.Products.GenerateNewContainerDeliveryServiceProduct {
    internal class NewZoneAddedNotificationHandler : INotificationHandler<NewZoneAddedNotification> {
        private readonly IDbConnection _connection;
        private readonly ICommandsScheduler _commandsScheduler;

        public NewZoneAddedNotificationHandler(
            ISqlConnectionFactory sqlConnectionFactory,
            ICommandsScheduler commandsScheduler) {
            _connection = sqlConnectionFactory.GetOpenConnection();
            _commandsScheduler = commandsScheduler;
        }

        public async Task Handle(
            NewZoneAddedNotification notification,
            CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [CT].[Id]
                FROM [catalog].[ContainerType] as [CT]";

            var containerTypeIds = await _connection.QueryAsync<Guid>(sql);

            foreach (var containerTypeId in containerTypeIds) {
                var cmd = new GenerateNewContainerDeliveryServiceProductCommand(
                    containerTypeId,
                    notification.DomainEvent.ZoneId.Value);

                await _commandsScheduler.EnqueueAsync(cmd);
            }
        }
    }
}