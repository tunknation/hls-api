﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Catalog.Application.Configuration.Commands;
using HLS.Catalog.Application.ContainerTypes.AddNewContainerType;
using HLS.Core.Application.Data;
using MediatR;

namespace HLS.Catalog.Application.Products.GenerateNewContainerDeliveryServiceProduct {
    internal class NewContainerTypeAddedNotificationHandler : INotificationHandler<NewContainerTypeAddedNotification> {
        private readonly IDbConnection _connection;
        private readonly ICommandsScheduler _commandsScheduler;

        public NewContainerTypeAddedNotificationHandler(
            ISqlConnectionFactory sqlConnectionFactory,
            ICommandsScheduler commandsScheduler) {
            _connection = sqlConnectionFactory.GetOpenConnection();
            _commandsScheduler = commandsScheduler;
        }

        public async Task Handle(
            NewContainerTypeAddedNotification notification,
            CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [Z].[Id]
                FROM [catalog].[Zone] as [Z]";

            var zoneIds = await _connection.QueryAsync<Guid>(sql);

            foreach (var zoneId in zoneIds) {
                var cmd = new GenerateNewContainerDeliveryServiceProductCommand(
                    notification.DomainEvent.ContainerTypeId.Value,
                    zoneId);

                await _commandsScheduler.EnqueueAsync(cmd);
            }
        }
    }
}