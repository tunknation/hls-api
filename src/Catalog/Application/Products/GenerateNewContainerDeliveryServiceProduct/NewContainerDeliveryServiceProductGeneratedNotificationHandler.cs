﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Catalog.Application.Products.Exceptions;
using HLS.Catalog.Domain.SeedWork;
using HLS.Catalog.IntegrationEvents;
using HLS.Core.EventBus;
using MediatR;

namespace HLS.Catalog.Application.Products.GenerateNewContainerDeliveryServiceProduct {
    internal class NewContainerDeliveryServiceProductGeneratedNotificationHandler : INotificationHandler<
        NewContainerDeliveryServiceProductGeneratedNotification> {
        private readonly IAggregateStore _aggregateStore;

        private readonly IEventsBus _eventsBus;

        public NewContainerDeliveryServiceProductGeneratedNotificationHandler(
            IAggregateStore aggregateStore,
            IEventsBus eventsBus) {
            _aggregateStore = aggregateStore;
            _eventsBus = eventsBus;
        }

        public async Task Handle(
            NewContainerDeliveryServiceProductGeneratedNotification notification,
            CancellationToken cancellationToken) {
            var product = await _aggregateStore.Load(notification.DomainEvent.ProductId);

            if (product is null) {
                throw new ProductNotFoundException();
            }

            var productSnapshot = product.GetSnapshot();

            var @event = new NewContainerDeliveryServiceProductGeneratedIntegrationEvent(
                notification.Id,
                notification.DomainEvent.OccurredOn,
                productSnapshot.ProductId.Value,
                productSnapshot.Sku.Code,
                productSnapshot.ContainerTypeId.Value,
                productSnapshot.ZoneId.Value);

            await _eventsBus.Publish(@event);
        }
    }
}