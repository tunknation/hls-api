﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Catalog.Application.Configuration.Commands;
using HLS.Catalog.Application.ContainerTypes.Exceptions;
using HLS.Catalog.Application.Zones.Exceptions;
using HLS.Catalog.Domain.ContainerTypes;
using HLS.Catalog.Domain.Products;
using HLS.Catalog.Domain.SeedWork;
using HLS.Catalog.Domain.Zones;
using MediatR;

namespace HLS.Catalog.Application.Products.GenerateNewContainerDeliveryServiceProduct {
    internal class
        GenerateNewContainerDeliveryServiceProductCommandHandler : ICommandHandler<
            GenerateNewContainerDeliveryServiceProductCommand> {
        private readonly IAggregateStore _aggregateStore;

        public GenerateNewContainerDeliveryServiceProductCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(
            GenerateNewContainerDeliveryServiceProductCommand cmd,
            CancellationToken cancellationToken) {
            var containerType = await _aggregateStore.Load(new ContainerTypeId(cmd.ContainerTypeId));
            if (containerType is null) {
                throw new ContainerTypeNotFoundException();
            }

            var zone = await _aggregateStore.Load(new ZoneId(cmd.ZoneId));
            if (zone is null) {
                throw new ZoneNotFoundException();
            }

            var product = ContainerDeliveryServiceProduct
                .GenerateNewContainerDeliveryServiceProduct(
                    containerType.GetSnapshot(),
                    zone.GetSnapshot());

            _aggregateStore.AppendChanges(product);

            return Unit.Value;
        }
    }
}