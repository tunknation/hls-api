﻿using System;
using System.Text.Json.Serialization;
using HLS.Catalog.Domain.Products.Events;
using HLS.Core.Application.Events;

namespace HLS.Catalog.Application.Products.GenerateNewContainerDeliveryServiceProduct {
    public class NewContainerDeliveryServiceProductGeneratedNotification : DomainNotificationBase<
        NewContainerDeliveryServiceProductGeneratedDomainEvent> {
        [JsonConstructor]
        public NewContainerDeliveryServiceProductGeneratedNotification(
            NewContainerDeliveryServiceProductGeneratedDomainEvent domainEvent,
            Guid id) : base(domainEvent, id) { }
    }
}