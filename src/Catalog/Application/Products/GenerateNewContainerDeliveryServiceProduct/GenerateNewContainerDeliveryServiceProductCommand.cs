﻿using System;
using HLS.Catalog.Application.Configuration.Commands;

namespace HLS.Catalog.Application.Products.GenerateNewContainerDeliveryServiceProduct {
    public class GenerateNewContainerDeliveryServiceProductCommand : InternalCommandBase {
        public GenerateNewContainerDeliveryServiceProductCommand(Guid containerTypeId, Guid zoneId) {
            ContainerTypeId = containerTypeId;
            ZoneId = zoneId;
        }

        public Guid ContainerTypeId { get; }

        public Guid ZoneId { get; }
    }
}