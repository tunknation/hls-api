﻿using System.Collections.Generic;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.Pagination;
using HLS.Catalog.Application.Products.Dtos;

namespace HLS.Catalog.Application.Products.GetProducts {
    public class GetProductsQuery : QueryBase<IEnumerable<ProductDto>>, IPagedQuery {
        public GetProductsQuery(int? page, int? perPage) {
            Page = page;
            PerPage = perPage;
        }

        public int? Page { get; }

        public int? PerPage { get; }
    }
}