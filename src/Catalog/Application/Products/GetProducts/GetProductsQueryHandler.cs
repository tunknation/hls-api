﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.Pagination;
using HLS.Catalog.Application.Products.Dtos;
using HLS.Catalog.Domain.Products;
using HLS.Core.Application.Data;

namespace HLS.Catalog.Application.Products.GetProducts {
    internal class GetProductsQueryHandler : IQueryHandler<GetProductsQuery, IEnumerable<ProductDto>> {
        private readonly IDbConnection _connection;

        public GetProductsQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<IEnumerable<ProductDto>> Handle(
            GetProductsQuery query,
            CancellationToken cancellationToken) {
            var parameters = new DynamicParameters();
            var pageData = PagedQueryHelper.GetPageData(query);

            parameters.Add(nameof(PagedQueryHelper.Offset), pageData.Offset);
            parameters.Add(nameof(PagedQueryHelper.Next), pageData.Next);

            var sql = @"
                SELECT [P].[Id],
                       [P].[Type],
                       [P].[Sku],
                       [P].[ContainerTypeId],
                       [P].[ZoneId]
                FROM [catalog].[v_Product] AS [P]
                ORDER BY [P].[Id]";

            sql = PagedQueryHelper.AppendPageStatement(sql);

            using var reader = await _connection.ExecuteReaderAsync(sql, parameters);

            var containerDeliveryServiceProductParser = reader.GetRowParser<ContainerDeliveryServiceProductDto>();
            var containerRetrievalServiceProductParser = reader.GetRowParser<ContainerRetrievalServiceProductDto>();

            var products = new List<ProductDto>();

            while (reader.Read()) {
                ProductDto product = reader.GetString(reader.GetOrdinal("Type")) switch {
                    nameof(ProductType.ContainerDeliveryService) => containerDeliveryServiceProductParser(reader),
                    nameof(ProductType.ContainerRetrievalService) => containerRetrievalServiceProductParser(reader),
                    _ => throw new Exception("Product type is not supported")
                };

                products.Add(product);
            }

            return products;
        }
    }
}