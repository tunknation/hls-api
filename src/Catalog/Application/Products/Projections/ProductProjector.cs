﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using HLS.Catalog.Application.Configuration.Projections;
using HLS.Catalog.Domain.Products.Events;
using HLS.Core.Application.Data;
using HLS.Core.Domain;

namespace HLS.Catalog.Application.Products.Projections {
    internal class ProductProjector : IProjector {
        private readonly IDbConnection _connection;

        public ProductProjector(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Project(IDomainEvent @event) {
            await _when((dynamic)@event);
        }

        private static Task _when(IDomainEvent @event) {
            return Task.CompletedTask;
        }

        private async Task _when(NewContainerDeliveryServiceProductGeneratedDomainEvent @event) {
            const string sql = @"
                INSERT INTO [catalog].[Product]
                    ([Id], [Type], [Sku], [ContainerTypeId], [ZoneId])
                VALUES (@Id, @Type, @Sku, @ContainerTypeId, @ZoneId)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ProductId);
            parameters.Add("@Type", @event.Type);
            parameters.Add("@Sku", @event.Sku);
            parameters.Add("@ContainerTypeId", @event.ContainerTypeId);
            parameters.Add("@ZoneId", @event.ZoneId);

            await _connection.ExecuteAsync(sql, parameters);
        }

        private async Task _when(NewContainerRetrievalServiceProductGeneratedDomainEvent @event) {
            const string sql = @"
                INSERT INTO [catalog].[Product]
                    ([Id], [Type], [Sku], [ContainerTypeId], [ZoneId])
                VALUES (@Id, @Type, @Sku, @ContainerTypeId, @ZoneId)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ProductId);
            parameters.Add("@Type", @event.Type);
            parameters.Add("@Sku", @event.Sku);
            parameters.Add("@ContainerTypeId", @event.ContainerTypeId);
            parameters.Add("@ZoneId", @event.ZoneId);

            await _connection.ExecuteAsync(sql, parameters);
        }
    }
}