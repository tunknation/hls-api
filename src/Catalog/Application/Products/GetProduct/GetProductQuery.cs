﻿using System;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.Products.Dtos;

namespace HLS.Catalog.Application.Products.GetProduct {
    public class GetProductQuery : QueryBase<ProductDto> {
        public GetProductQuery(Guid productId) {
            ProductId = productId;
        }

        public Guid ProductId { get; }
    }
}