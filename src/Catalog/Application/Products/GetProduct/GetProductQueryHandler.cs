﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.Products.Dtos;
using HLS.Catalog.Domain.Products;
using HLS.Core.Application.Data;

namespace HLS.Catalog.Application.Products.GetProduct {
    internal class GetProductQueryHandler : IQueryHandler<GetProductQuery, ProductDto> {
        private readonly IDbConnection _connection;

        public GetProductQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }


        public async Task<ProductDto> Handle(GetProductQuery query, CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [P].[Id],
                       [P].[Type],
                       [P].[Sku],
                       [P].[ContainerTypeId],
                       [P].[ZoneId]
                FROM [catalog].[v_Product] AS [P]
                WHERE [P].[Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", query.ProductId);

            using var reader = await _connection.ExecuteReaderAsync(sql, parameters);

            var containerDeliveryServiceProductParser = reader.GetRowParser<ContainerDeliveryServiceProductDto>();
            var containerRetrievalServiceProductParser = reader.GetRowParser<ContainerRetrievalServiceProductDto>();

            var products = new List<ProductDto>();

            while (reader.Read()) {
                ProductDto product = reader.GetString(reader.GetOrdinal("Type")) switch {
                    nameof(ProductType.ContainerDeliveryService) => containerDeliveryServiceProductParser(reader),
                    nameof(ProductType.ContainerRetrievalService) => containerRetrievalServiceProductParser(reader),
                    _ => throw new Exception("Product type is not supported")
                };

                products.Add(product);
            }

            return products.FirstOrDefault();
        }
    }
}