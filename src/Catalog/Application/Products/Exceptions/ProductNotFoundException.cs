﻿using System;

namespace HLS.Catalog.Application.Products.Exceptions {
    public class ProductNotFoundException : Exception {
        private const string ErrorMessage = "Product not found";

        public ProductNotFoundException() : base(ErrorMessage) { }
    }
}