﻿using System;
using HLS.Catalog.Application.Configuration.Commands;

namespace HLS.Catalog.Application.Products.GenerateNewContainerRetrievalServiceProduct {
    public class GenerateNewContainerRetrievalServiceProductCommand : InternalCommandBase {
        public GenerateNewContainerRetrievalServiceProductCommand(Guid containerTypeId, Guid zoneId) {
            ContainerTypeId = containerTypeId;
            ZoneId = zoneId;
        }

        public Guid ContainerTypeId { get; }

        public Guid ZoneId { get; }
    }
}