﻿using System;
using HLS.Catalog.Domain.Products.Events;
using HLS.Core.Application.Events;
using Newtonsoft.Json;

namespace HLS.Catalog.Application.Products.GenerateNewContainerRetrievalServiceProduct {
    public class NewContainerRetrievalServiceProductGeneratedNotification : DomainNotificationBase<
        NewContainerRetrievalServiceProductGeneratedDomainEvent> {
        [JsonConstructor]
        public NewContainerRetrievalServiceProductGeneratedNotification(
            NewContainerRetrievalServiceProductGeneratedDomainEvent domainEvent,
            Guid id) : base(domainEvent, id) { }
    }
}