﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Catalog.Application.Configuration.Commands;
using HLS.Catalog.Application.ContainerTypes.Exceptions;
using HLS.Catalog.Application.Zones.Exceptions;
using HLS.Catalog.Domain.ContainerTypes;
using HLS.Catalog.Domain.Products;
using HLS.Catalog.Domain.SeedWork;
using HLS.Catalog.Domain.Zones;
using MediatR;

namespace HLS.Catalog.Application.Products.GenerateNewContainerRetrievalServiceProduct {
    internal class
        GenerateNewContainerRetrievalServiceProductCommandHandler : ICommandHandler<
            GenerateNewContainerRetrievalServiceProductCommand> {
        private readonly IAggregateStore _aggregateStore;

        public GenerateNewContainerRetrievalServiceProductCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public async Task<Unit> Handle(
            GenerateNewContainerRetrievalServiceProductCommand cmd,
            CancellationToken cancellationToken) {
            var containerType = await _aggregateStore.Load(new ContainerTypeId(cmd.ContainerTypeId));
            if (containerType is null) {
                throw new ContainerTypeNotFoundException();
            }

            var zone = await _aggregateStore.Load(new ZoneId(cmd.ZoneId));
            if (zone is null) {
                throw new ZoneNotFoundException();
            }

            var product = ContainerRetrievalServiceProduct
                .GenerateNewContainerRetrievalServiceProduct(
                    containerType.GetSnapshot(),
                    zone.GetSnapshot());

            _aggregateStore.AppendChanges(product);

            return Unit.Value;
        }
    }
}