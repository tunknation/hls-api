﻿using System;

namespace HLS.Catalog.Application.Zones.Exceptions {
    public class ZoneNotFoundException : Exception {
        private const string ErrorMessage = "Zone not found";

        public ZoneNotFoundException() : base(ErrorMessage) { }
    }
}