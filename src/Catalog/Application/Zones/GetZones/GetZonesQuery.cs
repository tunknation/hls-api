﻿using System.Collections.Generic;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.Pagination;
using HLS.Catalog.Application.Zones.Dtos;

namespace HLS.Catalog.Application.Zones.GetZones {
    public class GetZonesQuery : QueryBase<IEnumerable<ZoneDto>>, IPagedQuery {
        public GetZonesQuery(int? page, int? perPage) {
            Page = page;
            PerPage = perPage;
        }

        public int? Page { get; }

        public int? PerPage { get; }
    }
}