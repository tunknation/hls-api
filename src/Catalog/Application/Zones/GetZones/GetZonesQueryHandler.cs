﻿using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.Pagination;
using HLS.Catalog.Application.Zones.Dtos;
using HLS.Core.Application.Data;

namespace HLS.Catalog.Application.Zones.GetZones {
    internal class GetZonesQueryHandler : IQueryHandler<GetZonesQuery, IEnumerable<ZoneDto>> {
        private readonly IDbConnection _connection;

        public GetZonesQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<IEnumerable<ZoneDto>> Handle(
            GetZonesQuery query,
            CancellationToken cancellationToken) {
            var parameters = new DynamicParameters();
            var pageData = PagedQueryHelper.GetPageData(query);
            parameters.Add(nameof(PagedQueryHelper.Offset), pageData.Offset);
            parameters.Add(nameof(PagedQueryHelper.Next), pageData.Next);

            var sql = @"
                SELECT [Z].[Id],
                       [Z].[Label]
                FROM [catalog].[v_Zone] AS [Z]
                ORDER BY [Z].[Id]";

            sql = PagedQueryHelper.AppendPageStatement(sql);

            return await _connection.QueryAsync<ZoneDto>(sql, parameters);
        }
    }
}