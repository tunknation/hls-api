﻿using System;
using HLS.Catalog.Domain.Zones.Events;
using HLS.Core.Application.Events;
using Newtonsoft.Json;

namespace HLS.Catalog.Application.Zones.AddNewZone {
    public class NewZoneAddedNotification : DomainNotificationBase<NewZoneAddedDomainEvent> {
        [JsonConstructor]
        public NewZoneAddedNotification(NewZoneAddedDomainEvent domainEvent, Guid id) : base(domainEvent, id) { }
    }
}