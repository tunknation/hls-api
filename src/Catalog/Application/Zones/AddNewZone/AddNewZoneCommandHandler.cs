﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Catalog.Application.Configuration.Commands;
using HLS.Catalog.Domain.SeedWork;
using HLS.Catalog.Domain.Zones;

namespace HLS.Catalog.Application.Zones.AddNewZone {
    internal class AddNewZoneCommandHandler : ICommandHandler<AddNewZoneCommand, ZoneId> {
        private readonly IAggregateStore _aggregateStore;

        public AddNewZoneCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public Task<ZoneId> Handle(AddNewZoneCommand cmd, CancellationToken cancellationToken) {
            var zone = Zone.AddNewZone(cmd.Label);

            _aggregateStore.AppendChanges(zone);

            return Task.FromResult(zone.Id);
        }
    }
}