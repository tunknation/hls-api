﻿using HLS.Catalog.Application.Configuration.Commands;
using HLS.Catalog.Domain.Zones;

namespace HLS.Catalog.Application.Zones.AddNewZone {
    public class AddNewZoneCommand : CommandBase<ZoneId> {
        public AddNewZoneCommand(string label) {
            Label = label;
        }

        public string Label { get; }
    }
}