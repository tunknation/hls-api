﻿using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.Zones.Dtos;
using HLS.Core.Application.Data;

namespace HLS.Catalog.Application.Zones.GetZone {
    internal class GetZoneQueryHandler : IQueryHandler<GetZoneQuery, ZoneDto> {
        private readonly IDbConnection _connection;

        public GetZoneQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<ZoneDto> Handle(GetZoneQuery query, CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [Z].[Id],
                       [Z].[Label]
                FROM [catalog].[v_Zone] AS [Z]
                WHERE [Z].[Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", query.ZoneId);

            return await _connection.QuerySingleOrDefaultAsync<ZoneDto>(sql, parameters);
        }
    }
}