﻿using System;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.Zones.Dtos;

namespace HLS.Catalog.Application.Zones.GetZone {
    public class GetZoneQuery : QueryBase<ZoneDto> {
        public GetZoneQuery(Guid zoneId) {
            ZoneId = zoneId;
        }

        public Guid ZoneId { get; }
    }
}