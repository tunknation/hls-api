﻿using HLS.Catalog.Domain.Zones;

namespace HLS.Catalog.Application.Zones.Dtos {
    public class ZoneDto {
        public ZoneId Id { get; set; }

        public string Label { get; set; }
    }
}