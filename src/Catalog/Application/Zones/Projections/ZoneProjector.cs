﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using HLS.Catalog.Application.Configuration.Projections;
using HLS.Catalog.Domain.Zones.Events;
using HLS.Core.Application.Data;
using HLS.Core.Domain;

namespace HLS.Catalog.Application.Zones.Projections {
    internal class ZoneProjector : IProjector {
        private readonly IDbConnection _connection;

        public ZoneProjector(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Project(IDomainEvent @event) {
            await _when((dynamic)@event);
        }

        private static Task _when(IDomainEvent @event) {
            return Task.CompletedTask;
        }

        private async Task _when(NewZoneAddedDomainEvent @event) {
            const string sql = @"
                INSERT INTO [catalog].[Zone]
                    ([Id], [Label])
                VALUES (@Id, @Label)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ZoneId);
            parameters.Add("@Label", @event.Label);

            await _connection.ExecuteAsync(sql, parameters);
        }
    }
}