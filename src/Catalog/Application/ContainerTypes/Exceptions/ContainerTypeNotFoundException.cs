﻿using System;

namespace HLS.Catalog.Application.ContainerTypes.Exceptions {
    public class ContainerTypeNotFoundException : Exception {
        private const string ErrorMessage = "Container type not found";

        public ContainerTypeNotFoundException() : base(ErrorMessage) { }
    }
}