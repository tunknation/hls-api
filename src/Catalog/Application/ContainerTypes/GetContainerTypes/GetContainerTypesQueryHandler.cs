﻿using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.ContainerTypes.Dtos;
using HLS.Catalog.Application.Pagination;
using HLS.Core.Application.Data;

namespace HLS.Catalog.Application.ContainerTypes.GetContainerTypes {
    internal class
        GetContainerTypesQueryHandler : IQueryHandler<GetContainerTypesQuery, IEnumerable<ContainerTypeDto>> {
        private readonly IDbConnection _connection;

        public GetContainerTypesQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<IEnumerable<ContainerTypeDto>> Handle(
            GetContainerTypesQuery query,
            CancellationToken cancellationToken) {
            var parameters = new DynamicParameters();
            var pageData = PagedQueryHelper.GetPageData(query);
            parameters.Add(nameof(PagedQueryHelper.Offset), pageData.Offset);
            parameters.Add(nameof(PagedQueryHelper.Next), pageData.Next);

            var sql = @"
                SELECT [CT].[Id],
                       [CT].[Label]
                FROM [catalog].[v_ContainerType] AS [CT]
                ORDER BY [CT].[Id]";

            sql = PagedQueryHelper.AppendPageStatement(sql);

            return await _connection.QueryAsync<ContainerTypeDto>(sql, parameters);
        }
    }
}