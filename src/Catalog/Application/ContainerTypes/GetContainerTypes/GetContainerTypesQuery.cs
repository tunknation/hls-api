﻿using System.Collections.Generic;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.ContainerTypes.Dtos;
using HLS.Catalog.Application.Pagination;

namespace HLS.Catalog.Application.ContainerTypes.GetContainerTypes {
    public class GetContainerTypesQuery : QueryBase<IEnumerable<ContainerTypeDto>>, IPagedQuery {
        public GetContainerTypesQuery(int? page, int? perPage) {
            Page = page;
            PerPage = perPage;
        }

        public int? Page { get; }

        public int? PerPage { get; }
    }
}