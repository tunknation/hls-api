﻿using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.ContainerTypes.Dtos;
using HLS.Core.Application.Data;

namespace HLS.Catalog.Application.ContainerTypes.GetContainerType {
    internal class GetContainerTypeQueryHandler : IQueryHandler<GetContainerTypeQuery, ContainerTypeDto> {
        private readonly IDbConnection _connection;

        public GetContainerTypeQueryHandler(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task<ContainerTypeDto> Handle(GetContainerTypeQuery query, CancellationToken cancellationToken) {
            const string sql = @"
                SELECT [CT].[Id],
                       [CT].[Label]
                FROM [catalog].[v_ContainerType] AS [CT]
                WHERE [CT].[Id] = @Id";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", query.ContainerTypeId);

            return await _connection.QuerySingleOrDefaultAsync<ContainerTypeDto>(sql, parameters);
        }
    }
}