﻿using System;
using HLS.Catalog.Application.Configuration.Queries;
using HLS.Catalog.Application.ContainerTypes.Dtos;

namespace HLS.Catalog.Application.ContainerTypes.GetContainerType {
    public class GetContainerTypeQuery : QueryBase<ContainerTypeDto> {
        public GetContainerTypeQuery(Guid containerTypeId) {
            ContainerTypeId = containerTypeId;
        }

        public Guid ContainerTypeId { get; }
    }
}