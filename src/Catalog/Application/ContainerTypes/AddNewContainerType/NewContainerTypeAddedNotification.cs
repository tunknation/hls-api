﻿using System;
using HLS.Catalog.Domain.ContainerTypes.Events;
using HLS.Core.Application.Events;
using Newtonsoft.Json;

namespace HLS.Catalog.Application.ContainerTypes.AddNewContainerType {
    public class NewContainerTypeAddedNotification : DomainNotificationBase<NewContainerTypeAddedDomainEvent> {
        [JsonConstructor]
        public NewContainerTypeAddedNotification(NewContainerTypeAddedDomainEvent domainEvent, Guid id) : base(
            domainEvent, id) { }
    }
}