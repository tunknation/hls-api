﻿using System.Threading;
using System.Threading.Tasks;
using HLS.Catalog.Application.Configuration.Commands;
using HLS.Catalog.Domain.ContainerTypes;
using HLS.Catalog.Domain.SeedWork;

namespace HLS.Catalog.Application.ContainerTypes.AddNewContainerType {
    internal class AddNewContainerTypeCommandHandler : ICommandHandler<AddNewContainerTypeCommand, ContainerTypeId> {
        private readonly IAggregateStore _aggregateStore;

        public AddNewContainerTypeCommandHandler(IAggregateStore aggregateStore) {
            _aggregateStore = aggregateStore;
        }

        public Task<ContainerTypeId> Handle(AddNewContainerTypeCommand cmd, CancellationToken cancellationToken) {
            var containerType = ContainerType.AddNewContainerType(cmd.Label);

            _aggregateStore.AppendChanges(containerType);

            return Task.FromResult(containerType.Id);
        }
    }
}