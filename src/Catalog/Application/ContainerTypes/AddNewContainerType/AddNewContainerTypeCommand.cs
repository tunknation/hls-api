﻿using HLS.Catalog.Application.Configuration.Commands;
using HLS.Catalog.Domain.ContainerTypes;

namespace HLS.Catalog.Application.ContainerTypes.AddNewContainerType {
    public class AddNewContainerTypeCommand : CommandBase<ContainerTypeId> {
        public AddNewContainerTypeCommand(string label) {
            Label = label;
        }

        public string Label { get; }
    }
}