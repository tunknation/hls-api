﻿using HLS.Catalog.Domain.ContainerTypes;

namespace HLS.Catalog.Application.ContainerTypes.Dtos {
    public class ContainerTypeDto {
        public ContainerTypeId Id { get; set; }

        public string Label { get; set; }
    }
}