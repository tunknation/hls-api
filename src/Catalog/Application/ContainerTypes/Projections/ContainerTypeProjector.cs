﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using HLS.Catalog.Application.Configuration.Projections;
using HLS.Catalog.Domain.ContainerTypes.Events;
using HLS.Core.Application.Data;
using HLS.Core.Domain;

namespace HLS.Catalog.Application.ContainerTypes.Projections {
    internal class ContainerTypeProjector : IProjector {
        private readonly IDbConnection _connection;

        public ContainerTypeProjector(ISqlConnectionFactory sqlConnectionFactory) {
            _connection = sqlConnectionFactory.GetOpenConnection();
        }

        public async Task Project(IDomainEvent @event) {
            await _when((dynamic)@event);
        }

        private static Task _when(IDomainEvent @event) {
            return Task.CompletedTask;
        }

        private async Task _when(NewContainerTypeAddedDomainEvent @event) {
            const string sql = @"
                INSERT INTO [catalog].[ContainerType]
                    ([Id], [Label])
                VALUES (@Id, @Label)";

            var parameters = new DynamicParameters();
            parameters.Add("@Id", @event.ContainerTypeId);
            parameters.Add("@Label", @event.Label);

            await _connection.ExecuteAsync(sql, parameters);
        }
    }
}