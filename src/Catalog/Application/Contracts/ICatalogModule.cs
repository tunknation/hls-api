﻿using System.Threading.Tasks;
using HLS.Catalog.Application.Configuration.Commands;
using HLS.Catalog.Application.Configuration.Queries;

namespace HLS.Catalog.Application.Contracts {
    public interface ICatalogModule {
        Task<TResult> ExecuteCommandAsync<TResult>(ICommand<TResult> command);

        Task ExecuteCommandAsync(ICommand command);

        Task<TResult> ExecuteQueryAsync<TResult>(IQuery<TResult> query);
    }
}