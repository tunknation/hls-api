﻿using MediatR;

namespace HLS.Catalog.Application.Configuration.Queries {
    public interface IQuery<out TResult> : IRequest<TResult> { }
}