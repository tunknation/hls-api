﻿using System.Threading.Tasks;

namespace HLS.Catalog.Application.Configuration.Commands {
    public interface ICommandsScheduler {
        Task EnqueueAsync(ICommand command);

        Task EnqueueAsync<T>(ICommand<T> command);
    }
}