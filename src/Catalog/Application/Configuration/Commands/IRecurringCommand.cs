﻿namespace HLS.Catalog.Application.Configuration.Commands {
    public interface IRecurringCommand : ICommand { }
}