﻿using HotChocolate.Types;

namespace HLS.Catalog.API.GraphQL {
    [ExtendObjectType(Name = "Query")]
    public class HelloWorldQuery {
        public string GetHelloWorld() {
            return "Hello World!";
        }
    }
}