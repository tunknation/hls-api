﻿using System.Threading.Tasks;
using HLS.Catalog.API.GraphQL.ContainerTypes.Inputs;
using HLS.Catalog.API.GraphQL.ContainerTypes.Payloads;
using HLS.Catalog.Application.ContainerTypes.AddNewContainerType;
using HLS.Catalog.Application.Contracts;
using HotChocolate.Types;

namespace HLS.Catalog.API.GraphQL.ContainerTypes {
    [ExtendObjectType(Name = "Mutation")]
    public class ContainerTypesMutation {
        private readonly ICatalogModule _module;

        public ContainerTypesMutation(ICatalogModule module) {
            _module = module;
        }

        public async Task<AddNewContainerTypePayload> AddNewContainerType(AddNewContainerTypeInput input) {
            var cmd = new AddNewContainerTypeCommand(input.Label);

            var containerTypeId = await _module.ExecuteCommandAsync(cmd);

            return new AddNewContainerTypePayload {
                Success = true,
                ContainerTypeId = containerTypeId.Value
            };
        }
    }
}