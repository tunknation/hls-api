﻿namespace HLS.Catalog.API.GraphQL.ContainerTypes.Inputs {
    public class AddNewContainerTypeInput {
        public string Label { get; set; }
    }
}