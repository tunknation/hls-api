﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HLS.Catalog.API.GraphQL.ContainerTypes.Types;
using HLS.Catalog.Application.ContainerTypes.GetContainerType;
using HLS.Catalog.Application.ContainerTypes.GetContainerTypes;
using HLS.Catalog.Application.Contracts;
using HotChocolate.Types;

namespace HLS.Catalog.API.GraphQL.ContainerTypes {
    [ExtendObjectType(Name = "Query")]
    public class ContainerTypesQuery {
        private readonly ICatalogModule _module;

        public ContainerTypesQuery(ICatalogModule module) {
            _module = module;
        }

        public async Task<ContainerType> GetContainerType(Guid containerTypeId) {
            var query = new GetContainerTypeQuery(containerTypeId);

            var containerType = await _module.ExecuteQueryAsync(query);

            return (ContainerType)containerType;
        }

        public async Task<List<ContainerType>> GetContainerTypes(int? page, int? perPage) {
            var query = new GetContainerTypesQuery(page, perPage);

            var containerTypes = await _module.ExecuteQueryAsync(query);

            return (from containerType in containerTypes select (ContainerType)containerType).ToList();
        }
    }
}