﻿using System;
using HLS.Catalog.Application.ContainerTypes.Dtos;

namespace HLS.Catalog.API.GraphQL.ContainerTypes.Types {
    public class ContainerType {
        public Guid Id { get; set; }

        public string Label { get; set; }

        public static explicit operator ContainerType(ContainerTypeDto dto) {
            return new ContainerType {
                Id = dto.Id,
                Label = dto.Label
            };
        }
    }
}