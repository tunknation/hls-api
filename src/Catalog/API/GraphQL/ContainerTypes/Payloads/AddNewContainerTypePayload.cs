﻿using System;

namespace HLS.Catalog.API.GraphQL.ContainerTypes.Payloads {
    public class AddNewContainerTypePayload {
        public bool Success { get; set; }

        public Guid ContainerTypeId { get; set; }
    }
}