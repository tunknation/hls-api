﻿using System;

namespace HLS.Catalog.API.GraphQL.Products.Types {
    public class ContainerRetrievalServiceProduct : Product {
        public Guid ContainerTypeId { get; set; }

        public Guid ZoneId { get; set; }
    }
}