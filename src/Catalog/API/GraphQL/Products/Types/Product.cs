﻿using System;
using HLS.Catalog.Application.Products.Dtos;
using HotChocolate.Types;

namespace HLS.Catalog.API.GraphQL.Products.Types {
    [UnionType(Name = "Product")]
    public abstract class Product {
        public Guid Id { get; set; }

        public string Type { get; set; }

        public string Sku { get; set; }

        public static explicit operator Product(ProductDto productDto) {
            return productDto switch {
                ContainerDeliveryServiceProductDto dto => (ContainerDeliveryServiceProduct)dto,
                ContainerRetrievalServiceProductDto dto => (ContainerRetrievalServiceProduct)dto,
                _ => throw new Exception("Product type is not supported")
            };
        }

        public static explicit operator Product(ContainerDeliveryServiceProductDto dto) {
            return new ContainerDeliveryServiceProduct {
                Id = dto.Id,
                Type = dto.Type,
                Sku = dto.Sku,
                ContainerTypeId = dto.ContainerTypeId,
                ZoneId = dto.ZoneId
            };
        }

        public static explicit operator Product(ContainerRetrievalServiceProductDto dto) {
            return new ContainerRetrievalServiceProduct {
                Id = dto.Id,
                Type = dto.Type,
                Sku = dto.Sku,
                ContainerTypeId = dto.ContainerTypeId,
                ZoneId = dto.ZoneId
            };
        }
    }
}