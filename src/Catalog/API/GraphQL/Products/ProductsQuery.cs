﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HLS.Catalog.API.GraphQL.Products.Types;
using HLS.Catalog.Application.Contracts;
using HLS.Catalog.Application.Products.GetProduct;
using HLS.Catalog.Application.Products.GetProducts;
using HotChocolate.Types;

namespace HLS.Catalog.API.GraphQL.Products {
    [ExtendObjectType(Name = "Query")]
    public class ProductsQuery {
        private readonly ICatalogModule _module;

        public ProductsQuery(ICatalogModule module) {
            _module = module;
        }

        public async Task<Product> GetProduct(Guid productId) {
            var query = new GetProductQuery(productId);

            var product = await _module.ExecuteQueryAsync(query);

            return (Product)product;
        }

        public async Task<List<Product>> GetProducts(int? page, int? perPage) {
            var query = new GetProductsQuery(page, perPage);

            var products = await _module.ExecuteQueryAsync(query);

            return (from product in products select (Product)product).ToList();
        }
    }
}