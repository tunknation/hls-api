﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HLS.Catalog.API.GraphQL.Zones.Types;
using HLS.Catalog.Application.Contracts;
using HLS.Catalog.Application.Zones.GetZone;
using HLS.Catalog.Application.Zones.GetZones;
using HotChocolate.Types;

namespace HLS.Catalog.API.GraphQL.Zones {
    [ExtendObjectType(Name = "Query")]
    public class ZonesQuery {
        private readonly ICatalogModule _module;

        public ZonesQuery(ICatalogModule module) {
            _module = module;
        }

        public async Task<Zone> GetZone(Guid zoneId) {
            var query = new GetZoneQuery(zoneId);

            var zone = await _module.ExecuteQueryAsync(query);

            return (Zone)zone;
        }

        public async Task<List<Zone>> GetZones(int? page, int? perPage) {
            var query = new GetZonesQuery(page, perPage);

            var zones = await _module.ExecuteQueryAsync(query);

            return (from zone in zones select (Zone)zone).ToList();
        }
    }
}