﻿namespace HLS.Catalog.API.GraphQL.Zones.Inputs {
    public class AddNewZoneInput {
        public string Label { get; set; }
    }
}