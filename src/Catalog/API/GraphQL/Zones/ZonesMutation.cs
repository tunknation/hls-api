﻿using System.Threading.Tasks;
using HLS.Catalog.API.GraphQL.Zones.Inputs;
using HLS.Catalog.API.GraphQL.Zones.Payloads;
using HLS.Catalog.Application.Contracts;
using HLS.Catalog.Application.Zones.AddNewZone;
using HotChocolate.Types;

namespace HLS.Catalog.API.GraphQL.Zones {
    [ExtendObjectType(Name = "Mutation")]
    public class ZonesMutation {
        private readonly ICatalogModule _module;

        public ZonesMutation(ICatalogModule module) {
            _module = module;
        }

        public async Task<AddNewZonePayload> AddNewZone(AddNewZoneInput input) {
            var cmd = new AddNewZoneCommand(input.Label);

            var zoneId = await _module.ExecuteCommandAsync(cmd);

            return new AddNewZonePayload {
                Success = true,
                ZoneId = zoneId.Value
            };
        }
    }
}