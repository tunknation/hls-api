﻿using System;

namespace HLS.Catalog.API.GraphQL.Zones.Payloads {
    public class AddNewZonePayload {
        public bool Success { get; set; }

        public Guid ZoneId { get; set; }
    }
}