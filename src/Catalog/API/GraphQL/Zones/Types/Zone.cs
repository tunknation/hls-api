﻿using System;
using HLS.Catalog.Application.Zones.Dtos;

namespace HLS.Catalog.API.GraphQL.Zones.Types {
    public class Zone {
        public Guid Id { get; set; }

        public string Label { get; set; }

        public static explicit operator Zone(ZoneDto dto) {
            return new Zone {
                Id = dto.Id,
                Label = dto.Label
            };
        }
    }
}