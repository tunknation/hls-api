﻿using Autofac;
using HLS.Catalog.Application.Contracts;
using HLS.Catalog.Infrastructure;

namespace HLS.Catalog.API.Modules {
    public class CatalogAutofacModule : Module {
        protected override void Load(ContainerBuilder builder) {
            builder.RegisterType<CatalogModule>()
                .As<ICatalogModule>()
                .InstancePerLifetimeScope();
        }
    }
}