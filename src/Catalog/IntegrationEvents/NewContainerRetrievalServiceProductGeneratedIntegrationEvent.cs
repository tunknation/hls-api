﻿using System;
using HLS.Core.EventBus;

namespace HLS.Catalog.IntegrationEvents {
    public class NewContainerRetrievalServiceProductGeneratedIntegrationEvent : IntegrationEvent {
        public NewContainerRetrievalServiceProductGeneratedIntegrationEvent(
            Guid id,
            DateTime occurredOn,
            Guid productId,
            string sku,
            Guid containerTypeId,
            Guid zoneId) : base(id, occurredOn) {
            ProductId = productId;
            Sku = sku;
            ContainerTypeId = containerTypeId;
            ZoneId = zoneId;
        }

        public Guid ProductId { get; }

        public string Sku { get; }

        public Guid ContainerTypeId { get; }

        public Guid ZoneId { get; }
    }
}